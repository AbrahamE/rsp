<?php

use Illuminate\Support\Facades\Route;

/**
 * 'admin' middleware and 'helpdesk' prefix applied to all routes (including names)
 *
 * @see \App\Providers\Route::register
 */

Route::admin('helpdesk', function () {
    // To be used in search/filter functionality (search-string.php)
    Route::get('statuses/index', 'Statuses@index')->name('statuses.index');

    // Tickets
    Route::post('tickets/{id}/update-partial', 'Tickets@updatePartial');
    Route::get('tickets/{id}/history', 'Tickets@history')->name('tickets.history'); 
    Route::post('tickets/import', 'Tickets@import')->name('tickets.import');
    Route::get('tickets/export', 'Tickets@export')->name('tickets.export');
    Route::get('tickets/categorys', 'Tickets@getCategorys')->name('tickets.categorys');
    Route::get('tickets/categorys_all', 'Tickets@getAllCategorys')->name('tickets.categorys_all');
    Route::get('tickets/sub_categorys', 'Tickets@getSubCategorys')->name('tickets.sub_categorys');
    Route::get('tickets/specific_service', 'Tickets@getSpecificService')->name('tickets.specific_service');
    Route::get('tickets/state', 'Tickets@getState')->name('tickets.state');
    Route::get('tickets/municipality', 'Tickets@getMunicipality')->name('tickets.municipality');
    Route::get('tickets/parish', 'Tickets@getParish')->name('tickets.parish');
    Route::get('tickets/community', 'Tickets@getCommunity')->name('tickets.community');
    Route::get('tickets/users', 'Tickets@getUserByDepartment')->name('tickets.users');
    Route::resource('tickets', 'Tickets')->middleware('dropzone');

    // Replies
    Route::post('replies/store', 'Replies@store');
    Route::patch('replies/{id}/update', 'Replies@update');
    Route::resource('replies', 'Replies');
});
