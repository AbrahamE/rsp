<?php

namespace Modules\Helpdesk\Widgets;

use App\Abstracts\Widget;
use Modules\Helpdesk\Models\Status;

class TotalByStatus extends Widget
{
    public function show($tickets = null, $status)
    {
        $total = 0;
        $statusId = Status::where('name', $status)->first()->id;

        if ($tickets) {
            $hasticket = true;
            $total = isset($tickets[$statusId]) && !is_null($tickets[$statusId]) ? $tickets[$statusId]->count() : 0;
            $this->views['header'] = 'helpdesk::widgets.standard_header';
        } else {
            $hasticket = false;
            $this->views['header'] = 'helpdesk::widgets.standard_header';
        }

        return $this->view('helpdesk::widgets.total_by_status', [
            'total' => $total,
            'hasticket' => $hasticket,
            'status'   => $status
        ]);
    }
}
