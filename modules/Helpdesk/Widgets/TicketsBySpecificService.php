<?php

namespace Modules\Helpdesk\Widgets;

use App\Abstracts\Widget;
use Modules\Helpdesk\Models\Ticket;
use App\Traits\Tailwind;
use Illuminate\Support\Facades\DB;

class TicketsBySpecificService extends Widget
{
    
    use Tailwind;

    public $default_name = 'helpdesk::widgets.tickets_by_specific_service';

    public $description = 'helpdesk::widgets.description.tickets_by_specific_service';

    public function show()
    {
        $query = Ticket::select('helpdesk_specific_service.name as service', 'helpdesk_specific_service.color', DB::raw('count(*) as service_count'))
            ->join('helpdesk_specific_service', 'helpdesk_tickets.specific_service', '=', 'helpdesk_specific_service.id')
            ->groupBy('helpdesk_specific_service.name');

        $query->each(function ($specific_service) {
            $this->applyFilters($specific_service, ['date_field' => 'created_at']);
            
            $this->addToBar($specific_service->color,  $specific_service->service, $specific_service->service_count);
        });
      
        $chart = $this->getBarChart(trans('helpdesk::widgets.tickets_by_specific_service'), '100%', 300, 6);
        
        return $this->view('widgets.bar_chart', [
            'chart' => $chart,
        ]);
    }
}
