<?php

namespace Modules\Helpdesk\Widgets;

use Akaunting\Apexcharts\Chart;
use App\Abstracts\Widget;
use App\Models\Banking\Transaction;
use App\Models\Common\Company;
use App\Traits\Currencies;
use App\Traits\DateTime;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Route;
use Modules\Helpdesk\Models\Ticket;

class TicketsLineChart extends Widget
{
    use Currencies, DateTime;

    public $default_name = 'helpdesk::general.widgets.tickets_line_chart';

    public $default_setting = [
        'width' => 'w-10/12 my-8 px-12',
    ];

    public $start_date;

    public $end_date;

    public $period;

    public function show($tickets = null)
    {
        $this->setFilter();
        $header = false;

        if (is_null($tickets)) {
            $tickets = Ticket::all()->collect();
            $header = true;
        }

        $labels = $this->getLabels();

        $companies = Company::all()->pluck('name', 'id');

        $rows = $companies->flatMap(function ($company, $id) {
            return [
                $company => array_values($this->calculateTotals(
                    Ticket::where('company_id', $id)->collect()
                ))
            ];
        });

        $totals = $companies->flatMap(function ($company, $company_id) {
            return [
                $company =>  Ticket::where('company_id', $company_id)->count()
            ];
        });

        $colors = $this->getColors();

        $options = [
            'chart' => [
                'type' => 'bar',
            ],
            'plotOptions' => [
                'bar' => [
                    'columnWidth' => '40%',
                ],
            ],
            'legend' => [
                'position' => 'top',
            ],
        ];

        $chart = new Chart();

        $chart->setType('line')
            ->setOptions($options)
            ->setLabels(array_values($labels))
            ->setColors($colors);


        $this->createRows($chart, $rows);

        return $this->view('helpdesk::widgets.cash_flow', [
            'chart' => $chart,
            'totals' => $totals,
            'header' => $header
        ]);
    }


    public function createRows(&$chart, $rows)
    {
        foreach ($rows as $key => $value) {
            $chart->setDataset($key, 'bar', $value);
        }
    }
    public function setFilter(): void
    {
        $financial_start = $this->getFinancialStart()->format('Y-m-d');

        // check and assign year start
        if (($year_start = Date::today()->startOfYear()->format('Y-m-d')) !== $financial_start) {
            $year_start = $financial_start;
        }

        $this->start_date = Date::parse(request('start_date', $year_start));
        $this->end_date = Date::parse(request('end_date', Date::parse($year_start)->addYear(1)->subDays(1)->format('Y-m-d')));
        $this->period = request('period', 'month');
    }

    public function getLabels(): array
    {
        $range = request('range', 'custom');

        $start_month = $this->start_date->month;
        $end_month = $this->end_date->month;

        // Monthly
        $labels = [];

        $s = clone $this->start_date;

        if ($range == 'last_12_months') {
            $end_month = 12;
            $start_month = 0;
        } elseif ($range == 'custom') {
            $end_month = $this->end_date->diffInMonths($this->start_date);
            $start_month = 0;
        }

        for ($j = $end_month; $j >= $start_month; $j--) {
            $labels[$end_month - $j] = $s->format('M Y');

            if ($this->period == 'month') {
                $s->addMonth();
            } else {
                $s->addMonths(3);
                $j -= 2;
            }
        }

        return $labels;
    }

    public function getColors(): array
    {
        return [
            // '#8bb475',
            // '#fb7185',
            // '#7779A2',
            '#F58934ff',
            '#3f88c5ff',
            '#032b43ff',
            '#136f63ff',
            '#c0e0deff'
        ];
    }

    private function calculateTotals($model): array
    {
        $totals = [];

        $date_format = 'Y-m';

        if ($this->period == 'month') {
            $n = 1;
            $start_date = $this->start_date->format($date_format);
            $end_date = $this->end_date->format($date_format);
            $next_date = $start_date;
        } else {
            $n = 3;
            $start_date = $this->start_date->quarter;
            $end_date = $this->end_date->quarter;
            $next_date = $start_date;
        }

        $s = clone $this->start_date;

        //$totals[$start_date] = 0;
        while ($next_date <= $end_date) {
            $totals[$next_date] = 0;

            if ($this->period == 'month') {
                $next_date = $s->addMonths($n)->format($date_format);
            } else {
                if (isset($totals[4])) {
                    break;
                }

                $next_date = $s->addMonths($n)->quarter;
            }
        }

        $model = $model->whereBetween('created_at', [$this->start_date, $this->end_date]);

        $this->setTotals($totals, $model, $date_format);

        return $totals;
    }


    private function setTotals(&$totals, $items, $date_format): void
    {

        foreach ($items as $item) {
            if ($this->period == 'month') {
                $i = Date::parse($item->created_at)->format($date_format);
            } else {
                $i = Date::parse($item->created_at)->quarter;
            }

            if (!isset($totals[$i])) {
                continue;
            }

            if ($i == Date::parse($item->created_at)->format($date_format)) {
                $totals[$i] += 1;
            }
        }
    }

    private function calculateProfit($incomes, $expenses): array
    {
        $profit = [];

        $precision = currency()->getPrecision();

        foreach ($incomes as $key => $income) {
            $value = $income - abs($expenses[$key]);

            $profit[$key] = round($value, $precision);
        }

        return $profit;
    }
}
