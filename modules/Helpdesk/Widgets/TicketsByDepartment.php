<?php

namespace Modules\Helpdesk\Widgets;

use App\Abstracts\Widget;
use Modules\Helpdesk\Models\Ticket;
use App\Traits\Tailwind;
use App\Models\Setting\Category;
use Illuminate\Support\Facades\DB;

class TicketsByDepartment extends Widget
{

    use Tailwind;

    public $default_name = 'helpdesk::widgets.tickets_by_department';

    public $description = 'helpdesk::widgets.description.tickets_by_department';

    public function show()
    {
        $query = Ticket::select('categories.name  as department', 'helpdesk_tickets.department_id', 'categories.color', DB::raw('count(*) as department_count'))
            ->join('categories', 'helpdesk_tickets.department_id', '=', 'categories.id')
            ->groupBy('categories.name', 'categories.color')
            ->get();

        $query->each(function ($department) {
            $this->applyFilters($department, ['date_field' => 'created_at']);
            
            $this->addToBar($department->color,  $department->department, $department->department_count);
        });

       
        $chart = $this->getBarChart(trans('helpdesk::widgets.tickets_by_department'), '100%', 300, 6);

        return $this->view('widgets.bar_chart', [
            'chart' => $chart,
        ]);
    }


}
