<?php

namespace Modules\Helpdesk\Widgets;

use App\Abstracts\Widget;
use App\Models\Setting\Category;
use App\Traits\Tailwind;
use Illuminate\Support\Facades\DB;
use Modules\Helpdesk\Models\Ticket;

class TicketsByCategory extends Widget
{
    use Tailwind;

    public $default_name = 'helpdesk::widgets.tickets_by_category';

    public $description = 'helpdesk::widgets.description.tickets_by_category';

    public function show()
    {
        // $query = Category::withSubCategory()->withCount('helpdesk_tickets')->type('ticket')->orderBy('helpdesk_tickets_count', 'desc');

        $query = Ticket::select('categories.name  as category','helpdesk_tickets.category_id', 'categories.color', DB::raw('count(*) as category_count'))
            ->join('categories', 'helpdesk_tickets.category_id', '=', 'categories.id')
            ->groupBy('categories.name')
            ->get();

        $query->each(function ($specific_service) {
            $this->applyFilters($specific_service, ['date_field' => 'created_at']);
            
            $this->addToBar($specific_service->color,  $specific_service->category, $specific_service->category_count);
        });

        $chart = $this->getBarChart(trans('helpdesk::widgets.tickets_by_category'), '100%', 300, 6);

        return $this->view('widgets.bar_chart', [
            'chart' => $chart,
        ]);
    }
}
