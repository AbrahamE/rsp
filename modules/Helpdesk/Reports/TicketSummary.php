<?php

namespace Modules\Helpdesk\Reports;

use App\Abstracts\Report;
use Modules\Helpdesk\Models\Ticket;

class TicketSummary extends Report
{
    public $default_name = 'helpdesk::reports.ticket_name';

    public $category = 'helpdesk::general.name';

    public $type = 'detail';

    public $icon = 'confirmation_number';

    public $has_money = false;

    public  $tickets = [];

    public  $tickets_by_group = [];

    public $groupsProject = [
        'category' => "Categoria",
        'status' => "Estatus",
        'department' => "departmento",
        'service' => "Servicio espesifico",
        'assignee' => "Asignado a",
    ];

    public function setData()
    {
        $this->tickets_by_group = $group_field = $this->getSetting('group') == "service"
            ? "specific_service"
            : $this->getSetting('group')."_id";

        $row = $this->getSetting('group');

        $this->tickets = Ticket::all()->groupBy((function ($item) use ($group_field, $row) {
            if ($item->$group_field && isset($item->$row)) 
                return $item->$row->name;
            return 'others';
        }));

        $this->has_money = false;
    }

    public function setViews()
    {
        parent::setViews();

        $this->views['detail.table'] = 'helpdesk::reports.ticket_by';
    }

    public function getGroupField()
    {
        return [
            'type' => 'selectGroup',
            'name' => 'group',
            'title' => trans('general.group_by'),
            'icon' => 'folder',
            'values' => $this->groupsProject,
            'selected' => 'department',
            'attributes' => [
                'required' => 'required',
            ],
        ];
    }
    public function getFields()
    {
        return [
            $this->getGroupField(),
            $this->getPeriodField(),
        ];
    }
}
