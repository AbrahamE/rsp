<?php

namespace Modules\Helpdesk\Http\Requests;

use App\Abstracts\Http\FormRequest;
use Illuminate\Support\Str;

class Ticket extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $attachment = 'nullable';

        if ($this->files->get('attachment')) {
            $attachment = 'mimes:' . config('filesystems.mimes') . ',docx,doc,pdf,xlsx'  . '|between:0,' . config('filesystems.max_size') * 1024;
        }


        $rules =  [
            'id'                 => 'integer',
            'subject'            => 'required|string',
            'department_id'      => 'required',
            'category_id'        => 'required',
            'specific_service'   => 'required',
            'state_id'              => 'required',
            'municipality_id'       => 'required',
            'parish_id'             => 'required', 
            'community_id'          => 'required',
            'type_rif'           => 'required',
            'rif'                => 'required',
            'business_name'      => 'required|string',
            'assignee_id'        => 'required',
            'status_id'          => 'required',
            'priority_id'        => 'required',
            'additional_phone'   => 'nullable|size:11', //regex:/^(0414|0424|0412|0416|0426)[0-9]{7}$/g    
            'phone'              => 'nullable|size:11', //regex:/^(0414|0424|0412|0416|0426)[0-9]{7}$/g            
            'email'              => 'nullable|email',
            'message'            => 'required|string',
            'created_by'         => 'integer',
            'document_ids'       => 'nullable|array',
            'attachment.*'       => $attachment,
            'department_id'      => 'required|integer',
        ];


        if (in_array(1, user()->company_ids)) {
            $rules['customers'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'subject.required'          => trans('validation.required', ['attribute' => Str::lower(trans('helpdesk::general.ticket.subject'))]),
            'customers.required'        => trans('validation.required', ['attribute' => Str::lower(trans('helpdesk::general.ticket.customers'))]),
            'message.required'          => trans('validation.required', ['attribute' => Str::lower(trans('helpdesk::general.ticket.message'))]),
            'category_id.required'      => trans('validation.required', ['attribute' => Str::lower(trans('helpdesk::general.ticket.category'))]),
            'department_id.required'    => trans('validation.required', ['attribute' => Str::lower(trans('helpdesk::general.ticket.department'))]),
            'department_id.integer'     => trans('validation.integer', ['attribute' => Str::lower(trans('helpdesk::general.ticket.department'))]),
            'phone'                     => trans('validation.numeric', ['attribute' => Str::lower(trans('helpdesk::general.ticket.phone'))]),
            'phone.size'                => trans('validation.size', ['attribute' => Str::lower(trans('helpdesk::general.ticket.phone')), 'size' => 11, 'max' => 11]),
            'phone.regex'               => 'El campo :attribute debe comenzar con 0412, 0416, 0424 o 0426 y tener exactamente 11 dígitos.',
            'additional_phone'          => trans('validation.numeric', ['attribute' => Str::lower(trans('helpdesk::general.ticket.additional_phone'))]),
            'additional_phone.size'     => trans('validation.size', ['attribute' => Str::lower(trans('helpdesk::general.ticket.additional_phone')), 'size' => 11, 'max' => 11]),
            'additional_phone.regex'    => 'El campo :attribute debe comenzar con 0412, 0416, 0424 o 0426 y tener exactamente 11 dígitos.',
            'email.required'            => trans('validation.required', ['attribute' => Str::lower(trans('helpdesk::general.ticket.email'))]),
            'email.email'               => trans('validation.email', ['attribute' => Str::lower(trans('helpdesk::general.ticket.email'))]),
            'email.unique'              => trans('validation.unique', ['attribute' => Str::lower(trans('helpdesk::general.ticket.email'))]),
            'business_name.required'    => trans('validation.required', ['attribute' => Str::lower(trans('helpdesk::general.ticket.business_name'))]),
            'rif.required'              => trans('validation.required', ['attribute' => Str::lower(trans('helpdesk::general.ticket.rif'))]),
            'type_rif.required'         => trans('validation.required', ['attribute' => Str::lower(trans('helpdesk::general.ticket.type_rif'))]),
            'assignee_id.required'      => trans('validation.required', ['attribute' => Str::lower(trans('helpdesk::general.ticket.assignee'))]),
            'status_id.required'        => trans('validation.required', ['attribute' => Str::lower(trans('helpdesk::general.ticket.status'))]),
            'priority_id.required'      => trans('validation.required', ['attribute' => Str::lower(trans('helpdesk::general.ticket.priority'))]),
            'created_by.integer'        => trans('validation.integer', ['attribute' => Str::lower(trans('helpdesk::general.ticket.created_by'))]),
            'attachment.*.mimes'        => trans('validation.mimes', ['attribute' => trans('helpdesk::general.ticket.attachment')]),
            'attachment.*.between'      => trans('validation.between.file', ['attribute' => trans('helpdesk::general.ticket.attachment'), 'min' => 0, 'max' => config('filesystems.max_size')]),
        ];
    }
}
