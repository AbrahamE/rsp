<?php

namespace Modules\Helpdesk\Database\Seeds;

use App\Abstracts\Model;
use Illuminate\Database\Seeder;
use Modules\Helpdesk\Models\Status;

class Statuses extends Seeder
{
    public function run()
    {
        Model::unguard();

        $this->create();

        Model::reguard();
    }

    private function create()
    {
        $company_id = $this->command->argument('company') ?? company_id();

        $statuses = [
            [
                'company_id' => $company_id,
                'name' => 'open',
                'position' => 'first',
                'flow_id' => 1,
                'flow' => implode(',', [1, 2]),
                'color' => '#EF8100',
                'notification' => false,
            ],
            [
                'company_id' => $company_id,
                'name' => 'in_progress',
                'position' => 'middle',
                'flow_id' => 2,
                'flow' => implode(',', [2,3,4,5,6]),
                'color' => '#EF8100',
                'notification' => false,
            ],
            [
                'company_id' => $company_id,
                'name' => 'on_hold',
                'position' => 'middle',
                'flow_id' => 3,
                'flow' => implode(',', [2,3,4,5,6]),
                'color' => '#EF8100',
                'notification' => false,
            ],
            [
                'company_id' => $company_id,
                'name' => 'answered',
                'position' => 'middle',
                'flow_id' => 4,
                'flow' => implode(',', [2,3,4,5,6]),
                'color' => '#EF8100',
                'notification' => false,
            ],
            [
                'company_id' => $company_id,
                'name' => 'earrings',
                'position' => 'last',
                'flow_id' => 6,
                'flow' => implode(',', [6,1,7]),
                'color' => '#EF8100',
                'notification' => false,
            ],
            [
                'company_id' => $company_id,
                'name' => 'closed',
                'position' => 'middle',
                'flow_id' => 5,
                'flow' => implode(',', [5]),
                'color' => '#EF8100',
                'notification' => false,
            ],
            [
                'company_id' => $company_id,
                'name' => 'solved',
                'position' => 'last',
                'flow_id' => 8,
                'flow' => implode(',', [5,6]),
                'color' => '#EF8100',
                'notification' => false,
            ]
        ];

        foreach ($statuses as $status) {
            Status::firstOrCreate($status)
                ->update(['created_from' => 'helpdesk::seed']); 
        }
    }
}
