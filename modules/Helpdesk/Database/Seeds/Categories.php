<?php

namespace Modules\Helpdesk\Database\Seeds;

use App\Abstracts\Model;
use Illuminate\Database\Seeder;
use App\Jobs\Setting\CreateCategory;
use Modules\Helpdesk\Jobs\specificService\CreateSpecificService;
use App\Traits\Jobs;

class Categories extends Seeder
{
    use Jobs;

    public $category  = [];
    public $sub_categories  = [];
    public $specific_service  = [];

    public function run()
    {
        Model::unguard();

        $this->create();

        Model::reguard();
    }

    private function create()
    {
        $company_id = $this->command->argument('company') ?? company_id();

        $categories = [
            [
                'company_id' => $company_id,
                'name' => trans('helpdesk::general._category.address_registry_taxpayers_and_obligations'),
                'type' => 'ticket',
                'color' => '#726e97',
                'enabled' => '1',
            ],
            [
                'company_id' => $company_id,
                'name' => trans('helpdesk::general._category.tax_accounting_department'),
                'type' => 'ticket',
                'color' => '#726e97',
                'enabled' => '1',
            ],
            [
                'company_id' => $company_id,
                'name' => trans('helpdesk::general._category.collection_department'),
                'type' => 'ticket',
                'color' => '#726e97',
                'enabled' => '1',
            ],
            [
                'company_id' => $company_id,
                'name' => trans('helpdesk::general._category.supervision_directorate'),
                'type' => 'ticket',
                'color' => '#726e97',
                'enabled' => '1',
            ],
            [
                'company_id' => $company_id,
                'name' => trans('helpdesk::general._category.sigat_support'),
                'type' => 'ticket',
                'color' => '#726e97',
                'enabled' => '1',
            ],
        ];

        $sub_categories = [
            trans('helpdesk::general._category.address_registry_taxpayers_and_obligations') => [
                [
                    'company_id' => $company_id,
                    'name' =>   trans('helpdesk::general._sub_category.user_registration'),
                    'type' => 'ticket',
                    'color' => '#586E26',
                    'enabled' => '1',
                ],
                [
                    'company_id' => $company_id,
                    'name' =>  trans('helpdesk::general._sub_category.request_management'),
                    'type' => 'ticket',
                    'color' => '#586E26',
                    'enabled' => '1',
                ],
                [
                    'company_id' => $company_id,
                    'name' =>  trans('helpdesk::general._sub_category.management_tax_obligation_records'),
                    'type' => 'ticket',
                    'color' => '#586E26',
                    'enabled' => '1',
                ],
                [
                    'company_id' => $company_id,
                    'name' => trans('helpdesk::general._sub_category.declaration_management'),
                    'type' => 'ticket',
                    'color' => '#586E26',
                    'enabled' => '1',
                ]
            ],
            trans('helpdesk::general._category.tax_accounting_department') => [
                [
                    'company_id' => $company_id,
                    'name' =>  trans('helpdesk::general._sub_category.payment_management'),
                    'type' => 'ticket',
                    'color' => '#586E26',
                    'enabled' => '1',
                ]
            ],
            trans('helpdesk::general._category.collection_department') => [
                [
                    'company_id' => $company_id,
                    'name' => trans('helpdesk::general._sub_category.collection'),
                    'type' => 'ticket',
                    'color' => '#586E26',
                    'enabled' => '1',
                ]
            ],
            trans('helpdesk::general._category.supervision_directorate') => [
                [
                    'company_id' => $company_id,
                    'name' => trans('helpdesk::general._sub_category.inspection'),
                    'type' => 'ticket',
                    'color' => '#586E26',
                    'enabled' => '1',
                ]
            ],
            trans('helpdesk::general._category.sigat_support') => [
                [
                    'company_id' => $company_id,
                    'name' => trans('helpdesk::general._sub_category.technical_incidents'),
                    'type' => 'ticket',
                    'color' => '#586E26',
                    'enabled' => '1',
                ]
            ]
        ];

        $specific_service = [
            trans('helpdesk::general._sub_category.user_registration') => [
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Registro nuevo de contribuyente'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Cambio de correo electronico de acceso al sistema'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Cambio o actualización de representante legal'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Modificación de direcion fiscal incorrecta'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Modificacion de capital social incorrecto'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Eliminacion de registro de usuario in correcto'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Modificación de documentos cargados erróneamente'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Reactivacion notificación de correo por intentos inválidos'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Modificacion de razon social incorrecta'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Cambio o actualizacion de numero de contacto'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Camnio o actualizacion de correo electronicos de contacto'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Cambio de tipo de contribuyente agente de retención']
            ],

            trans('helpdesk::general._sub_category.request_management') => [
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Eliminación de solicitud de inmueble incorrecta'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Eliminación de solicitud de vehículo incorrecta'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Eliminación de solicitud de licencia ISAE incorrecta'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Carga de documentos faltantes en registro de inmueble en proceso'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Carga de documento faltante de licencia ISAE en proceso'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Eliminación de tasa solicitada por error'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Modificación de datos incorrectos en solicitud de inmueble'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Modificación de datos incorrectos en solicitud de licencia ISAE'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Modificación de datos incorrecto en solicitud de vehículo'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Solicitud con proceso incluido de la administracion']
            ],

            trans('helpdesk::general._sub_category.management_tax_obligation_records') => [
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Registro nueva licencia'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Registro nuevo inmurbe'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Registro nuevo vehiculo'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Registro nueva publicidad'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Registro nuevo espectáculo publico'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Modificación de tarifa aseo urbano '],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Modificación de permiso expendio de licores'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'modificacion de codigo incorrectos en licencoa ISAE activa'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Modificación de datos de licencia ISAE activa'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Eliminacion / retiro de registro de contribuyente sin licencia activo'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación de licencia ISAE activa aprobada por error'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Eliminación / retiro de registro e inmueble activo'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Eliminación / retiro de permiso temporal activo'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Eliminación / retiro de registro de vehículo activo'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Eliminación de servicio de aseo urbano incorrecto'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Eliminacion / retiro de registro de publicidad comercial'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Eliminación / retiro de registro de espectáculos públicos']
            ],

            trans('helpdesk::general._sub_category.declaration_management') => [
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Presentacion de declaracion ISAE'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Presentación de declaración de inmuebles urbanos'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Presentación de declaración de vehículo'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Declaración de declaración de publicidad'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación de declaración ISAE incorrecta'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación de declaracion de inmuebles urbanos incorrecta'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación de declaración de vehículos incorrecta'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación de impuesto a la publicidad incorrecta'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación de declaración de publicidad comercial'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación de declaración de espectáculos públicos'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación de declaración de asea urbano'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Reclamo por recargos de declaración ISAE'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación por recargos por declaración de ISAE'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación por recargos por declaración de inmuebles urbanos'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación por recargos por declaración de publicidad comercial'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación de recargos por declaración de vehículos'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación de recargos por declaración de espectáculos públicos'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación de entrenamiento por retención ISAE']
            ],

            trans('helpdesk::general._sub_category.payment_management') => [
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Envío de pagos rechazados a revisión '],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Anulación de pagos incorrectos conciliados'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Verificación de pagos en linea no aplicados'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Verificación de pago movil no registrado'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Reporte de pago por punto de venta no aplicado'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Reporte de transferencia bancaria a cuenta no recaudadora'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Registro de pagos']
            ],

            trans('helpdesk::general._sub_category.collection') => [
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Consulta de deuda tributaria'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Convenio de pagos']
            ],

            trans('helpdesk::general._sub_category.inspection') => [
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Multas'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Reparos']
            ],

            trans('helpdesk::general._sub_category.technical_incidents') => [
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Cierre de sesión inesperado al seleccionar una opción'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Error al realizar una declaración ISAE'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Error al realizar registro e contribuyente'],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Error al seleccionar geolocalizacion '],
                ['company_id' => $company_id, 'color' => '#640056', 'name' => 'Correo de recuperación / Registro no llega por reiteradas solicitudes']
            ],
        ];

        foreach ($categories as $category) {
            $category['created_from'] = 'helpdesk::seed';

            $this->category =  $this->dispatch(new CreateCategory($category));

            foreach ($sub_categories[$this->category->name] as $value) {
                $value['created_from'] = 'helpdesk::seed';
                $value['parent_id'] = $this->category->id;

                $this->sub_categories = $this->dispatch(new CreateCategory($value));

                foreach ($specific_service[$this->sub_categories->name] as $service) {
                    $service['created_from'] = 'helpdesk::seed';
                    $service['category_id'] = $this->sub_categories->id;

                    $this->dispatch(new CreateSpecificService($service));
                }
            }
        }
    }
}
