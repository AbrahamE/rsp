<?php

namespace Modules\Helpdesk\Database\Seeds;

use App\Abstracts\Model;
use Illuminate\Database\Seeder;

class Install extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Categories::class);
        $this->call(Statuses::class);
        $this->call(Priorities::class);
        $this->call(Reports::class);
        $this->call(Dashboards::class);
        $this->call(Permissions::class);
    }
}
