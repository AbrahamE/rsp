<?php

namespace Modules\Helpdesk\Database\Seeds;

use App\Abstracts\Model;
use Illuminate\Database\Seeder;
use App\Traits\Permissions as Helper;

class Permissions extends Seeder
{
    use Helper;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->create();

        Model::reguard();
    }

    private function create()
    {
        $this->attachPermissionsToAdminRoles([
            'helpdesk-widgets-tickets-by-department' => 'r',
            'helpdesk-widgets-tickets-by-specific-service' => 'r',
            'helpdesk-widgets-tickets-line-chart' => 'r',
        ]);

        $this->attachPermissionsByRoleNames([
            'analyst' => [
                'helpdesk-tickets' => 'c,r,u,d',
                'helpdesk-replies' => 'c',
            ],
            'coordinator' => [
                'helpdesk-tickets' => 'c,r,u,d',
                'helpdesk-replies' => 'c',
            ],
            'head_of_area' => [
                'helpdesk-tickets' => 'c,r,u,d',
                'helpdesk-replies' => 'c',
            ],
        ]);

        $this->attachPermissionsToPortalRoles([
            'helpdesk-widgets-tickets-by-department' => 'r',
            'helpdesk-widgets-tickets-by-specific-service' => 'r',
            'helpdesk-widgets-tickets-line-chart' => 'r',
        ]);
        
    }
}
