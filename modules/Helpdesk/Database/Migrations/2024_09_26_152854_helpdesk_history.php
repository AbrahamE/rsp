<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HelpdeskHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helpdesk_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('ticket_id')->nullable();
            $table->text('description');
            $table->integer('user_id')->nullable();
            $table->string('action')->nullable();
            $table->unsignedInteger('created_by')->default(1);
            $table->timestamp('created')->useCurrent();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helpdesk_history');
    }
}
