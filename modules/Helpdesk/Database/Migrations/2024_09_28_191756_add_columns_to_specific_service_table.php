<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToSpecificServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('helpdesk_tickets', function (Blueprint $table) {
            $table->text('state_id')->nullable()->after('specific_service');
            $table->text('municipality_id')->nullable()->after('state_id');
            $table->text('parish_id')->nullable()->after('municipality_id');
            $table->text('community_id')->nullable()->after('parish_id'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
