UPDATE vcq_helpdesk_specific_service
SET color='#FF0000' WHERE category_id = 11;

UPDATE vcq_helpdesk_specific_service
SET color='#00FF00' WHERE category_id = 12;

UPDATE vcq_helpdesk_specific_service
SET color='#0000FF' WHERE category_id = 13;

UPDATE vcq_helpdesk_specific_service
SET color='#FFFF00' WHERE category_id = 14;

UPDATE vcq_helpdesk_specific_service
SET color='#FFA500' WHERE category_id = 15;

UPDATE vcq_helpdesk_specific_service
SET color='#FFC0CB' WHERE category_id = 16;

UPDATE vcq_helpdesk_specific_service
SET color='#800080' WHERE category_id = 17;

UPDATE vcq_helpdesk_specific_service
SET color='#00FFFF' WHERE category_id = 18;

UPDATE vcq_helpdesk_specific_service
SET color='#FF00FF' WHERE category_id = 19;
