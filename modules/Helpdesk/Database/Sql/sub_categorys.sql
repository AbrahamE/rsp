INSERT INTO etax.vfm_categories
    (company_id, name , type, color, enabled, parent_id, created_from)
VALUES
    (1, 'Registro de usuario', 'ticket', '#3c3f72', 1, 6, 'helpdesk::seed'),
    (1, 'Gestión de solicitudes', 'ticket', '#3c3f72', 1, 6, 'helpdesk::seed'),
    (1, 'Gestión de registros de obligaciones tributarias', 'ticket', '#3c3f72', 1, 6, 'helpdesk::seed'),
    (1, 'Gestión de declaraciones', 'ticket', '#3c3f72', 1, 6, 'helpdesk::seed'),
    (1, 'Gestión de pagos', 'ticket', '#3c3f72', 1, 7, 'helpdesk::seed'),
    (1, 'Cobranza', 'ticket', '#3c3f72', 1, 8, 'helpdesk::seed'),
    (1, 'Fiscalización', 'ticket', '#3c3f72', 1, 9, 'helpdesk::seed'),
    (1, 'Incidencias tecnicas', 'ticket', '#3c3f72', 1, 10, 'helpdesk::seed');





INSERT INTO etax.vfm_helpdesk_specific_service
    (company_id,category_id,name,created_from,created_by,created_at,updated_at,deleted_at)
VALUES
    (1, 11, 'Registro nuevo de contribuyente', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 11, 'Cambio de correo electronico de acceso al sistema', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 11, 'Cambio o actualización de representante legal', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 11, 'Modificación de direcion fiscal incorrecta', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 11, 'Modificacion de capital social incorrecto', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 11, 'Eliminacion de registro de usuario in correcto', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 11, 'Modificación de documentos cargados erróneamente', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 11, 'Reactivacion notificación de correo por intentos inválidos', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 11, 'Modificacion de razon social incorrecta', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 11, 'Cambio o actualizacion de numero de contacto', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL);
INSERT INTO etax.vfm_helpdesk_specific_service
    (company_id,category_id,name,created_from,created_by,created_at,updated_at,deleted_at)
VALUES
    (1, 11, 'Camnio o actualizacion de correo electronicos de contacto', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 11, 'Cambio de tipo de contribuyente agente de retención', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 12, 'Eliminación de solicitud de inmueble incorrecta', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 12, 'Eliminación de solicitud de vehículo incorrecta', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 12, 'Eliminación de solicitud de licencia ISAE incorrecta', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 12, 'Carga de documentos faltantes en registro de inmueble en proceso', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 12, 'Carga de documento faltante de licencia ISAE en proceso', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 12, 'Eliminación de tasa solicitada por error', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 12, 'Modificación de datos incorrectos en solicitud de inmueble', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 12, 'Modificación de datos incorrectos en solicitud de licencia ISAE', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL);
INSERT INTO etax.vfm_helpdesk_specific_service
    (company_id,category_id,name,created_from,created_by,created_at,updated_at,deleted_at)
VALUES
    (1, 12, 'Modificación de datos incorrecto en solicitud de vehículo', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 12, 'Solicitud con proceso incluido de la administracion', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Registro nueva licencia', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Registro nuevo inmurbe', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Registro nuevo vehiculo', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Registro nueva publicidad', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Registro nuevo espectáculo publico', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Modificación de tarifa aseo urbano ', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Modificación de permiso expendio de licores', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'modificacion de codigo incorrectos en licencoa ISAE activa', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL);

INSERT INTO etax.vfm_helpdesk_specific_service
    (company_id,category_id,name,created_from,created_by,created_at,updated_at,deleted_at)
VALUES
    (1, 13, 'Modificación de datos de licencia ISAE activa', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Eliminacion / retiro de registro de contribuyente sin licencia activo', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Anulación de licencia ISAE activa aprobada por error', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Eliminación / retiro de registro e inmueble activo', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Eliminación / retiro de permiso temporal activo', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Eliminación / retiro de registro de vehículo activo', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Eliminación de servicio de aseo urbano incorrecto', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Eliminacion / retiro de registro de publicidad comercial', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 13, 'Eliminación / retiro de registro de espectáculos públicos', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Presentacion de declaracion ISAE', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL);

INSERT INTO etax.vfm_helpdesk_specific_service
    (company_id,category_id,name,created_from,created_by,created_at,updated_at,deleted_at)
VALUES
    (1, 14, 'Presentación de declaración de inmuebles urbanos', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Presentación de declaración de vehículo', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Declaración de declaración de publicidad', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación de declaración ISAE incorrecta', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación de declaracion de inmuebles urbanos incorrecta', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación de declaración de vehículos incorrecta', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación de impuesto a la publicidad incorrecta', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación de declaración de publicidad comercial', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación de declaración de espectáculos públicos', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación de declaración de asea urbano', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL);

INSERT INTO etax.vfm_helpdesk_specific_service
    (company_id,category_id,name,created_from,created_by,created_at,updated_at,deleted_at)
VALUES
    (1, 14, 'Reclamo por recargos de declaración ISAE', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación por recargos por declaración de ISAE', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación por recargos por declaración de inmuebles urbanos', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación por recargos por declaración de publicidad comercial', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación de recargos por declaración de vehículos', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación de recargos por declaración de espectáculos públicos', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 14, 'Anulación de entrenamiento por retención ISAE', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 15, 'Envío de pagos rechazados a revisión ', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 15, 'Anulación de pagos incorrectos conciliados', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 15, 'Verificación de pagos en linea no aplicados', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL);

INSERT INTO etax.vfm_helpdesk_specific_service
    (company_id,category_id,name,created_from,created_by,created_at,updated_at,deleted_at)
VALUES
    (1, 15, 'Verificación de pago movil no registrado', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 15, 'Reporte de pago por punto de venta no aplicado', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 15, 'Reporte de transferencia bancaria a cuenta no recaudadora', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 15, 'Registro de pagos', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 16, 'Consulta de deuda tributaria', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 16, 'Convenio de pagos', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 17, 'Multas', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 17, 'Reparos', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 18, 'Cierre de sesión inesperado al seleccionar una opción', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 18, 'Error al realizar una declaración ISAE', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL);

INSERT INTO etax.vfm_helpdesk_specific_service
    (company_id,category_id,name,created_from,created_by,created_at,updated_at,deleted_at)
VALUES
    (1, 18, 'Error al realizar registro e contribuyente', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 18, 'Error al seleccionar geolocalizacion ', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 18, 'Correo de recuperación / Registro no llega por reiteradas solicitudes', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL),
    (1, 18, 'Nada Seleccionado', 'helpdesk::seed', NULL, '2024-01-24 19:16:21', '2024-01-24 19:16:21', NULL);
