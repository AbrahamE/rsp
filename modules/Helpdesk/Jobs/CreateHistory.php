<?php

namespace Modules\Helpdesk\Jobs;

use App\Abstracts\Job;
use App\Interfaces\Job\ShouldCreate;
use Illuminate\Support\Facades\DB;
use Modules\Helpdesk\Models\TicketHistory;


class CreateHistory extends Job implements ShouldCreate
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {
            $this->model = TicketHistory::create($this->request->all());
        });

        return $this->model;
    }
}
