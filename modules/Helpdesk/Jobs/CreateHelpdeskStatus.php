<?php

namespace Modules\Helpdesk\Jobs;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\HasSource;
use App\Interfaces\Job\ShouldCreate;
use Modules\Helpdesk\Models\Status;

class CreateHelpdeskStatus extends Job implements HasOwner, HasSource, ShouldCreate
{
    /**
     * Execute the job.
     *
     * @return CreateHelpdeskStatus
     */
    public function handle(): Status
    {
        \DB::transaction(function () {
            $this->model = Status::create($this->request->all());
        });

        return $this->model;
    }
}
