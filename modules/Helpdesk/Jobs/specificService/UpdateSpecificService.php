<?php

namespace Modules\Helpdesk\Jobs\specificService;

use App\Abstracts\Job;
use App\Interfaces\Job\ShouldUpdate;
use Illuminate\Support\Facades\DB;

class UpdateSpecificService extends Job implements ShouldUpdate
{
    /**
     * Execute the job.
     *
     * @return SpecificService
     */
    public function handle()
    {
        DB::transaction(function () {
            $this->model->update($this->request->all());
        });

        return $this->model;
    }
}
