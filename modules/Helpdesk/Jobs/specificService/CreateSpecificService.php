<?php

namespace Modules\Helpdesk\Jobs\specificService;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\HasSource;
use App\Interfaces\Job\ShouldCreate;
use Illuminate\Support\Facades\DB;
use Modules\Helpdesk\Models\SpecificService;

class CreateSpecificService extends Job implements HasOwner, HasSource, ShouldCreate
{
    /**
     * Execute the job.
     *
     * @return SpecificService
     */
    public function handle(): SpecificService
    {
        DB::transaction(function () {
            $this->model = SpecificService::create($this->request->all());

            // $this->model = SpecificService::create($this->request->merge([
            //     'color' => "#6040056",
            // ])->all());
        });

        return $this->model;
    }
}
