<?php

namespace Modules\Helpdesk\Observers;

use App\Traits\Jobs;
use App\Abstracts\Observer;
use Carbon\Carbon;
use Modules\Helpdesk\Models\Status;
use Modules\Helpdesk\Jobs\CreateHistory;
use Modules\Helpdesk\Models\Ticket as Model;
use Modules\Helpdesk\Notifications\Ticket as Notification;

class Ticket extends Observer
{

    use Jobs;
    /**
     * Listen to the created event.
     *
     * @param  Model $ticket
     *
     * @return void
     */
    public function created(Model $ticket)
    {
        // Do not send email if created from IMPORT
        if ($ticket->created_from == 'helpdesk::import')
            return;

        $user = $ticket->owner;
        
        $this->dispatch(new CreateHistory([
            'company_id' => company_id(),
            'ticket_id' => $ticket->id,
            'user_id' => auth()->id(),
            'description' => trans('helpdesk::general.user_created', [
                'user' => auth()->user()->name
            ]),
            'action' => 'created',
            'created_by' => auth()->id(),
            'created'    =>  Carbon::now()
        ]));

        try {
            $user->notify(new Notification($ticket, 'ticket_created'));

            // TODO: Add Helpdesk admin to receive an email when a ticket is created
        } catch (\Exception $e) {
            flash(trans('helpdesk::general.error.email_error'))->warning()->important();
        }
    }

    /**
     * Listen to the updated event.
     *
     * @param  Model $ticket
     *
     * @return void
     */
    public function updated(Model $ticket)
    {
        $originalTicket = $ticket->getOriginal();
        $modifications = $ticket->getDirty();
        $changedFields = [];

        foreach ($modifications as $key => $value) {
            $changedFields[] = [
                'key' => $key,
                $key => $originalTicket[$key],
                "update_$key" => $value,
            ];
        }

        $this->dispatch(new CreateHistory(
            [
                'company_id' => company_id(),
                'ticket_id' => $ticket->id,
                'user_id' => auth()->id(),
                'description' => trans('helpdesk::general.user_updated', [
                    'user' => auth()->user()->name
                ]),
                'action' => json_encode($changedFields),
                'created_by' => auth()->id(),
                'created'    => Carbon::now() 
            ]
        ));
        if ($ticket->status_id !== $originalTicket['status_id']) {
            $status = Status::all()->where('id', $ticket->status_id)->first();

            // Only in case the nofitication for a specific status is enabled.

            if ($status->notification) {
                $user = $ticket->owner;

                try {
                    $user->notify(new Notification($ticket, 'ticket_updated'));
                } catch (\Exception $e) {
                    flash(trans('helpdesk::general.error.email_error'))->warning()->important();
                }
            }
        }
    }
}
