<div id="widget-{{ $class->model->id }}" class="w-2/12 p-3 text-center">
    <a class="group">
        {{-- {{ $class->getDefaultSettings()['width'] }} --}}
        <div class="relative text-xl sm:text-6xl text-matisse-900 group-hover:text-matisse-900-700 mb-2">
            {{ $total ?? 0 }}
            <span class="w-8 absolute left-0 right-0 m-auto -bottom-1 bg-gray-200 transition-all group-hover:bg-gray-900" style="height: 1px;"></span>
        </div>

        <p class="font-light mt-3 block truncate">
            {{ trans_choice("helpdesk::general.status.$status", 2) }}
        </p>
    </a>
</div>
