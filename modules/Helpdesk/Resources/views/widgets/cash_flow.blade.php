<div id="widget-{{ $class->model->id }}" class="w-full my-8 px-12">
    @if ($header)
        @include($class->views['header'], ['header_class' => ''])
    @endif

    <div class="flex flex-col-reverse lg:flex-row mt-3">
        <div class="w-10/12 apex-chart-cash-flow">
            {!! $chart->container() !!}
        </div>

        <div class="w-2/12 flex flex-row lg:flex-col items-start justify-start sm:justify-start lg:mt-11 space-y-0 sm:space-y-2 overflow-y-scroll h-56 menu-tabs-scroll">
            @foreach ($totals as $company => $total)
                <div class="relative w-32 lg:w-auto flex flex-col items-center sm:justify-between">
                    <div class="flex justify-end lg:block text-sm">
                        {{ $company}}: {{ $total }}
                    </div>

                    <span class="absolute lg:relative hidden lg:block material-icons mt-2 -right-12 lg:right-0">remove</span>
                </div>
            @endforeach
        </div>
    </div>
</div>

@push('body_scripts')
    {!! $chart->script() !!}
@endpush
