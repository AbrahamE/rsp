<x-layouts.admin>
    <x-slot name="title">
        {{ trans_choice('helpdesk::general.tickets', 2) }}
    </x-slot>

    <x-slot name="buttons">
        @can('create-helpdesk-tickets')
            <x-link href="{{ route('helpdesk.tickets.create') }}" kind="primary">
                {{ trans('general.title.new', ['type' => trans_choice('helpdesk::general.tickets', 1)]) }}
            </x-link>
        @endcan

        <x-link href="{{ route('helpdesk.tickets.show', $ticket->id) }}">
            Ver tickets
        </x-link>
    </x-slot>

    <x-slot name="content">
        @if ($history->count() || request()->get('search', false))
            <x-index.container>

                <x-index.search search-string="Modules\Helpdesk\Models\TicketHistory"/>
                <x-table>
                    <x-table.thead>
                        <x-table.tr class="flex items-center px-1">
                            <x-table.th class="w-1/4">
                                <x-sortablelink column="description" title="{{ trans('general.description') }}" />
                            </x-table.th>

                            <x-table.th class="w-2/4 hidden sm:table-cell pl-2">
                                <x-sortablelink column="action" title="{{ trans('helpdesk::general.action') }}" />
                            </x-table.th>

                            <x-table.th class="w-1/4" kind="right">
                                <x-slot name="first">
                                    <x-sortablelink column="created_at"
                                        title="{{ trans('helpdesk::general.ticket.created') }}" />
                                </x-slot>
                            </x-table.th>
                        </x-table.tr>
                    </x-table.thead>

                    <x-table.tbody>
                        @foreach ($history as $historie)
                            <x-table.tr>

                                <x-table.td class="w-1/4 truncate hidden sm:table-cell">
                                    <div class="flex flex-col text-sm mb-5">
                                        {{ $historie->description }}
                                    </div>
                                </x-table.td>

                                <x-table.td class="w-2/4 truncate hidden sm:table-cell pl-2">
                                    <div class="flex items-center">
                                        <ul>
                                            @foreach (json_decode($historie->action) as $actions)
                                                @php
                                                    $key_update = 'update_' . $actions->key;
                                                    $key = $actions->key;
                                                @endphp

                                                @if ($actions->key == 'status_id')
                                                    <li class="pb-2">
                                                        <b>
                                                            {{ trans_choice('helpdesk::general.history_fields.' . $actions->key, 1) }}:
                                                        </b>
                                                        {{ strtoupper($ticket->statuses[$actions->$key]) }}
                                                    </li>
                                                    <li class="pb-2">
                                                        <b>
                                                            {{ trans_choice('helpdesk::general.history_fields.update_' . $actions->key, 1) }}:
                                                        </b>
                                                        {{ strtoupper($ticket->statuses[$actions->$key_update]) }}
                                                    </li>
                                                @elseif ($actions->key == 'updated_at')
                                                @else
                                                    <li class="pb-2">
                                                        <b>
                                                            {{ trans_choice('helpdesk::general.history_fields.' . $actions->key, 1) }}:
                                                        </b>
                                                        {{ $actions->$key }}
                                                    </li>
                                                    <li class="pb-2">
                                                        <b>
                                                            {{ trans_choice('helpdesk::general.history_fields.update_' . $actions->key, 1) }}:
                                                        </b>
                                                        {{ $actions->$key_update }}
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </x-table.td>

                                <x-table.td class="w-1/4 truncate" kind="right">
                                    <div>
                                        <x-date date="{{ $historie->created_at }}" />
                                    </div>
                                </x-table.td>
                            </x-table.tr>
                        @endforeach
                    </x-table.tbody>
                </x-table>

                {{-- <x-pagination :items="$history" /> --}}
            </x-index.container>
        @else
            <x-empty-page group="helpdesk" page="tickets" />
        @endif
    </x-slot>

    <x-script alias="helpdesk" file="settings" />
</x-layouts.admin>
