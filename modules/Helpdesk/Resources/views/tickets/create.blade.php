<x-layouts.admin>
    <x-slot name="title">{{ trans('general.title.new', ['type' => trans_choice('helpdesk::general.tickets', 1)]) }}
    </x-slot>

    <x-slot name="content">
        <x-form.container>
            <x-form id="ticket" route="helpdesk.tickets.store">
                <x-form.section>
                    <x-slot name="head">
                        <x-form.section.head title="{{ trans('general.general') }}"
                            description="{{ trans('helpdesk::general.form_description.create') }}" />
                    </x-slot>

                    <x-slot name="body">

                       

                        <x-form.group.text name="subject" label="{{ trans('helpdesk::general.ticket.subject') }}" />
                        
                        @if (user()->isAdmin())
                            <x-form.group.select name="customers"
                            label="{{ trans_choice('general.customers', 1) }}" :options="$companies"
                            form-group-class="sm:col-span-3 el-select-tags-pl-38" />
                        @endif

                        <x-form.group.select name="department_id"
                            label="{{ trans('helpdesk::general.ticket.department') }}" :options="$categories"
                            form-group-class="sm:col-span-3 el-select-tags-pl-38" change="onCategoryByDepartment" />

                        <x-form.group.select name="category_id" label="{{ trans_choice('general.categorie', 1) }}" :options="[]"
                            form-group-class="sm:col-span-3 el-select-tags-pl-38"  change="onSpecificServiceByCategory" />

                        <x-form.group.select name="specific_service"
                            label="{{ trans('helpdesk::general.ticket.specific_service') }}" :options="[]"
                            required form-group-class="sm:col-span-3 el-select-tags-pl-38" />

                        <x-form.group.select name="state_id"
                            label="{{ trans('helpdesk::general.ticket.state') }}" :options="$state"
                            form-group-class="sm:col-span-3 el-select-tags-pl-38" change="onMunicipalityByState" />

                        <x-form.group.select name="municipality_id" label="{{ trans('helpdesk::general.ticket.municipality') }}" :options="[]"
                            form-group-class="sm:col-span-3 el-select-tags-pl-38"  change="onParishByMunicipality" />

                        <x-form.group.select name="parish_id"
                            label="{{ trans('helpdesk::general.ticket.parish') }}" :options="[]"
                            required form-group-class="sm:col-span-3 el-select-tags-pl-38" change="onCommunityByParish"  />

                        <x-form.group.select name="community_id"
                            label="{{ trans('helpdesk::general.ticket.community') }}" :options="[]"
                            required form-group-class="sm:col-span-3 el-select-tags-pl-38" />

                        <div class="sm:col-span-3 el-select-tags-pl-38">
                            <x-form.label label="{{ trans('helpdesk::general.ticket.rif') }}" required>
                                {{ trans('helpdesk::general.ticket.rif') }}</x-form.label>
                            <div class="flex sm:col-span-3 el-select-tags-pl-38 ">
                                <x-form.input.select name="type_rif" form-group-class="grow-0 w-1/5 select-group"
                                    :options="['V', 'E', 'G', 'J']" required selected="0" change='onDetailsPersonByRif' />
                                <x-form.group.text form-group-class="grow input-group  rounded-none" name="rif" required  change='onDetailsPersonByRif'/>
                               
                                <button type="button" @click="onDetailsPersonByRif" class="relative flex items-center justify-center mt-1 bg-gray-200 hover:bg-gray-600 px-6 py-1 text-base rounded-r-lg disabled:bg-jaffa-100" style="height: 2.6rem;">buscar</button>
                            </div>
                        </div>

                        <x-form.group.text name="business_name" label="{{ trans('helpdesk::general.ticket.business_name') }}" />

                        <x-form.group.text name="phone" not-required  minlength="11" maxlength="11"
                        label="{{ trans('helpdesk::general.ticket.phone') }}" />

                        <x-form.group.text name="email" not-required
                            label="{{ trans('helpdesk::general.ticket.email') }}" />

                        <x-form.group.text name="additional_phone" not-required
                            label="{{ trans('helpdesk::general.ticket.additional_phone') }}" minlength="11" maxlength="11" />

                        
                        <x-form.group.select name="assignee_id" required
                            label="{{ trans('helpdesk::general.ticket.assignee') }}" :options="$assignees"
                            form-group-class="sm:col-span-3 el-select-tags-pl-38" />

                        {{-- <x-form.group.select multiple name="document_ids"
                            label="{{ trans('helpdesk::general.ticket.related_to') }}" :options="$documents" not-required
                            form-group-class="sm:col-span-3 el-select-tags-pl-38" /> --}}

                        <x-form.group.select name="status_id"
                            label="{{ trans_choice('helpdesk::general.statuses', 1) }}" :options="$statuses"
                            :selected="$status_id" form-group-class="sm:col-span-3 el-select-tags-pl-38" :readonly="true" />

                        <x-form.group.select name="priority_id"
                            label="{{ trans_choice('helpdesk::general.priorities', 1) }}" :options="$priorities"
                            :selected="$priority_id" form-group-class="sm:col-span-3 el-select-tags-pl-38" />

                        {{-- <x-form.group.select name="assignee_id"
                            label="{{ trans('helpdesk::general.ticket.assignee') }}" :options="$assignees" not-required
                            form-group-class="sm:col-span-3 el-select-tags-pl-38" /> --}}

                        
                        <x-form.group.textarea name="message"
                            label="{{ trans('helpdesk::general.ticket.observation') }}" />

                        <x-form.input.hidden name="name" />

                        <x-form.input.hidden name="company_id" :value="company_id()" />

                        <x-form.group.attachment />
                    </x-slot>
                </x-form.section>

                <x-form.section>
                    <x-slot name="foot">
                        <x-form.buttons cancel-route="helpdesk.tickets.index" />
                    </x-slot>
                </x-form.section>
            </x-form>
        </x-form.container>
    </x-slot>

    {{-- <x-script alias="helpdesk" file="tickets" /> --}}
    <x-script alias="helpdesk" file="settings" />
    @push('scripts_start')
        <script>
            const departments = @json($categories);
            const categories = @json($sub_categories);
            const statuses = @json($statuses);
            const priorities = @json($priorities);
            const assignees = @json($assignees);
            const documents = @json($documents);
        </script>
    @endpush
</x-layouts.admin>
