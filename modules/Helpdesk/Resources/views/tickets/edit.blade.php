<x-layouts.admin>
    <x-slot
        name="title">{{ trans('general.title.edit', ['type' => trans_choice('helpdesk::general.tickets', 1)]) . ' ' . $ticket->name }}
    </x-slot>

    <x-slot name="content">
        <x-form.container>
            <x-form id="ticket" method="PATCH" :route="['helpdesk.tickets.update', $ticket->id]" :model="$ticket">
                <x-form.section>
                    <x-slot name="head">
                        <x-form.section.head title="{{ trans('general.general') }}"
                            description="{{ trans('helpdesk::general.form_description.show') }}" />
                    </x-slot>

                    <x-slot name="body">
                        <div class="sm:col-span-2 flex flex-col text-sm mb-5">
                            <div class="font-medium">{{ trans('helpdesk::general.ticket.created') }}</div>
                            <x-date date="{{ $ticket->created_at }}" />{{ $ticket->created_at->format('H:i') }}
                        </div>

                        <div class="sm:col-span-2 flex flex-col text-sm mb-5">
                            <div class="font-medium">{{ trans('helpdesk::general.ticket.updated') }}</div>
                            <x-date date="{{ $ticket->updated_at }}" />{{ $ticket->updated_at->format('H:i') }}
                        </div>

                        <div class="sm:col-span-2 flex flex-col text-sm mb-5">
                            <div class="font-medium">{{ trans('helpdesk::general.ticket.reporter') }}</div>
                            <span>{{ $ticket->owner->name }}</span>
                        </div>
                    </x-slot>
                </x-form.section>

                <x-form.section>
                    <x-slot name="head">
                        <x-form.section.head title="{{ trans('helpdesk::general.details') }}"
                            description="{{ trans('helpdesk::general.form_description.edit') }}" />
                    </x-slot>

                    <x-slot name="body">

                        <x-form.group.text name="subject" label="{{ trans('helpdesk::general.ticket.subject') }}" />

                        @if (user()->isAdmin())
                            <x-form.group.select name="customers"
                                label="{{ trans_choice('general.customers', 1) }}" :options="$companies"
                                form-group-class="sm:col-span-3 el-select-tags-pl-38" :value="$ticket->customers"/>
                        @endif

                        <x-form.group.select name="department_id"
                            label="{{ trans('helpdesk::general.ticket.department') }}" :options="$categories"
                            form-group-class="sm:col-span-3 el-select-tags-pl-38" change="onCategoryByDepartment" />

                        <x-form.group.select name="category_id" label="{{ trans_choice('general.categorie', 1) }}" :options="$sub_categories"
                            form-group-class="sm:col-span-3 el-select-tags-pl-38"  change="onSpecificServiceByCategory" />

                        <x-form.group.select name="specific_service"
                            label="{{ trans('helpdesk::general.ticket.specific_service') }}" :options="$specific_service"
                            not-required form-group-class="sm:col-span-3 el-select-tags-pl-38" :value="$ticket->specific_service" />

                        <x-form.group.select name="state_id"
                            label="{{ trans('helpdesk::general.ticket.state') }}" :options="$state" :value="$ticket->state"
                            form-group-class="sm:col-span-3 el-select-tags-pl-38" change="onMunicipalityByState" />

                        <x-form.group.select name="municipality_id" label="{{ trans('helpdesk::general.ticket.municipality') }}" :options="$municipality" :value="$ticket->municipality"
                            form-group-class="sm:col-span-3 el-select-tags-pl-38"  change="onParishByMunicipality" />

                        <x-form.group.select name="parish_id"
                            label="{{ trans('helpdesk::general.ticket.parish') }}" :options="$parish" :value="$ticket->parish"
                            required form-group-class="sm:col-span-3 el-select-tags-pl-38" change="onCommunityByParish"  />

                        <x-form.group.select name="community_id"
                            label="{{ trans('helpdesk::general.ticket.community') }}" :options="$community" :value="$ticket->community"
                            required form-group-class="sm:col-span-3 el-select-tags-pl-38" />
                        
                        <div class="sm:col-span-3 el-select-tags-pl-38">
                            <x-form.label label="{{ trans('helpdesk::general.ticket.rif') }}" required>
                                {{ trans('helpdesk::general.ticket.rif') }}</x-form.label>
                            <div class="flex sm:col-span-3 el-select-tags-pl-38 ">
                                <x-form.input.select name="type_rif" form-group-class="grow-0 w-1/5 select-group"
                                    :options="['V', 'E', 'G', 'J']" required selected="0" change='onDetailsPersonByRif' />
                                <x-form.group.text form-group-class="grow input-group  rounded-none" name="rif" required  change='onDetailsPersonByRif'/>
                               
                                <button type="button" @click="onDetailsPersonByRif" class="relative flex items-center justify-center mt-1 bg-gray-200 hover:bg-gray-600 px-6 py-1 text-base rounded-r-lg disabled:bg-jaffa-100">buscar</button>
                            </div>
                        </div>

                        <x-form.group.text name="business_name" label="{{ trans('helpdesk::general.ticket.business_name') }}" />

                        {{-- <x-form.group.select multiple name="document_ids"
                            label="{{ trans('helpdesk::general.ticket.related_to') }}" :options="$documents" not-required
                            form-group-class="sm:col-span-3 el-select-tags-pl-38" /> --}}

                        <x-form.group.select name="assignee_id"
                            label="{{ trans('helpdesk::general.ticket.assignee') }}" :options="$assignees" not-required
                            form-group-class="sm:col-span-3 el-select-tags-pl-38" />

                        <x-form.group.select name="status_id"
                            label="{{ trans_choice('helpdesk::general.statuses', 1) }}" :options="$statuses"
                            :selected="$ticket->status_id" form-group-class="sm:col-span-3 el-select-tags-pl-38" :readonly="true" />

                        <x-form.group.select name="priority_id"
                            label="{{ trans_choice('helpdesk::general.priorities', 1) }}" :options="$priorities"
                            :selected="$priority_id" form-group-class="sm:col-span-3 el-select-tags-pl-38" />

                        <x-form.group.text name="phone" not-required
                            label="{{ trans('helpdesk::general.ticket.phone') }}" />

                        <x-form.group.text name="additional_phone" not-required
                            label="{{ trans('helpdesk::general.ticket.additional_phone') }}" />

                        <x-form.group.text name="email" not-required
                            label="{{ trans('helpdesk::general.ticket.email') }}" />

                        <x-form.group.textarea name="message"
                            label="{{ trans('helpdesk::general.ticket.observation') }}" />

                        <x-form.group.attachment />


                        <x-form.input.hidden name="category_id" v-value="form.category_id" />
                        <x-form.input.hidden name="specific_service" v-value="form.specific_service" />
                        <x-form.input.hidden name="state_id" v-value="form.state" />
                        <x-form.input.hidden name="municipality_id" v-value="form.municipality" />
                        <x-form.input.hidden name="parish_id" v-value="form.parish" />
                        <x-form.input.hidden name="community_id" v-value="form.community" />
                    

                    </x-slot>
                </x-form.section>

                @can('update-helpdesk-tickets')
                    <x-form.section>
                        <x-slot name="foot">
                            <x-form.buttons cancel-route="helpdesk.tickets.index" />
                        </x-slot>
                    </x-form.section>
                @endcan
            </x-form>
        </x-form.container>
    </x-slot>

    <x-script alias="helpdesk" file="settings" />
    @push('scripts_start')
        <script>
            const departments = @json($categories);
            const categories = @json($sub_categories);
            const statuses = @json($statuses);
            const priorities = @json($priorities);
            const assignees = @json($assignees);
            const documents = @json($documents);
        </script>
    @endpush
</x-layouts.admin>
