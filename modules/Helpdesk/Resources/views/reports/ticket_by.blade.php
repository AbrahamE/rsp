 @foreach ($class->tickets as $department => $tickets)
     <h1 class="bg-black-100 py-5 pl-3">{{ $department }}: {{ $tickets->count() }}</h1>
     <x-table>
         <x-table.thead>

             <x-table.tr class="flex items-center px-1">

                 <x-table.th class="w-2/12">
                     <x-sortablelink column="subject" title="{{ trans('helpdesk::general.ticket.subject') }}" />
                 </x-table.th>

                 <x-table.th class="w-4/12 hidden sm:table-cell">
                     <x-slot name="first">
                         <x-sortablelink column="category.department"
                             title="{{ trans_choice('helpdesk::general.ticket.department', 1) }}" />
                     </x-slot>
                     <x-slot name="second" class="flex">
                         <x-sortablelink column="category.categories"
                             title="{{ trans_choice('general.categorie', 1) }}" />
                    </x-slot>
                        <x-sortablelink column="category.specific_service"
                            title="{{ trans_choice('helpdesk::general.ticket.specific_service', 1) }}" />
                 </x-table.th>

                 <x-table.th class="w-2/12">
                     <x-slot name="first">
                         <x-sortablelink column="owner.name" title="{{ trans('helpdesk::general.ticket.reporter') }}" />
                     </x-slot>
                     <x-slot name="second">
                         <x-sortablelink column="assignee.name"
                             title="{{ trans('helpdesk::general.ticket.assignee') }}" />
                     </x-slot>
                 </x-table.th>

                 <x-table.th class="w-2/12 hidden sm:table-cell">
                     <x-sortablelink column="status.name" title="{{ trans('helpdesk::general.ticket.status') }}" />
                 </x-table.th>

                 <x-table.th class="w-2/12">
                     <x-slot name="first">
                         <x-sortablelink column="created_at" title="{{ trans('helpdesk::general.ticket.created') }}" />
                     </x-slot>
                     <x-slot name="second">
                         <x-sortablelink column="updated_at" title="{{ trans('helpdesk::general.ticket.updated') }}" />
                     </x-slot>
                 </x-table.th>

             </x-table.tr>
         </x-table.thead>

         <x-table.tbody>
             @foreach ($tickets as $ticket)
                 <x-table.tr href="{{ route('helpdesk.tickets.show', $ticket->id) }}">

                     <x-table.td class="w-2/12 truncate">
                         <p>
                             <b> {{ $ticket->name }} </b> {{ $ticket->subject }}
                         </p>
                     </x-table.td>

                     <x-table.td class="w-4/12 truncate hidden sm:table-cell">
                         <x-slot name="first" class="flex items-center font-bold" override="class">
                             <x-index.category width="w-24" :model="$ticket->department" />
                         </x-slot>
                         <x-slot name="second">
                             <x-index.category width="w-24" :model="$ticket->category" />
                             <x-index.category width="w-24" :model="$ticket->service" />
                         </x-slot>
                     </x-table.td>

                     <x-table.td class="w-2/12 truncate">
                         <x-slot name="first" class="flex items-center font-bold" override="class">
                             @isset($ticket->owner->id)
                                 <a href="{{ route('users.edit', $ticket->owner->id) }}">{{ $ticket->owner->name }}</a>
                             @else
                                 {{ $ticket->owner->name }}
                             @endisset
                         </x-slot>
                         <x-slot name="second">
                             @if (isset($ticket->assignee->id))
                                 <a
                                     href="{{ route('users.edit', $ticket->assignee->id) }}">{{ $ticket->assignee->name }}</a>
                             @else
                                 {{ $ticket->assignee->name }}
                             @endif
                         </x-slot>
                     </x-table.td>

                     <x-table.td class="w-2/12 truncate hidden sm:table-cell">
                         <div class="flex items-center">
                             <x-index.category :model="$ticket->status" />
                         </div>
                     </x-table.td>

                     <x-table.td class="w-2/12 truncate">
                         <x-slot name="first" class="flex items-center font-bold" override="class">
                             <x-date date="{{ $ticket->created_at }}" />
                         </x-slot>
                         <x-slot name="second">
                             <x-date date="{{ $ticket->updated_at }}" />
                         </x-slot>
                     </x-table.td>
                 </x-table.tr>
             @endforeach
         </x-table.tbody>
     </x-table>
 @endforeach
