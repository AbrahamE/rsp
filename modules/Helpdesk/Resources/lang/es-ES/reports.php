<?php

return [

    'ticket_name'           => 'Resumen de Incidencias',

    'ticket_name_s'         => 'Resumen de Incidencia/Estado',
    'ticket_description_s'  => 'Resumen mensual de incidencias por Estado.',

    'ticket_name_category'         => 'Resumen de Incidencia/Categoría',
    'ticket_description_category'  => 'Resumen mensual de incidencias por Categoría.',

    'ticket_name_department'         => 'Resumen de Incidencia/Departamento',
    'ticket_description_department'  => 'Resumen mensual de incidencias por Departamento.',

    'ticket_name_service'         => 'Resumen de Incidencia/Servicio especifico',
    'ticket_description_service'  => 'Resumen mensual de incidencias por Servicio especifico.',

    'ticket_name_assignee'         => 'Resumen de Incidencia/Asignado',
    'ticket_description_assignee'  => 'Resumen mensual de incidencias/assignee.',

];
