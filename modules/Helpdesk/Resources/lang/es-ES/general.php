<?php

return [

    'name'                  => 'Módulo de Soporte',
    'name_dashboard'        => 'Soporte',
    'message'               => 'Gestiona las incidencias de tus clientes y manténlas actualizadas.',
    'history'               => 'Historico',
    'created'               => 'Usario ah creado el ticket',
    'updated'               => 'Usario ah modificado el ticket',
    
    'empty' => [
        'tickets'           => 'Puedes crear incidencias para informar de errores, problemas o peticiones. Será asignada a un agente para poder resolverla lo antes possible',
    ],
    
    'tickets'               => 'tickets|tickets',
    'created_at'            => 'Fecha',
    
    'user_created'          => 'El Usario ( :user ) ah creado el ticket',
    'user_updated'          => 'El Usario ( :user ) ah actualizado el ticket',

    'action'                => 'Accion',
    'fields' => [
        'status'            => 'Estatus',
        'date'              => 'Fecha',
    ],

    'ticket' => [
        'id'                => 'Identificación',
        'name'              => 'Nombre',
        'subject'           => 'Asunto',
        'customers'         => 'Cliente',
        'message'           => 'Mensaje',
        'observation'       => 'Observacion',
        'category'          => 'Categoria',
        'reporter'          => 'Informador',
        'assignee'          => 'Asignado a',
        'specific_service'  => 'Servicio especifico',
        'status'            => 'Estatus',
        'created'           => 'Creada',
        'updated'           => 'Actualizada',
        'related_to'        => 'Relacionado con',
        'not_related'       => 'No relacionado con ningún documento',
        'department'        => 'Área',
        'rif'               => 'Numero de rif',
        'phone'             => 'Télefono',
        'email'             => 'Correo electronico',
        'business_name'     => 'Razon social',
        'additional_phone'  => 'Télefono adicional',
        'state'             => 'Estado',
        'municipality'      => 'Municipio',
        'parish'            => 'Parroquia', 
        'community'         => 'Comunidad'
    ],

    'history_fields'               => [
        'status_id'         => 'Estatus',
        'update_status_id'  => 'Estatus actualizado',
        'name'              => 'Nombre',
        'update_name'       => 'Nombre actualizado',
        'subject'           => 'Asunto',
        'update_subject'    => 'Asunto actualizado',
        'customers'         => 'Cliente',
        'update_customers'  => 'Cliente actualizado',
        'message'           => 'Mensaje',
        'update_message'    => 'Correo electronico actualizado',
        'observation'       => 'Observacion',
        'update_observation' => 'Observacion actualizada',
        'category'          => 'Categoria',
        'update_category'   => 'Categoria actualizada',
        'reporter'          => 'Informador',
        'update_reporter'   => 'Informador actualizado',
        'assignee'          => 'Asignado',
        'update_assignee'   => 'Asignado actualizado',
        'specific_service'  => 'Servicio especifico',
        'update_specific_service' => 'Servicio especifico actualizado',
        'department'        => 'Área',
        'update_department' => 'Área actualizada',
        'rif'               => 'Numero de rif',
        'update_rif'        => 'Numero de rif actualizado',
        'phone'             => 'Télefono',
        'update_phone'      => 'Télefono actualizado',
        'email'             => 'Correo electronico',
        'update_email'      => 'Correo electronico actualizado',
        'business_name'     => 'Razon social',
        'update_business_name' => 'Razon social actualizada',
        'additional_phone'  => 'Télefono adicional',
        'update_additional_phone' => 'Télefono adicional actualizado',
        'state_id'             => 'Estado',
        'update_state_id'      => 'Estado actualizado',
        'municipality_id'      => 'Municipio',
        'update_municipality_id' => 'Municipio actualizado',
        'parish_id'            => 'Parroquia',
        'update_parish_id'     => 'Parroquia actualizada',
        'community_id'         => 'Comunidad',
        'update_community_id'  => 'Comunidad actualizada'
    ],

    '_category' => [ // Avoids conflict with 'category' or 'categories' in search-string.php
        'address_registry_taxpayers_and_obligations'    => 'Dirección de Registro de contribuyentes y obligaciones',
        'tax_accounting_department'                     => 'Dirección de contabilidad fiscal',
        'collection_department'                         => 'Dirección de Cobranza',
        'supervision_directorate'                       => 'Dirección de Fiscalización',
        'sigat_support'                                 => 'Soporte SIGAT',
    ],
    '_sub_category' => [ // Avoids conflict with 'category' or 'categories' in search-string.php
        'user_registration'                 => "Registro de usuario",
        'request_management'                => "Gestión de solicitudes",
        'management_tax_obligation_records' => "Gestión de registros de obligaciones tributarias",
        'declaration_management'            => "Gestión de declaraciones",
        'payment_management'                => "Gestión de pagos",
        'collection'                        => "Cobranza",
        'inspection'                        => "Fiscalización",
        'technical_incidents'               => "Incidencias tecnicas",
    ],

    'statuses'              => 'Estado|Estados',

    'status' => [
        'open'              => 'Abierto',
        'in_progress'       => 'En progreso',
        'on_hold'           => 'En espera',
        'answered'          => 'Contestado',
        'closed'            => 'Cerrado',
        'spam'              => 'Spam',
        'earrings'          => 'Pendiente',
        'solved'            => 'Solventado',
    ],

    'replies'                 => 'Respuesta|Respuestas',

    'reply' => [
        'internal_note'     => 'Nota interna',
        'message'           => 'Mensaje',
        'new_reply'         => 'Nueva respuesta',
    ],

    'priorities'            => 'Prioridad|Prioridades',

    'priority' => [
        'urgent'            => 'Urgente',
        'high'              => 'Alta',
        'medium'            => 'Media',
        'low'               => 'Baja',
    ],

    'created_at'            => 'Fecha de creación',
    'updated_at'            => 'Última actualización',

    'reporters'             => 'Solicitante|Solicitantes',
    'assignees'             => 'Responsable|Responsables',
    

    'error' => [
        'email_error'       => 'El correo no se ha enviado. Por favor, revisa la configuración del correo.',
    ],

    'form_description' => [
        'create'            => 'Entra la descripción de la incidencia, la categoría y otros detalles para hacer el seguimiento.',
        'edit'              => 'Puedes editar la descripción de la incidencia, la categoría y otros detalles para actualizar su estado.',
        'show'              => 'Cuando se creó la incidencia y por quién.',
    ],


    'download'              => 'Descarga los ficheros asociados a esta incidencia',

    'details'               => 'Details',


    'widgets'               => [
        'tickets_by_department'      => 'Incidencias por Àreas',
        'tickets_by_specific_service' => 'Incidencias por servicio especifico',
        'tickets_line_chart'          => 'Incidencias por compañia',
    ]
];
