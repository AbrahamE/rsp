<?php

return [

    'ticket_created' => [
        'subject'   => 'Nueva incidencia {name} creada',
        'body'      => 'Estimado/a {reporter},<br /><br />Has creado una nueva incidencia con el asunto: {subject}.<br /><br />Descripción:<br />{description}<br /><br />Saludos cordiales,<br />{company_name}',
    ],

    'ticket_updated' => [
        'subject'   => 'El estado de la incidencia {name} ha cambiado a {status}',
        'body'      => 'Estimado/a {reporter},<br /><br />El estado de la incidencia <i>{name} {subject}</i> ha cambiado a <strong>{status}</strong>. Por favor, revisa la incidencia actualizada aquí: {route}.<br /><br />Saludos cordiales,<br />{company_name}',
    ],

    'reply_created' => [
        'subject'   => 'Nueva respuesta en la incidencia {ticket_name}',
        'body'      => 'Estimado/a {reporter},<br /><br />Hay una nueva respuesta de {reply_author} relacionada con la incidencia <i>{ticket_name} {ticket_subject}</i>.<br /><br />Mensaje de la respuesta:<br />{message}<br /><br />Fecha de la respuesta: {reply_created_at}<br /><br />Puedes revisarla en el siguiente enlace: {route}.<br /><br />Saludos cordiales,<br />{company_name}',
    ],
    
];
