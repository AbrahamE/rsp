<?php

return [

    'tickets_by_category'           => 'Incidencias por Categoría',
    'tickets_by_department'        => "Incidencias por  Área",
    'tickets_by_specific_service'   => "Incidencias por  Servicio especifico",
    

    'tickets_by_status'             => 'Incidencias por Estado',
    'latest_tickets'                => 'Últimas incidencias',
    'top_reporters'                 => 'Máximos Informadores',
    'top_assignees'                 => 'Máximos Responsables',

    'description' => [
        'tickets_by_category'           => 'Lista de incidencias por categoría',
        'tickets_by_status'             => 'Lista de incidencias por estado',
        'latest_tickets'                => 'Lista de las últimas incidencias',
        'top_reporters'                 => 'Lista de los informadores con más incidencias',
        'top_assignees'                 => 'Lista de empleados con más incidencias asignadas',
        'tickets_by_department'        => "Lista de incidencias por Área",
        'tickets_by_specific_service'   => "Lista de incidencias por  Servicio especifico",
    ],

];
