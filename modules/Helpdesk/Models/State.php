<?php

namespace Modules\Helpdesk\Models;

use App\Abstracts\Model;

class State extends Model
{
    protected $table = 'state';

    protected $fillable = [
        'code',
        'name',
    ];

    public function municipality()
    {
        return $this->hasMany('Modules\Helpdesk\Models\Municipality', 'state_code', 'code');
    }

}
