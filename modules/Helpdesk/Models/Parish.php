<?php

namespace Modules\Helpdesk\Models;

use App\Abstracts\Model;

class Parish extends Model
{

    protected $table = 'parish';

    protected $fillable = [
        'code',
        'name',
        'municipality_code'
    ];

    public function community()
    {
        return $this->hasMany('Modules\Helpdesk\Models\Community', 'parish_code', 'code');
    }
}
