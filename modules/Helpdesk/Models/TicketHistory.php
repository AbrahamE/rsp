<?php

namespace Modules\Helpdesk\Models;

use App\Abstracts\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TicketHistory extends Model
{
    use HasFactory;

    protected $table = 'helpdesk_history';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['company_id', 'ticket_id', 'description', 'user_id', 'action'];

    /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['company_id', 'ticket_id', 'description', 'user_id', 'action', 'created_at'];

    protected $casts = [
        'created' => 'datetime',
    ];
    /**
     * Get the line actions.
     *
     * @return array
     */

    public function setCreatedAtAttribute($value)
    {
        $this->attributes['created'] = Carbon::parse($value);
        $this->attributes['created_at'] = Carbon::parse($value);
    }

    public function getLineActionsAttribute()
    {
        $actions = [];

        $actions[] = [
            'title' => trans('general.edit'),
            'icon' => 'create',
            'url' => route('categories.department.specificService.edit', [
                'specific_service' => $this->id,
                'department' => $this->category_id
            ]),
            'permission' => 'update-settings-categories',
            'attributes' => [
                'id' => 'index-line-actions-edit-category-' . $this->id,
            ],
        ];

        $actions[] = [
            'type' => 'delete',
            'icon' => 'delete',
            'route' => 'categories.department.specificService.delete',
            'permission' => 'delete-settings-categories',
            'attributes' => [
                'id' => 'index-line-actions-delete-category-' . $this->id,
            ],
            'model' => $this,
        ];

        return $actions;
    }

    public function getActionTextAttribute()
    {
        return collect($this->action ?? [])->map(
            fn($value, $field) => trans_choice(
                "helpdesk::general.field.$field",
                1
            ) .
                ": " . $this->execTransformFieldWhenExist($field, $value)
        )->join(", ");
    }

    public function execTransformFieldWhenExist($field, $value)
    {
        $transformFunction = "transform" . ucfirst(Str::camel($field));
        $existTransform = method_exists($this, $transformFunction);

        if ($existTransform) {
            return $this->{$transformFunction}($value);
        }

        return $value;
    }
}
