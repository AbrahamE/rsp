<?php

namespace Modules\Helpdesk\Models\Portal;

use App\Abstracts\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Media;
use App\Models\Common\Media as MediaModel;

class Ticket extends Model
{
    use HasFactory, Media;

    protected $table = 'helpdesk_tickets';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['document_ids', 'statuses'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'subject',
        'customers',
        'company_id',
        'department_id',
        'specific_service',
        'state_id',
        'municipality_id',
        'parish_id',
        'community_id',
        'rif',
        'business_name',
        'type_rif',
        'phone',
        'additional_phone',
        'email',
        'message',
        'category_id',
        'status_id',
        'priority_id',
        'assignee_id',
        'started_at',
        'created_from',
        'created_by'
    ];

    /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['name', 'subject', 'company_id', 'department_id', 'rif', 'type_rif', 'phone', 'email', 'category.name', 'status.name', 'priority_id', 'owner.name', 'assignee.name', 'created_at'];


    public function category()
    {
        return $this->belongsTo('App\Models\Setting\Category')->withDefault(['name' => trans('general.na')])->withSubCategory();
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Setting\Category', 'department_id', 'id')->withDefault(['name' => trans('general.na')]);
    }

    public function service()
    {
        return $this->belongsTo('Modules\Helpdesk\Models\SpecificService', 'specific_service', 'id');
    }

    public function state()
    {
        return $this->belongsTo('Modules\Helpdesk\Models\State', 'state_id', 'id');
    }

    public function municipality()
    {
        return $this->belongsTo('Modules\Helpdesk\Models\Municipality', 'municipality_id', 'id');
    }

    public function parish()
    {
        return $this->belongsTo('Modules\Helpdesk\Models\Parish', 'parish_id', 'id');
    }
    
    public function community()
    {
        return $this->belongsTo('Modules\Helpdesk\Models\Community', 'community_id', 'id');
    }
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function priority()
    {
        return $this->belongsTo(Priority::class);
    }

    public function assignee()
    {
        return $this->belongsTo('App\Models\Auth\User')->withDefault(['name' => trans('general.na')]);
    }

    public function documents()
    {
        return $this->hasMany('Modules\Helpdesk\Models\TicketDocument');
    }

    /**
     * Accessors to provide default value on null
     */
    public function getStatusIdAttribute($value)
    {
        return $value ?? Status::getFirstStatusID();
    }

    public function getPriorityIdAttribute($value)
    {
        return $value ?? Priority::getDefaultPriorityID();
    }

    /**
     * Name accessor. Returns ticket name (dynamically generated)
     * 
     * @return String
     */
    public function getNameAttribute($value)
    {
        // In first versions the id was used as 'name'.
        // To keep compatibility we check for empty 'name' property.
        return '#' . (int) $value;
    }

    /**
     * Name mutator. Returns ticket name (dynamically generated)
     * 
     * @return string
     */
    public function setNameAttribute($value = null)
    {
        $value = (int) setting('helpdesk.tickets.next'); // 1 if module was just installed

        // Add 0s to allow Natural Sorting
        $this->attributes['name'] = str_pad($value, 10, '0', STR_PAD_LEFT);

        setting([
            'helpdesk.tickets.next' => ($value + 1) // Next ticket
        ])->save();
    }

    /**
     * Functions required to preload/delete attachments in tickets
     */
    public function getAttachmentAttribute($value = null)
    {
        if (!empty($value) && !$this->hasMedia('attachment')) {
            return $value;
        } elseif (!$this->hasMedia('attachment')) {
            return false;
        }

        return $this->getMedia('attachment')->all();
    }

    public function delete_attachment()
    {
        $attachments = $this->attachment;

        if (!empty($attachments)) {
            foreach ($attachments as $file) {
                MediaModel::where('id', $file->id)->delete();
            }
        }
    }

    /**
     * Get the document ids.
     *
     * @return string
     */
    public function getDocumentIdsAttribute()
    {
        return $this->documents()->pluck('document_id');
    }

    /**
     * Get the document ids.
     *
     * @return string
     */
    public function getStatusesAttribute()
    {
        $statuses = Status::all()->pluck('name', 'id');
        return $statuses;
    }

    /**
     * Get the document ids.
     *
     * @return int
     */
    public function getTotalTicketsCompany($company)
    {
        return $this::where('company_id', $company)->count();
    }

    /**
     * Get the line actions.
     *
     * @return array
     */
    public function getLineActionsAttribute()
    {
        $actions = [];

        $actions[] = [
            'title' => trans('general.edit'),
            'icon' => 'edit',
            'url' => route('helpdesk.tickets.edit', $this->id),
            'permission' => 'update-helpdesk-tickets',
        ];

        $actions[] = [
            'type' => 'delete',
            'title' => trans_choice('helpdesk::general.tickets', 1),
            'icon' => 'delete',
            'route' => 'helpdesk.tickets.destroy',
            'permission' => 'delete-helpdesk-tickets',
            'model' => $this,
        ];

        return $actions;
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return \Modules\Helpdesk\Database\Factories\Ticket::new();
    }
}
