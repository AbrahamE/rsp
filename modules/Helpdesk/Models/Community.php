<?php

namespace Modules\Helpdesk\Models;

use App\Abstracts\Model;

class Community extends Model
{
    protected $table = 'community';
    
    protected $fillable = [
        'code',
        'name',
        'parish_code'
    ];
}
