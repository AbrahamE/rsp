<?php

namespace Modules\Helpdesk\Models;

use App\Abstracts\Model;

class Municipality extends Model
{
    protected $table = 'municipality';

    protected $fillable = [
        'code',
        'name',
        'state_code'
    ];

    public function parish()
    { 
        return $this->hasMany('Modules\Helpdesk\Models\Parish', 'municipality_code', 'code');
    }
}
