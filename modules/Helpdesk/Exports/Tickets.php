<?php

namespace Modules\Helpdesk\Exports;

use App\Abstracts\Export;
use Modules\Helpdesk\Models\Ticket as Model;

class Tickets extends Export
{
    public $type_rif =  ['V', 'E', 'G', 'J'];
    public function collection()
    {
        return Model::with('category','department','service', 'status', 'priority', 'owner')->collectForExport($this->ids);
    }

    public function map($model): array
    {
       
        $model->N = $model->name;
       // $model->cliente = $model->category->customer;
        $model->area = $model->category->name;
        $model->categoria = $model->department->name;
        $model->servicio_especifico = $model->service->name;
        $model->nombre = $model->subject;
        $model->rif = $this->type_rif[$model->type_rif] . "-" . $model->rif;
        $model->telefono = $model->phone;
        $model->correo = $model->email;
        $model->observacion = $model->message;
        $model->nombre_del_autor = $model->owner->name;
        $model->status = $model->status->name;
        $model->prioridad = $model->priority->name;
        $model->asignado = $model->assignee->name;
        $model->creado = $model->created_at->format('d/m/Y H:i');
        $model->actualizado_en = $model->updated_at->format('d/m/Y H:i');

        return parent::map($model);
    }

    public function fields(): array
    {
        return [
            'N',
            'area',
            'categoria',
            'servicio_especifico',
            'Asunto',
            'rif',
            'telefono',
            'correo',
            'observacion',
            'nombre_del_autor',
            'status',
            'prioridad',
            'asignado',
            'creado',
            'actualizado_en'
        ];
    }
}
