<?php

namespace Modules\Helpdesk\Notifications;

use App\Abstracts\Notification;
use App\Models\Setting\EmailTemplate;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Helpdesk\Models\Reply as Model;

class Reply extends Notification
{
    /**
     * The reply model.
     *
     * @var Model
     */
    public $reply;

    /**
     * The email template.
     *
     * @var EmailTemplate
     */
    public $template;

    public function __construct($reply = null, $template_alias = null)
    {
        parent::__construct();

        $this->reply = $reply;

        $this->template = EmailTemplate::alias($template_alias)->first();
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail($notifiable): MailMessage
    {
        $message = $this->initMailMessage();

        return $message;
    }

    /**
     * Get the array representation of the notification.
     */
    public function toArray($notifiable): array
    {
        $this->initArrayMessage();

        return [
            'template_alias'  => $this->template->alias,
            'title'           => trans('helpdesk::notifications.menu.' . $this->template->alias . '.title'),
            'description'     => trans('helpdesk::notifications.menu.' . $this->template->alias . '.description', $this->getTagsBinding()),
            'reporter'        => $this->reply->ticket->owner->name,
            'message'         => $this->reply->message,
            'ticket_name'     => $this->reply->ticket->name,
            'ticket_subject'  => $this->reply->ticket->subject,
            'company_name'    => company()->name,
            'route'           => route('helpdesk.tickets.show', $this->reply->ticket->id),
            'reply_author'    => $this->reply->owner->name, // Agregado el autor de la respuesta
            'reply_created_at'=> $this->reply->created_at->format('Y-m-d H:i:s'), // Agregada fecha de creación de la respuesta
        ];
    }

    public function getTags(): array
    {
        return [
            '{reporter}',          // Reportero del ticket
            '{message}',  
            '{description}',         // Mensaje de la respuesta
            '{company_name}',       // Nombre de la compañía
            '{ticket_name}',       // Nombre del ticket
            '{ticket_subject}',    // Asunto del ticket
            '{route}',             // Enlace al ticket
            '{reply_author}',      // Autor de la respuesta
            '{reply_created_at}',  // Fecha de creación de la respuesta
        ];
    }

    public function getTagsReplacement(): array
    {
        return [
            $this->reply->ticket->owner->name,               // Reportero del ticket
            $this->reply->message, 
            $this->reply->ticket->message,                          // Mensaje de la respuesta
            company()->name,                                 // Nombre de la compañía
            $this->reply->ticket->name,                      // Nombre del ticket
            $this->reply->ticket->subject,                   // Asunto del ticket
            route('helpdesk.tickets.show', $this->reply->ticket->id), // Enlace al ticket
            $this->reply->owner->name,                       // Autor de la respuesta
            $this->reply->created_at->format('Y-m-d H:i:s'), // Fecha de creación de la respuesta
        ];
    }
}
