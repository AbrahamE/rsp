<?php

namespace Modules\Helpdesk\Notifications;

use App\Abstracts\Notification;
use App\Models\Setting\EmailTemplate;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Helpdesk\Models\Ticket as Model;

class Ticket extends Notification
{
    /**
     * The ticket model.
     *
     * @var Model
     */
    public $ticket;

    /**
     * The email template.
     *
     * @var EmailTemplate
     */
    public $template;

    public function __construct($ticket = null, $template_alias = null)
    {
        parent::__construct();

        $this->ticket = $ticket;
        $this->template = EmailTemplate::alias($template_alias)->first();
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail($notifiable): MailMessage
    {
        $message = $this->initMessage();

        return $message;
    }

    /**
     * Get the array representation of the notification.
     */
    public function toArray($notifiable): array
    {
        $this->initArrayMessage();

        return [
            'template_alias' => $this->template->alias,
            'name'           => $this->ticket->name,
            'subject'        => $this->ticket->subject,
            'reporter'       => $this->ticket->owner->name,
            'description'    => $this->ticket->message,
            'status'         => $this->ticket->statuses[$this->ticket->status_id],
            'company_name'   => company()->name,
            'route'          => route('helpdesk.tickets.show', $this->ticket->id)
        ];
    }

    public function getTags(): array
    {
        return [
            '{template_alias}',
            '{name}',
            '{subject}',
            '{reporter}',
            '{description}',
            '{status}',
            '{company_name}',
            '{route}'
        ];
    }

    public function getTagsReplacement(): array
    {
        return [
            $this->template->alias,
            $this->ticket->name,
            $this->ticket->subject,
            $this->ticket->owner->name,
            $this->ticket->message,
            $this->ticket->statuses[$this->ticket->status_id],
            company()->name,
            route('helpdesk.tickets.show', $this->ticket->id)
        ];
    }
}
