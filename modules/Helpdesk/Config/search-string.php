<?php

return [

    'Modules\Helpdesk\Models\Ticket' => [
        'columns' => [
            'name' => ['searchable' => true],
            'subject' => ['searchable' => true],
            'message' => ['searchable' => true],
            'category_id' => [
                'route' => ['helpdesk.tickets.categorys_all', 'search=type:ticket'],
            ],
            'department_id' => [
                'route' => ['helpdesk.tickets.categorys', 'search=type:ticket'],
            ],
            'specific_service' => [
                'route' => ['helpdesk.tickets.specific_service', 'search=type:ticket'],
            ],
            'state' => [
                'route' => ['helpdesk.tickets.state', 'search=type:ticket'],
            ],
            'municipality' => [
                'route' => ['helpdesk.tickets.municipality', 'search=type:ticket'],
            ],
            'parish' => [
                'route' => ['helpdesk.tickets.parish', 'search=type:ticket'],
            ],
            'status_id' => [
                'route' => 'helpdesk.statuses.index'
            ],
            'started_at' => [
                'key' => 'started_at',
                'translation' => 'general.start_date_ticket',
                'date' => true,
            ],
            
        ],
    ],

    'Modules\Helpdesk\Models\TicketHistory' => [
        'columns' => [
            'description' => ['searchable' => true],
            'created_at' => [
                'key' => 'created_at',
                'date' => true
            ],
            'created' => [
                'key' => 'created',
                'translation' => 'helpdesk::general.fields.date',
                'date' => true
            ],
            'updated_at' => [
                'key' => 'updated_at',
                'date' => true
            ],
        ],
    ],

    'Modules\Helpdesk\Models\Portal\Ticket' => [
        'columns' => [
            'name' => ['searchable' => true],
            'subject' => ['searchable' => true],
            'message' => ['searchable' => true],
            'category_id' => [
                'route' => 'portal.helpdesk.tickets.categories',
            ],
            'status_id' => [
                'route' => 'portal.helpdesk.statuses.index'
            ],
            'created_at' => [
                'date' => true
            ],
            'updated_at' => [
                'date' => true
            ],
        ],
    ],

];
