<?php

return [

    'name'              => 'Показники продажів',
    'description'       => 'Це мій чудовий модуль',
    'widgets'           => [
        'top_customers_revenue_based'   => 'Топ 5 клієнтів на основі доходу',
        'top_customers_profit_based'    => 'Топ 5 клієнтів за прибутком',
        'top_items_revenue_based'       => 'Топ 5 товарі за доходами',
        'top_items_profit_based'        => 'Топ 5 товарі за прибутками',
        'top_items_profit_margin_based' => 'Топ 5 товарів за прибутком маржі',
    ]
];