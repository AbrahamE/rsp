<?php

return [

    'name'              => 'Performance de ventes',
    'description'       => 'Consultez votre top des ventes',
    'widgets'           => [

        'top_customers_revenue_based'   => 'Top 5 clients en recette',
        'top_customers_profit_based'    => 'Top 5 clients en profit',
        'top_items_revenue_based'       => 'Top 5 Articles en recette',
        'top_items_profit_based'        => 'Top 5 Articles en profit',
        'top_items_profit_margin_based' => 'Top 5 Articles en marge',

        'description'                   => [
            'top_customers_revenue_based'   => 'Mesure le montant des revenus des 5 principaux clients de votre entreprise.',
            'top_customers_profit_based'    => 'Mesure le montant des bénéfices des 5 principaux clients de votre entreprise.',
            'top_items_revenue_based'       => 'Mesure le montant des revenus des 5 principaux éléments de votre entreprise.',
            'top_items_profit_based'        => 'Measures the amount of profit from your company’s top 5 items.',
            'top_items_profit_margin_based' => 'Mesure le montant de la marge bénéficiaire des 5 principaux éléments de votre entreprise.',
        ],

    ]
];