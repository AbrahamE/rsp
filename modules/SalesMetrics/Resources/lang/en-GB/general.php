<?php

return [

    'name'              => 'Sales Metrics',
    'description'       => 'This is my awesome module',
    'widgets'           => [

        'top_customers_revenue_based'   => 'Top 5 Customers Revenue Based',
        'top_customers_profit_based'    => 'Top 5 Customers Profit Based',
        'top_items_revenue_based'       => 'Top 5 Items Revenue Based',
        'top_items_profit_based'        => 'Top 5 Items Profit Based',
        'top_items_profit_margin_based' => 'Top 5 Items Profit Margin Based',

        'description'                   => [
            'top_customers_revenue_based'   => 'Measures the amount of revenue from your company’s top 5 customers.',
            'top_customers_profit_based'    => 'Measures the amount of profit from your company’s top 5 customers.',
            'top_items_revenue_based'       => 'Measures the amount of revenue from your company’s top 5 items.',
            'top_items_profit_based'        => 'Measures the amount of profit from your company’s top 5 items.',
            'top_items_profit_margin_based' => 'Measures the amount of profit margin from your company’s top 5 items.',
        ],

    ]
];