<?php

return [

    'name'              => 'Métricas de Vendas',
    'description'       => 'Este é o meu módulo incrível',
    'widgets'           => [
        'top_customers_revenue_based'   => 'Top 5 Clientes com base na receita',
        'top_customers_profit_based'    => 'Top 5 Clientes com base no lucro',
        'top_items_revenue_based'       => 'Top 5 Itens com base na receita',
        'top_items_profit_based'        => 'Top 5 Itens com base na lucro',
        'top_items_profit_margin_based' => 'Top 5 Itens com base na margem de lucro',
    ]
];