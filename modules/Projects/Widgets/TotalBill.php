<?php

namespace Modules\Projects\Widgets;

use App\Models\Document\Document;
use Modules\Projects\Abstracts\Widgets\Widget;
use Modules\Projects\Models\Financial;

class TotalBill extends Widget
{
    public function show($project = null)
    {
        $billTotalAmount = 0;
        $currency_code = setting('default.currency');

        if ($project) {
            $ids = $project->financials()->type(Document::class)->pluck('financialable_id');
            $this->views['header'] = 'projects::widgets.standard_header';
            $currency_code = $project->currency_code ?? setting('default.currency');
        } else {
            $ids = Financial::type(Document::class)->pluck('financialable_id');
            $this->views['header'] = 'projects::widgets.stats_header';
        }

        $this->applyFilters(Document::whereIn('id', $ids)->bill()->accrued(), ['date_field' => 'issued_at'])
            ->get()
            ->each(function (Document $item, $key) use (&$billTotalAmount, $currency_code) {
                if ($item->currency_code === $currency_code) {
                    $billTotalAmount += $item->amount;
                } else {
                    $billTotalAmount += $item->convertBetween($item->amount, $item->currency_code, $item->currency_rate, $currency_code, currency($currency_code)->getRate());
                }
            });

        $total_amount = money($billTotalAmount, $currency_code, true);

        return $this->view('projects::widgets.total_bill', [
            'total_amount_formatted'    => $total_amount->formatForHumans(),
            'total_amount'              => $total_amount,
        ]);
    }
}
