<?php

namespace Modules\Projects\Widgets;

use App\Models\Banking\Transaction;
use Modules\Projects\Abstracts\Widgets\Widget;
use Modules\Projects\Models\Financial;

class TotalPayment extends Widget
{
    public function show($project = null)
    {
        $paymentTotalAmount = 0;
        $currency_code = setting('default.currency');

        if ($project) {
            $ids = $project->financials()->type(Transaction::class)->pluck('financialable_id');
            $this->views['header'] = 'projects::widgets.standard_header';
            $currency_code = $project->currency_code ?? setting('default.currency');
        } else {
            $ids = Financial::type(Transaction::class)->pluck('financialable_id');
            $this->views['header'] = 'projects::widgets.stats_header';
        }

        $this->applyFilters(Transaction::whereIn('id', $ids)->type('expense')->isNotDocument())
            ->get()
            ->each(function (Transaction $item, $key) use (&$paymentTotalAmount, $currency_code) {
                if ($item->currency_code === $currency_code) {
                    $paymentTotalAmount += $item->amount;
                } else {
                    $paymentTotalAmount += $item->convertBetween($item->amount, $item->currency_code, $item->currency_rate, $currency_code, currency($currency_code)->getRate());
                }
            });

        $total_amount = money($paymentTotalAmount, $currency_code, true);

        return $this->view('projects::widgets.total_payment', [
            'total_amount_formatted'    => $total_amount->formatForHumans(),
            'total_amount'              => $total_amount,
        ]);
    }
}
