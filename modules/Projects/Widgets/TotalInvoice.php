<?php

namespace Modules\Projects\Widgets;

use App\Models\Document\Document;
use Modules\Projects\Abstracts\Widgets\Widget;
use Modules\Projects\Models\Financial;

class TotalInvoice extends Widget
{
    public function show($project = null)
    {
        $invoiceTotalAmount = 0;
        $currency_code = setting('default.currency');

        if ($project) {
            $ids = $project->financials()->type(Document::class)->pluck('financialable_id');
            $this->views['header'] = 'projects::widgets.standard_header';
            $currency_code = $project->currency_code ?? setting('default.currency');
        } else {
            $ids = Financial::type(Document::class)->pluck('financialable_id');
            $this->views['header'] = 'projects::widgets.stats_header';
        }

        $this->applyFilters(Document::whereIn('id', $ids)->invoice()->accrued(), ['date_field' => 'issued_at'])
            ->get()
            ->each(function (Document $item, $key) use (&$invoiceTotalAmount, $currency_code) {
                if ($item->currency_code === $currency_code) {
                    $invoiceTotalAmount += $item->amount;
                } else {
                    $invoiceTotalAmount += $item->convertBetween($item->amount, $item->currency_code, $item->currency_rate, $currency_code, currency($currency_code)->getRate());
                }
            });

        $total_amount = money($invoiceTotalAmount, $currency_code, true);

        return $this->view('projects::widgets.total_invoice', [
            'total_amount_formatted'    => $total_amount->formatForHumans(),
            'total_amount'              => $total_amount,
        ]);
    }
}
