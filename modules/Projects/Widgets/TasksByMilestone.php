<?php

namespace Modules\Projects\Widgets;

use Modules\Projects\Abstracts\Widgets\Widget;
use App\Traits\Tailwind;

class TasksByMilestone extends Widget
{
    use Tailwind;
    
    public $default_settings = [
        'width' => 'w-2/4 px-12',
    ];

    public function show($project = null)
    {
        $project->milestones()->collect()->map(function ($query) {
            return $this->applyFilters($query, ['date_field' => 'created_at']);
        })->each(function ($milestones){
            $this->addToDonut(
                null, 
                $milestones->name, 
                $milestones->tasks()->count()
            );
        });

        $this->donut['colors'] =  [
            '#F58934ff',
            '#3f88c5ff',
            '#032b43ff',
            '#136f63ff',
            '#c0e0deff'
        ];;

        $chart = $this->getDonutChart(trans('projects::general.widgets.tasks_by_milestones'), '100%', 300, 10);

        return $this->view('widgets.donut_chart', [
            'chart' => $chart
        ]);
    }
}
