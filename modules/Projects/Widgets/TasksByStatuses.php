<?php

namespace Modules\Projects\Widgets;

use Modules\Projects\Abstracts\Widgets\Widget;
use App\Traits\Tailwind;

class TasksByStatuses extends Widget
{
    use Tailwind;
    
    public $default_settings = [
        'width' => 'w-2/4 px-12',
    ];

    public function show($project = null)
    {
        if ($project == null) {
            return false;
        }

        $project->taskStatuses()->withCount('status')->collect()->map(function ($query) {
            return $this->applyFilters($query, ['date_field' => 'created_at']);
        })->each(function ($tasks) {
            $this->addToDonut(
                $this->getHexCodeOfTailwindClass($tasks->color), 
                $tasks->name, 
                $tasks->status_count
            );
        });

        $chart = $this->getDonutChart(trans('projects::general.widgets.tasks_by_statuses'), '100%', 300, 10);

        return $this->view('widgets.donut_chart', [
            'chart' => $chart
        ]);
    }
}
