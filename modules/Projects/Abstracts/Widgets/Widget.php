<?php

namespace Modules\Projects\Abstracts\Widgets;

use App\Abstracts\Widget as BaseWidget;

class Widget extends BaseWidget
{
    public function getDefaultName()
    {
        $class_name = get_class($this);
        $class_name = str_replace('Modules\Projects\Widgets\\', '', $class_name);
        $class_name_snake = str($class_name)->snake();

        return trans('projects::general.widgets.' . $class_name_snake);
    }

    public function getDefaultSettings()
    {
        $route = request()->route();

        $condition = ! is_null($route)
                    ? $route->getName() == 'dashboard' ?? false
                    : false;

        $width = $condition
                    ? 'w-1/4 sm:w-1/4 text-center text-center py-8'
                    : 'w-1/2 sm:w-1/3 text-center';

        return ['width' => $width];
    }
}
