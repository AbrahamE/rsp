<?php

namespace Modules\Projects\Traits;

use Illuminate\Validation\Validator;

trait Validation
{
    /**
     * Find and remove custom fields from validation rules.
     */
    public function withValidator(Validator $validator): void
    {
        $rules = $validator->getRules() ?? [];

        if (empty($rules)) {
            return;
        }

        foreach (array_keys($rules) as $title) {
            if (str_contains($title, 'custom_field_')) {
                unset($rules[$title]);
            }
        }

        $validator->setRules($rules);
    }
}
