<?php

namespace Modules\Projects\Observers;

use App\Traits\Jobs;
use App\Abstracts\Observer;
use Modules\Projects\Events\Tasks\CreateComment;
use Modules\Projects\Models\Activity;
use Modules\Projects\Models\Comment as Model;
use Modules\Projects\Jobs\Activities\CreateActivity;
use Modules\Projects\Jobs\Activities\DeleteActivity;
use Modules\Projects\Notifications\Tasks\TasksCreateComment as Notification;

class Comment extends Observer
{
    use Jobs;

    /**
     * Listen to the created event.
     *
     * @param Model $comment
     * @return void
     */
    public function created(Model $comment)
    {
        $this->dispatch(new CreateActivity([
            'company_id' => company_id(),
            'project_id' => $comment->project_id,
            'activity_id' => $comment->id,
            'activity_type' => get_class($comment),
            'description' =>  $comment->discussion_id
                ? trans('projects::activities.created.comment.discussion', [
                    'user' => auth()->user()->name,
                    'discussion' => $comment->discussion->name
                ])
                : trans('projects::activities.created.comment.task', [
                    'user' => auth()->user()->name,
                    'task' => $comment->task->name
                ]),
            'created_by' => auth()->id()
        ]));

        try {
            if(!$comment->discussion_id){
                $task = $comment->task;

                if ($task->liable->count()) 
                    $this->notificationUser($task, $task->liable, $comment);
  
                $this->notificationUser($task, $task->users, $comment);
            }
        } catch (\Exception $e) {
            flash(trans('helpdesk::general.error.email_error'))->warning()->important();
        }
    }

    /**
     * Listen to the created event.
     *
     * @param Model $task
     * @return void
     */
    public function updated(Model $comment)
    {
        $this->dispatch(new CreateActivity([
            'company_id' => company_id(),
            'project_id' => request('project_id'),
            'activity_id' => $comment->id,
            'activity_type' => get_class($comment),
            'description' =>  $comment->discussion_id
            ? trans('projects::activities.updated.comment.discussion', [
                'user' => auth()->user()->name,
                'discussion' => $comment->discussion->name
            ])
            : trans('projects::activities.updated.comment.task', [
                'user' => auth()->user()->name,
                'task' => $comment->task->name
            ]),
            'created_by' => auth()->id()
        ]));
  
    }

    private function notificationUser($task, $users, )
    {
        $users->each(function ($taskUser) use ($task) {
            if ($taskUser->receive_notifications)
                event(
                    new CreateComment($task, $taskUser->user, Notification::class)
                );
        });
    }

    /**
     * Listen to the deleted event.
     *
     * @param Model $comment
     * @return void
     */
    public function deleted(Model $comment)
    {
        $activities = Activity::where('activity_type', get_class($comment))->where('activity_id', $comment->id)->get();

        foreach ($activities as $activity) {
            $this->dispatch(new DeleteActivity($activity));
        }
    }
}
