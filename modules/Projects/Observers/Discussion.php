<?php

namespace Modules\Projects\Observers;

use App\Traits\Jobs;
use App\Abstracts\Observer;
use Modules\Projects\Models\Activity;
use Modules\Projects\Models\Discussion as Model;
use Modules\Projects\Jobs\Activities\CreateActivity;
use Modules\Projects\Jobs\Activities\DeleteActivity;

class Discussion extends Observer
{
    use Jobs;

    /**
     * Listen to the created event.
     *
     * @param Model $discussion
     * @return void
     */
    public function created(Model $discussion)
    {
        $this->dispatch(new CreateActivity([
            'company_id' => company_id(),
            'project_id' => $discussion->project_id,
            'activity_id' => $discussion->id,
            'activity_type' => get_class($discussion),
            'description' => trans('projects::activities.created.discussion', [
                'user' => auth()->user()->name,
                'discussion' => $discussion->name
            ]),
            'created_by' => auth()->id()
        ]));
    }

    /**
     * Listen to the deleted event.
     *
     * @param Model $discussion
     * @return void
     */
    public function deleted(Model $discussion)
    {
        $activities = Activity::where('activity_type', get_class($discussion))->where('activity_id', $discussion->id)->get();

        foreach ($activities as $activity) {
            $this->dispatch(new DeleteActivity($activity));
        }
    }
}
