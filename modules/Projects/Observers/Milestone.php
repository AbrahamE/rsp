<?php

namespace Modules\Projects\Observers;

use App\Traits\Jobs;
use App\Abstracts\Observer;
use Modules\Projects\Models\Activity;
use Modules\Projects\Models\Milestone as Model;
use Modules\Projects\Jobs\Activities\CreateActivity;
use Modules\Projects\Jobs\Activities\DeleteActivity;

class Milestone extends Observer
{
    use Jobs;

    /**
     * Listen to the created event.
     *
     * @param Model $milestone
     * @return void
     */
    public function created(Model $milestone)
    {
        $this->dispatch(new CreateActivity([
            'company_id' => company_id(),
            'project_id' => request('project_id'),
            'activity_id' => $milestone->id,
            'activity_type' => get_class($milestone),
            'description' => trans('projects::activities.created.milestone', [
                'user' => auth()->user()->name,
                'milestone' => $milestone->name
            ]),
            'created_by' => auth()->id()
        ]));
    }

    /**
     * Listen to the deleted event.
     *
     * @param Model $milestone
     * @return void
     */
    public function deleted(Model $milestone)
    {
        $activities = Activity::where('activity_type', get_class($milestone))->where('activity_id', $milestone->id)->get();

        foreach ($activities as $activity) {
            $this->dispatch(new DeleteActivity($activity));
        }
    }
}
