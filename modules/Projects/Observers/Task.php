<?php

namespace Modules\Projects\Observers;

use App\Traits\Jobs;
use App\Abstracts\Observer;
use Modules\Projects\Events\Tasks\Updated;
use Modules\Projects\Models\Task as Model;
use Modules\Projects\Jobs\Activities\CreateActivity;
use Modules\Projects\Jobs\Activities\DeleteActivity;
use Modules\Projects\Models\Activity;
use Modules\Projects\Notifications\Tasks\TaskUpdate as Notification;

class Task extends Observer
{
    use Jobs;

    /**
     * Listen to the created event.
     *
     * @param Model $task
     * @return void
     */
    public function created(Model $task)
    {
        $this->dispatch(new CreateActivity([
            'company_id' => company_id(),
            'project_id' => request('project_id'),
            'activity_id' => $task->id,
            'activity_type' => get_class($task),
            'description' => trans('projects::activities.created.task', [
                'user' => auth()->user()->name,
                'task' => $task->name
            ]),
            'created_by' => auth()->id()
        ]));
        
    }

 /**
     * Listen to the created event.
     *
     * @param Model $task
     * @return void
     */
     public function updated(Model $task)
    {
        $this->dispatch(new CreateActivity([
            'company_id' => company_id(),
            'project_id' => $task->project_id,
            'activity_id' => $task->id,
            'activity_type' => get_class($task),
            'description' => trans('projects::activities.updated.task', [
                'user' => auth()->user()->name,
                'task' => $task->name
            ]),
            'created_by' => auth()->id()
        ]));

        try {
            if ($task->liable->count()) 
                $this->notificationUser($task, $task->liable);
            $this->notificationUser($task, $task->users);
        } catch (\Exception $e) {
            flash(trans('helpdesk::general.error.email_error'))->warning()->important();
        }
    }

    private function notificationUser($task, $users) {
        $users->each(function ($taskUser) use ($task) {
            if($taskUser->receive_notifications)
                event(
                    new Updated($task, $taskUser->user, Notification::class)
                );
        });
    }

    /**
     * Listen to the deleted event.
     *
     * @param Model $task
     * @return void
     */
    public function deleted(Model $task)
    {
        $activities = Activity::where('activity_type', get_class($task))->where('activity_id', $task->id)->get();

        foreach ($activities as $activity) {
            $this->dispatch(new DeleteActivity($activity));
        }
    }
}
