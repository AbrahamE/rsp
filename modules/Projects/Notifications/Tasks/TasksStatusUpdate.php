<?php

namespace Modules\Projects\Notifications\Tasks;

use App\Abstracts\Notification;
use App\Models\Setting\EmailTemplate;

class TasksStatusUpdate extends Notification
{
    public $task;

    public $member;
    
    public $template;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($task = null, $member = null, $template_alias = null)
    {
        parent::__construct();

        $this->task = $task;
        $this->member = $member;
        $this->template = EmailTemplate::alias($template_alias)->first();
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return $this->initMailMessage();
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'template_alias' => $this->template->alias,
            'description'    => $this->task->description,
            'name'           => $this->task->name,
            'task'           => $this->task->id,
            'company'        => $this->task->company_id,
            'project'        => $this->task->project_id,
            'use_name'       => user()->name,
            'started_at'     => $this->task->started_at,
            'deadline_at'    => $this->task->deadline_at,
            'priority'       => $this->task->priority,
            'status'         => $this->task->statusCurrent->status->name,
            'company_name'   => company()->name,
            'route'          => route('projects.projects.show', $this->task->project_id) . '#tasks'
        ];
    }

    public function getTags(): array
    {
        return [
            '{template_alias}',
            '{description}',
            '{name}',
            '{task}',
            '{company}',
            '{project}',
            '{use_name}',
            '{started_at}',
            '{deadline_at}',
            '{priority}',
            '{status}',
            '{company_name}',
            '{route}'
        ];
    }

    public function getTagsReplacement(): array
    {
        return [
            $this->template->alias,
            $this->task->description,
            $this->task->name,
            $this->task->id,
            $this->task->company_id,
            $this->task->project_id,
            user()->name,
            $this->task->started_at,
            $this->task->deadline_at,
            $this->task->priority,
            $this->task->statusCurrent->status->name,
            company()->name,
            route('projects.projects.show', $this->task->project_id) . '#tasks'
        ];
    }
}
