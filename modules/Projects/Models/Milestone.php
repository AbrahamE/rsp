<?php

namespace Modules\Projects\Models;

use App\Abstracts\Model;

class Milestone extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'project_milestones';


    
    protected $dates = ['deleted_at', 'created_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = ['company_id', 'project_id', 'name', 'deadline_at', 'description'];

    public function tasks()
    {
        return $this->hasMany('Modules\Projects\Models\Task', 'milestone_id');
    }

    public function project()
    {
        return $this->belongsTo('Modules\Projects\Models\Project');
    }

    public function status()
    {
        $tasks = $this->tasks();
        if (!$tasks->count()) return $tasks->count();

        $finalized = $this->tasks()->collect()->map(function ($task) {
            $task->finalized = !!$task->statusCurrent->status->status_end;
            return $task;
        })->filter(fn ($task) => $task->finalized)->count();

        return number_format(($finalized / $tasks->count()) * 100, 2);
    }

    public function status_color()
    {
        $status = $this->status();

        if($status == 100) return 'green';
        if($status == 0)   return 'gray';
        
        if($status >= 50)  return 'jaffa';
        if($status < 50)   return 'indigo';
        
        return 'gray';
    }

    /**
     * Get the line actions.
     *
     * @return array
     */
    public function getLineActionsAttribute()
    {
        if (user()->isCustomer()) {
            return [];
        }

        $actions = [];

        $actions[] = [
            'type' => 'button',
            'title' => trans('general.edit'),
            'icon' => 'edit',
            'attributes' => [
                '@click' => 'onModalAddNew("' . route('projects.milestones.edit', [$this->project->id, $this->id]) . '")',
            ],
            'permission' => 'update-projects-milestones',
        ];

        $actions[] = [
            'type' => 'delete',
            'icon' => 'delete',
            'title' => trans_choice('projects::general.milestones', 1),
            'route' => [
                'projects.milestones.destroy',
                $this->project->id,
                $this->id,
            ],
            'permission' => 'delete-projects-milestones',
            'model' => $this,
        ];

        return $actions;
    }
}
