<?php

namespace Modules\Projects\Models;

use App\Abstracts\Model;
use App\Traits\Media;
use Illuminate\Database\Eloquent\Builder;

class Task extends Model
{
    use Media;

    protected $table = 'project_tasks';

    protected $dates = ['deleted_at', 'started_at', 'ended_at'];

    protected $fillable = ['company_id', 'project_id', 'milestone_id', 'name', 'description', 'status', 'started_at', 'deadline_at', 'priority', 'advance', 'billable', 'is_visible_to_customer', 'hourly_rate', 'created_by', 'is_invoiced', 'invoice_id'];

    public function milestone()
    {
        return $this->belongsTo('Modules\Projects\Models\Milestone');
    }

    public function project()
    {
        return $this->belongsTo('Modules\Projects\Models\Project');
    }

    public function users()
    {
        return $this->hasMany('Modules\Projects\Models\ProjectTaskUser', 'task_id')->where('is_liable', false);
    }

    public function liable()
    {
        return $this->hasMany('Modules\Projects\Models\ProjectTaskUser', 'task_id')->where('is_liable', true);
    }

    public function timesheets()
    {
        return $this->hasMany('Modules\Projects\Models\ProjectTaskTimesheet', 'task_id');
    }

    public function statusCurrent()
    {
        return $this->belongsTo('Modules\Projects\Models\TaskStatus', 'id', 'task_id');
    }

    public function statuses()
    {
        return $this->hasMany('Modules\Projects\Models\TaskStatuses', 'project_id', 'project_id');
    }

    public function statusById($id)
    {
        return $this->statuses->where('id', $id)->first();
    }

    public function comments()
    {
        return $this->hasMany('Modules\Projects\Models\Comment', 'task_id');
    }


    public function latestComment()
    {
        return $this->hasOne('Modules\Projects\Models\Comment', 'task_id')->latest();
    }

    public function scopeVisibleToCustomer(Builder $query)
    {
        return $query->where('is_visible_to_customer', true);
    }

    /**
     * Get the attachments.
     *
     * @return string
     */
    public function getAttachmentAttribute($value)
    {
        if (!empty($value) && !$this->hasMedia('attachment')) {
            return $value;
        } elseif (!$this->hasMedia('attachment')) {
            return false;
        }

        return $this->getMedia('attachment')->all();
    }

    public function getStatusColorAttribute()
    {
        return match ($this->status) {
            'not-started' => 'status-draft',
            'active' => 'status-partial',
            'completed' => 'status-success',
            default => 'status-draft',
        };
    }

    /**
     * Get the line actions.
     *
     * @return array
     */
    public function getLineActionsAttribute()
    {

        $projectTaskUser = ProjectTaskUser::where([
            'project_id' => $this->project_id,
            'milestone_id' => $this->milestone_id,
            'task_id' => $this->id,
            'user_id' => user_id(),
        ])->first();

        if (user()->isCustomer()) {
            return [
                [
                    'type' => 'button',
                    'title' => trans('general.show'),
                    'icon' => 'visibility',
                    'attributes' => [
                        '@click' => 'onModalAddNew("' . route('portal.projects.tasks.show', ['project' => $this->project->id, 'task' => $this->id]) . '")',
                    ],
                    'permission' => 'read-projects-portal-tasks',
                ]
            ];
        }

        $actions = [];

        if (user()->can('update-projects-tasks')) {
            $actions[] = [
                'type' => 'button',
                'title' => trans('general.edit'),
                'icon' => 'edit',
                'attributes' => [
                    '@click' => 'onModalAddNew("' . route('projects.tasks.edit', [$this->project->id, $this->id]) . '")',
                ],
                'permission' => 'update-projects-tasks',
            ];
        } else {
            $actions[] = [
                'type' => 'button',
                'title' => trans('general.show'),
                'icon' => 'visibility',
                'attributes' => [
                    '@click' => 'onModalAddNew("' . route('projects.tasks.show', ['project' => $this->project->id, 'task' => $this->id]) . '")',
                ],
                'permission' => 'read-projects-tasks',
            ];
        }

        $actions[] = [
            'type' => 'button',
            'title' => trans('general.notify'),
            'icon' => $projectTaskUser?->receive_notifications ? 'notifications' : 'notifications_off',
            'attributes' => [
                '@click' => 'onNotifyTask("' . route(
                    'projects.tasks.notify',
                    [
                        $this->project->id,
                        $this->id,
                        user_id()
                    ]
                ) . '")',
            ],
            'permission' => 'update-projects-tasks',
        ];

        $actions[] = [
            'type' => 'delete',
            'icon' => 'delete',
            'title' => trans_choice('projects::general.tasks', 1),
            'route' => [
                'projects.tasks.destroy',
                $this->project->id,
                $this->id,
            ],
            'permission' => 'delete-projects-tasks',
            'model' => $this,
        ];

        if ($this->timesheets->isEmpty() || $this->timesheets->last()->ended_at !== null) {
            $actions[] = [
                'type' => 'button',
                'title' => trans('general.start'),
                'icon' => 'play_arrow',
                'attributes' => [
                    '@click' => 'timesheet("' . route('projects.timesheets.start', [$this->project, $this]) . '")',
                ],
                'permission' => 'update-projects-timesheets',
            ];
        } elseif ($this->timesheets->isNotEmpty()) {
            $actions[] = [
                'type' => 'button',
                'title' => trans('projects::general.stop'),
                'icon' => 'stop',
                'attributes' => [
                    '@click' => 'timesheet("' . route('projects.timesheets.stop', [$this->project, $this->timesheets()->whereNull('ended_at')->first()]) . '")',
                ],
                'permission' => 'update-projects-timesheets',
            ];
        }

        $actions[] = [
            'type' => 'button',
            'title' => trans_choice('projects::general.comments', 0),
            'icon' => 'visibility',
            'attributes' => [
                '@click' => 'onModalAddNew("' . route('projects.comments.show',  [
                    'project' => $this->project->id,
                    'comment' => $this->id,
                    'task_id' => $this->id,
                    'type_comment' => 'task',
                ]) . '")',
            ],
            'permission' => 'update-projects-tasks',
        ];

        return $actions;
    }
}
