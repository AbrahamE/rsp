<?php

namespace Modules\Projects\Models;

use App\Abstracts\Model;

class ProjectStatus extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'project_status';
    
    protected $fillable = ['id', 'id_status', 'name'];
}
