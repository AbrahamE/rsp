<?php

namespace Modules\Projects\Models;

use App\Abstracts\Model;

class TaskStatus extends Model
{

    protected $table = 'project_task_status';
    
    protected $fillable = [
        'task_id',	
        'status_id'
    ];


    public function status()
    {
        return $this->belongsTo('Modules\Projects\Models\TaskStatuses', 'status_id');
    }

    public function task()
    {
        return $this->belongsTo('Modules\Projects\Models\Task', 'task_id');
    }

}
