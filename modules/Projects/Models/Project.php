<?php

namespace Modules\Projects\Models;

use App\Abstracts\Model;
use App\Models\Banking\Transaction;
use App\Models\Document\Document;
use App\Traits\Media;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Modules\Projects\Casts\DateFormat;

class Project extends Model
{
    use Media;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'projects';

    protected $dates = ['deleted_at', 'started_at', 'ended_at'];

    protected $fillable = ['company_id', 'name', 'description', 'customer_id', 'status', 'started_at', 'ended_at', 'billing_type', 'billing_rate', 'currency_code'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'started_at' => DateFormat::class,
        'ended_at' => DateFormat::class,
    ];

    public function financials()
    {
        return $this->hasMany(Financial::class);
    }

    public function transactions()
    {
        return $this->belongsToMany(Transaction::class, 'Modules\Projects\Models\Financial', 'project_id', 'financialable_id')->where('financialable_type', Transaction::class);
    }

    public function documents()
    {
        return $this->belongsToMany(Document::class, 'Modules\Projects\Models\Financial', 'project_id', 'financialable_id')->where('financialable_type', Document::class);
    }

    public function tasks()
    {
        return $this->hasMany('Modules\Projects\Models\Task', 'project_id');
    }

    public function taskStatuses()
    {
        return $this->hasMany('Modules\Projects\Models\TaskStatuses', 'project_id');
    }

    public function discussions()
    {
        return $this->hasMany('Modules\Projects\Models\Discussion', 'project_id');
    }

    public function users()
    {
        return $this->hasMany('Modules\Projects\Models\ProjectUser', 'project_id');
    }

    public function followers()
    {
        return $this->hasMany('Modules\Projects\Models\ProjectFollowers', 'project_id');
    }

    public function activities()
    {
        return $this->hasMany('Modules\Projects\Models\Activity', 'project_id');
    }

    public function milestones()
    {
        return $this->hasMany('Modules\Projects\Models\Milestone', 'project_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Common\Contact');
    }

    public function timesheets()
    {
        return $this->hasMany('Modules\Projects\Models\ProjectTaskTimesheet', 'project_id');
    }

    public function comments()
    {
        return $this->hasMany('Modules\Projects\Models\Comment', 'project_id');
    }

    public  function projectProgressPercentage(){
        if ($this->milestones->count()) {
        
            return $this->milestones->map(fn ($mileston) => (int) $mileston->status())->sum() / $this->milestones->count();
        }
        return $this->milestones->count();
    }
    /**
     * Scope to get all rows filtered, sorted and paginated.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $sort
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCollect($query, $sort = 'started_at')
    {
        $request = request();

        /**
         * Modules that use the sort parameter in CRUD operations cause an error,
         * so this sort parameter set back to old value after the query is executed.
         *
         * for Custom Fields module
         */
        $request_sort = $request->get('sort') ?? $sort;

        $direction = $request->get('direction') ?? 'desc';

        $request->merge([
            'sort'      => $request_sort, 
            'direction' => $direction
        ]);

        $query->usingSearchString()->sortable($sort);

        if ($request->expectsJson() && $request->isNotApi()) {
            return $query->get();
        }

        // This line disabled because broken sortable issue.
        //$request->offsetUnset('direction');
        $limit = (int) $request->get('limit', setting('default.list_limit', '25'));

        return $query->paginate($limit);
    }

    /**
     * Get the project's status translated.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function transStatus(): Attribute
    {
        return Attribute::make(
            get:fn() => trans("projects::general.$this->status"),
        );
    }

    /**
     * Get the attachments.
     *
     * @return string
     */
    public function getAttachmentAttribute($value)
    {
        if (!empty($value) && !$this->hasMedia('attachment')) {
            return $value;
        } elseif (!$this->hasMedia('attachment')) {
            return false;
        }

        return $this->getMedia('attachment')->all();
    }

    /**
     * Get the line actions.
     *
     * @return array
     */
    public function getLineActionsAttribute()
    {
        $actions = [];

        if (! user()->isCustomer()) {
            $actions[] = [
                'title' => trans('general.show'),
                'icon' => 'visibility',
                'url' => route('projects.projects.show', $this->id),
                'permission' => 'read-projects-projects',
            ];

            $actions[] = [
                'title' => trans('general.edit'),
                'icon' => 'edit',
                'url' => route('projects.projects.edit', $this->id),
                'permission' => 'update-projects-projects',
            ];

            $actions[] = [
                'type' => 'delete',
                'icon' => 'delete',
                'route' => 'projects.projects.destroy',
                'permission' => 'delete-projects-projects',
                'model' => $this,
            ];
        } else {
            $actions[] = [
                'title' => trans('general.show'),
                'icon' => 'visibility',
                'url' => route('portal.projects.projects.show', $this->id),
                'permission' => 'read-projects-portal-projects',
            ];
        }

        return $actions;
    }

    /**
     * Get the status label.
     *
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        return match($this->status) {
            'to_start'   => 'matisse',
            'inprogress' => 'jaffa',
            'completed' => 'green',
            'suspended' => 'red',
            default => 'gray',
        };
    }
}











