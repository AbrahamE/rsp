<?php

namespace Modules\Projects\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Media;

class Comment extends Model
{
    use SoftDeletes, Media; 
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'project_comments';
    
    protected $fillable = ['company_id', 'project_id', 'discussion_id','task_id', 'comment', 'created_by'];

    public function discussion()
    {
        return $this->belongsTo('Modules\Projects\Models\Discussion', 'discussion_id', 'id');
    }

    public function task()
    {   
        return $this->belongsTo('Modules\Projects\Models\Task', 'task_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User', 'created_by');
    }

    public function getAttachmentAttribute($value)
    {
        if (!empty($value) && !$this->hasMedia('attachment')) {
            return $value;
        } elseif (!$this->hasMedia('attachment')) {
            return false;
        }

        return $this->getMedia('attachment')->all();
    }
}
