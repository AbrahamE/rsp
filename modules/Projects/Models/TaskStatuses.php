<?php

namespace Modules\Projects\Models;

use App\Abstracts\Model;

class TaskStatuses extends Model
{

    protected $table = 'project_task_statuses';
    
    protected $fillable = [
        'company_id',
        'project_id',
        'name',
        'color',
        'status_end'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucfirst(trim($value));
    }

    public function status()
    {
        return $this->belongsTo('Modules\Projects\Models\TaskStatus','id', 'status_id');
    }

}
