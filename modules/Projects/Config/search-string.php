<?php

use Modules\Projects\Models\Milestone;
use Modules\Projects\Models\Project;
use Modules\Projects\Models\ProjectTaskTimesheet;
use Modules\Projects\Models\Task;
use Modules\Projects\Models\Activity;
use Modules\Projects\Models\TaskStatuses;

return [
    Project::class => [
        'columns' => [
            'name' => ['searchable' => true],
            'description' => ['searchable' => true],
            'status' => [
                'values' => [
                    'to_start' => 'projects::general.to_start',
                    'suspended' => 'projects::general.suspended',
                    'inprogress' => 'projects::general.inprogress',
                    'completed' => 'projects::general.completed',
                ],
               
            ],
            'customer_id' => [
                'route' => ['customers.index', 'search=enabled:1'],
            ],
            'started_at' => [
                'key' => 'started_at',
                'translation' => 'general.start_date',
                'date' => true,
            ],
            'ended_at' => [
                'key' => 'ended_at',
                'translation' => 'general.end_date',
                'date' => true,
            ],
        ],
    ],

    Modules\Projects\Models\Portal\Project::class => [
        'columns' => [
            'name' => ['searchable' => true],
            'description' => ['searchable' => true],
            'status' => [
                'values' => [
                    'to_start' => 'projects::general.to_start',
                    'suspended' => 'projects::general.suspended',
                    'inprogress' => 'projects::general.inprogress',
                    'completed' => 'projects::general.completed',
                ],
            ],
            'started_at' => [
                'key' => 'started_at',
                'translation' => 'general.start_date',
                'date' => true,
            ],
            'ended_at' => [
                'key' => 'ended_at',
                'translation' => 'general.end_date',
                'date' => true,
            ],
        ],
    ],
    
    Task::class => [
        'columns' => [
            'name' => ['searchable' => true],
            'description' => ['searchable' => true],
            'status' => [
                'route' => 'projects.projects.taskStatuses',
            ],
            'priority' => [
                'values' => [
                    'low' => 'projects::general.low',
                    'medium' => 'projects::general.medium',
                    'high' => 'projects::general.high',
                    'urgent' => 'projects::general.urgent',
                ],
            ],
            'started_at' => [
                'key' => 'started_at',
                'translation' => 'general.start_date',
                'date' => true,
            ],
            'deadline_at' => [
                'key' => 'deadline_at',
                'translation' => 'general.end_date',
                'date' => true,
            ],
        ],
    ],

    Activity::class => [
        'columns' => [
            'description' => ['searchable' => true],
            'created_by' => [
                'key' => 'created_by',
                'translation' => 'general.start_date',
                'date' => true,
            ],
        ],
    ],


    ProjectTaskTimesheet::class => [
        'columns' => [
            'user_id' => [
                'route' => ['users.index', 'search=enabled:1'],
            ],
            'started_at' => [
                'key' => 'started_at',
                'translation' => 'general.start_date',
                'date' => true,
            ],
            'ended_at' => [
                'key' => 'ended_at',
                'translation' => 'general.end_date',
                'date' => true,
            ],
        ]
    ],
    Milestone::class => [
        'columns' => [
            'name' => ['searchable' => true],
            'description' => ['searchable' => true],
            'deadline_at' => [
                'key' => 'deadline_at',
                'translation' => 'general.end_date',
                'date' => true,
            ],
        ],
    ],

    
];
