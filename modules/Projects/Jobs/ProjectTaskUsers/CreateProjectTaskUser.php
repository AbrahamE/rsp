<?php

namespace Modules\Projects\Jobs\ProjectTaskUsers;

use App\Abstracts\Job;
use App\Interfaces\Job\ShouldCreate;
use Illuminate\Support\Facades\DB;
use Modules\Projects\Events\Tasks\MemberAssigned;
use Modules\Projects\Models\ProjectTaskUser;
use Modules\Projects\Notifications\Tasks\MemberAssignment as NotificationMember;
use Modules\Projects\Notifications\Tasks\LiableAssignment as NotificationLiable;

class CreateProjectTaskUser extends Job implements ShouldCreate
{
    protected $project_task_user;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {
            $this->project_task_user = ProjectTaskUser::create($this->request->all());

            $user = $this->project_task_user->user;
            $task = $this->project_task_user->task;

            $is_liable = $this->project_task_user->is_liable;

            if (!$this->request->email_send_me && $user->id == user_id()) {
                return;
            }

            event(
                new MemberAssigned(
                    $task,
                    $user,
                    $is_liable
                        ? NotificationLiable::class
                        : NotificationMember::class
                )
            );
        });
    }
}
