<?php

namespace Modules\Projects\Jobs\Projects;

use App\Abstracts\Job;
use App\Interfaces\Job\ShouldCreate;
use Illuminate\Support\Facades\DB;
use Modules\Projects\Jobs\Tasks\UpdateTaskStatuses;
use Modules\Projects\Models\Project;
use Modules\Projects\Models\TaskStatuses;

class CreateProject extends Job implements ShouldCreate
{
    public function handle(): Project
    {
        DB::transaction(function () {
            $this->model = Project::create($this->request->all());

            // Update billing rate
            if ($this->model->billing_type == 'fixed-rate') {
                $this->model->billing_rate = $this->request->get('total_rate');
            }

            if ($this->model->billing_type == 'projects-hours') {
                $this->model->billing_rate = $this->request->get('rate_per_hour');
            }

            $this->model->save();

            // Create members of the project
            $members = $this->request->get('members');
            // Create followers of the project
            $followers = $this->request->get('followers');

            $arguments = [
                'company_id' => company_id(),
                'project_id' => $this->model->id,
            ];

            $userProject = [
                'merbers' => [...$arguments],
                'follower' => [...$arguments]
            ];

            $user = user();

            foreach ($members as $member) {
                $userProject['merbers']['user_id'] = $member;

                $this->dispatch(new CreateProjectUser($userProject['merbers']));
            }

            if (!in_array($user->id, $members)) {
                $userProject['merbers']['user_id'] = $user->id;

                $this->dispatch(new CreateProjectUser($userProject['merbers']));
            }

            foreach ($followers as $follower) {
                $userProject['follower']['user_id'] = $follower;

                $this->dispatch(new CreateProjectFollowers($userProject['follower']));
            }

            if (!in_array($user->id, $followers)) {
                $userProject['follower']['user_id'] = $user->id;

                $this->dispatch(new CreateProjectFollowers($userProject['follower']));
            }

            if ($this->request->task_statuses) {
                $taskStatus = TaskStatuses::whereIn('id', $this->request->get('task_statuses'))->collect();
                    
                $taskStatus->map(function ($taskStatus) {
                    $taskStatus->project_id = $this->model->id;
                    return $taskStatus;
                })->each(fn ($status) => $status->save());
            }

            if ($this->request->attachment) {
                foreach ($this->request->attachment as $attachment) {
                    $media = $this->getMedia($attachment, "projects/{$this->model->id}");

                    $this->model->attachMedia($media, 'attachment');
                }
            }
        });

        return $this->model;
    }
}
