<?php

namespace Modules\Projects\Jobs\Projects;

use App\Abstracts\Job;
use Illuminate\Support\Facades\DB;
use App\Jobs\Document\CreateDocument;
use App\Models\Common\Item;
use App\Models\Document\Document;
use App\Models\Setting\Category;
use App\Models\Setting\Currency;
use App\Traits\Documents;
use App\Utilities\Date;
use Modules\Projects\Http\Requests\Invoice;
use Modules\Projects\Models\Project;
use Modules\Projects\Models\ProjectTaskTimesheet as Timesheet;
use Modules\Projects\Models\Task;

class CreateInvoice extends Job
{
    use Documents;

    protected $model;
    protected $project;
    protected $request;
    protected array $invoice_request;

    public function __construct(Project $project, Invoice $request)
    {
        $this->project = $project;
        $this->request = $request;
    }

    public function handle(): Document
    {
        DB::transaction(function () {
            $contact    = $this->project->customer;
            $now        = now();

            $this->invoice_request = [
                'company_id'            => company_id(),
                'type'                  => 'invoice',
                'document_number'       => $this->getNextDocumentNumber('invoice'),
                'status'                => 'draft',
                'issued_at'             => $now,
                'due_at'                => $now,
                'currency_code'         => $this->project->currency_code,
                'currency_rate'         => Currency::code($this->project->currency_code)->first()->rate,
                'category_id'           => Category::income()->enabled()->first()->id,
                'contact_id'            => $this->project->customer_id,
                'contact_name'          => $contact->name,
                'contact_email'         => $contact->email,
                'contact_tax_number'    => $contact->tax_number,
                'contact_phone'         => $contact->phone,
                'contact_address'       => $contact->address,
                'items'                 => [],
            ];

            $function_name = str($this->request->get('project_invoicing_type'))->camel()->value;

            $this->{$function_name}();

            $this->model = $this->dispatch(new CreateDocument($this->invoice_request));

            $this->tasksForEach(function (Task $task, $optinal_args) {
                if ($optinal_args['value'] && $task->is_invoiced == false) {
                    $task->update([
                        'status' => 'completed',
                        'is_invoiced' => true,
                        'invoice_id' => $this->model->id,
                    ]);
                }
            });
        });

        return $this->model;
    }

    protected function singleLine()
    {
        $notes = '';
        $total_hours_to_decimal = 0;

        $this->tasksForEach(function (Task $task) use (&$notes, &$total_hours_to_decimal) {
            $task_date = new Date("00:00");

            $task->timesheets()->each(function (Timesheet $timesheet) use (&$task_date, &$total_hours_to_decimal) {
                date_add($task_date, date_diff(Date::parse($timesheet->started_at), Date::parse($timesheet->ended_at)));

                $total_hours_to_decimal += $this->calculateQuantityByDate($timesheet);
            });

            $notes .= $task->name . ' - ' . (new Date('00:00'))->diff($task_date)->format('%H:%I') . ' ' . trans('projects::general.hours') . "\n";
        });

        $item = $this->getItem([
            'company_id'        => company_id(),
            'name'              => $this->project->name,
            'sale_price'        => $this->project->billing_rate,
            'purchase_price'    => 0,
            'description'       => $this->project->description,
        ]);

        $invoice_items = [
            'item_id' => $item->id,
            'name' => $item->name,
            'quantity' => 1,
            'price' => $this->project->billing_rate,
            'description' => $notes,
        ];

        if ($this->project->billing_type == 'projects-hours') {
            $invoice_items['quantity'] = $total_hours_to_decimal;
        }

        $this->addItem($invoice_items);
    }

    protected function taskPerItem()
    {
        $total_hours_to_decimal = 0;

        $this->tasksForEach(function (Task $task) use (&$total_hours_to_decimal) {
            $task_date = new Date('00:00');

            $task->timesheets()->each(function (Timesheet $timesheet) use (&$task_date, &$total_hours_to_decimal) {
                date_add($task_date, date_diff(Date::parse($timesheet->started_at), Date::parse($timesheet->ended_at)));

                $total_hours_to_decimal += $this->calculateQuantityByDate($timesheet);
            });

            $item = $this->getItem([
                'company_id'        => company_id(),
                'name'              => $this->project->name . ' - ' . $task->name,
                'sale_price'        => $this->project->billing_rate,
                'purchase_price'    => 0,
                'description'       => $this->project->description,
            ]);

            $price = $this->project->billing_rate;

            if ($this->project->billing_type == 'task-hours') {
                $price = $task->hourly_rate;
            }

            $this->addItem([
                'item_id' => $item->id,
                'name' => $item->name,
                'quantity' => $total_hours_to_decimal,
                'price' => $price,
                'description' => (new Date('00:00'))->diff($task_date)->format('%H:%I') . ' ' . trans('projects::general.hours'),
            ]);
        });
    }

    protected function allTimesheetsIndividually()
    {
        $this->tasksForEach(function (Task $task) {
            if (! $task->timesheets->count()) {
                throw new \Exception(trans('projects::messages.error.timesheet_for_invoice'), 40);
            }

            $item = $this->getItem([
                'company_id'        => company_id(),
                'name'              => $this->project->name . ' - ' . $task->name,
                'sale_price'        => $this->project->billing_rate,
                'purchase_price'    => 0,
                'description'       => $this->project->description,
            ]);

            $price = $this->project->billing_rate;

            if ($this->project->billing_type == 'task-hours') {
                $price = $task->hourly_rate;
            }

            $task->timesheets()->each(function (Timesheet $timesheet) use (&$item, &$price) {
                $timesheet_note = ! is_null($timesheet->note) ? " - {$timesheet->note}" : '';

                $this->addItem([
                    'item_id' => $item->id,
                    'name' => $item->name,
                    'quantity' => $this->calculateQuantityByDate($timesheet),
                    'price' => $price,
                    'description' => $timesheet->elapsed_time . $timesheet_note,
                ]);
            });
        });
    }

    protected function addItem(array $invoice_items): void
    {
        array_push($this->invoice_request['items'], $invoice_items);
    }

    protected function tasksForEach(callable $callback, $optinal_args = []): void
    {
        $tasks = $this->request->get('tasks');

        foreach ($tasks as $key => $value) {
            $task = Task::find($key);
            $optinal_args['key'] = $key;
            $optinal_args['value'] = $value;

            if ($value) {
                $callback($task, $optinal_args);
            }
        }
    }

    protected function getItem(array $item): Item
    {
        return Item::firstOrCreate(['name' => $item['name']], $item);
    }

    protected function calculateQuantityByDate(Timesheet $timesheet)
    {
        return Date::parse($timesheet->started_at)->diffInMinutes(Date::parse($timesheet->ended_at ?? now())) / 60;
    }
}
