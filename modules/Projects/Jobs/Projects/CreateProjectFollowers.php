<?php

namespace Modules\Projects\Jobs\Projects;

use App\Abstracts\Job;
use App\Interfaces\Job\ShouldCreate;
use Illuminate\Support\Facades\DB;
use Modules\Projects\Models\ProjectFollowers;

class CreateProjectFollowers extends Job implements ShouldCreate
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() : ProjectFollowers
    {
        DB::transaction(function () {
            $this->model = ProjectFollowers::create($this->request->all());
        });

        return $this->model;
    }
}
