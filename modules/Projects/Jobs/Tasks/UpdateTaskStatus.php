<?php

namespace Modules\Projects\Jobs\Tasks;

use App\Abstracts\Job;
use App\Interfaces\Job\ShouldUpdate;
use Illuminate\Support\Facades\DB;

class UpdateTaskStatus extends Job implements ShouldUpdate
{
    /**
     * Execute the job.
     *
     * @return TaskStatus
     */
    public function handle()
    {
        DB::transaction(function () {
            $this->model->update($this->request->all());
        });

        return $this->model;
    }
}
