<?php

namespace Modules\Projects\Jobs\Tasks;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\ShouldCreate;
use Illuminate\Support\Facades\DB;
use Modules\Projects\Models\TaskStatuses;

class CreateTaskStatuses extends Job implements ShouldCreate, HasOwner
{

    /**
     * Execute the job.
     *
     * @return TaskStatuses
     */
    public function handle()
    {
        DB::transaction(function () {
            $isset = TaskStatuses::where('name', $this->request->name)->whereNull('project_id')->get();

            if ($isset->count()){
                flash(trans('projects::general.error.task_status'))->warning()->important();
                throw new \Exception(trans('projects::general.error.task_status'));
            }   

            $this->model = TaskStatuses::create($this->request->all());

            // $this->model = TaskStatuses::create($this->request->merge([
            //     'color' => "#3b3c3e",
            // ])->all());
        });

        return $this->model;
    }
}
