<?php

namespace Modules\Projects\Jobs\Tasks;

use App\Abstracts\Job;
use App\Interfaces\Job\ShouldUpdate;
use App\Models\Common\Media;
use Illuminate\Support\Facades\DB;
use Modules\Projects\Models\ProjectTaskUser;
use Modules\Projects\Jobs\ProjectTaskUsers\CreateProjectTaskUser;
use Modules\Projects\Jobs\ProjectTaskUsers\DeleteProjectTaskUser;
use Modules\Projects\Jobs\ProjectTaskUsers\UpdateProjectTaskUser;
use Modules\Projects\Models\TaskStatus;

class UpdateTask extends Job implements ShouldUpdate
{
    /**
     * Execute the job.
     *
     * @return Task
     */
    public function handle()
    {
        DB::transaction(function () {
            $this->model->update($this->request->all());

            $request = [
                'company_id' => company_id(),
                'project_id' => $this->model->project_id,
                'milestone_id' => $this->model->milestone_id,
                'task_id' => $this->model->id,
            ];

            $users = $this->request->input('users') ?? [];
            $liables =  $this->request->input('liable') ?? [];

            foreach ($users as $user) {
                if ($this->getUsers()->doesntContain('user_id', $user)) {
                    $request['user_id'] = $user;

                    $this->dispatch(new CreateProjectTaskUser($request));
                }
            }

            foreach ($this->getUsers() as $user) {
                if (!in_array($user->user_id, $users)) {
                    $this->dispatch(new DeleteProjectTaskUser($user));
                }
            }

            foreach ($liables as $liable) {
                if ($this->getLiable()->doesntContain('user_id', $liable)) {
                    $request['user_id'] = $liable;
                    $request['is_liable'] = 1;

                    $this->dispatch(new CreateProjectTaskUser($request));
                }
            }
            foreach ($this->getLiable() as $liable) {
                if (!in_array($liable->user_id, $liables)) {
                    $this->dispatch(new DeleteProjectTaskUser($liable));
                }
            }

            if ($this->request->status_id) {
                $taskStatus = TaskStatus::where('task_id', $this->model->id)->first();

                $request = [
                    'task_id' => $this->model->id,
                    'status_id' => $this->request->status_id
                ];

                $this->dispatch(new UpdateTaskStatus($taskStatus, $request));
            }

            // Upload attachment
            if ($this->request->attachment) {
                if ($this->model->attachment) {
                    $this->deleteMediaModel($this->model, 'attachment', $this->request);
                }

                foreach ($this->request->attachment as $value) {
                    if (is_array($value)) {
                        Media::withTrashed()->find($value['id'])->restore();
                    }
                }

                foreach ($this->request->file('attachment') ?? [] as $attachment) {
                    $media = $this->getMedia($attachment, "projects/{$this->model->project_id}/tasks/{$this->model->id}");

                    $this->model->attachMedia($media, 'attachment');
                }
            } elseif (! $this->request->attachment && $this->model->attachment) {
                $this->deleteMediaModel($this->model, 'attachment', $this->request);
            }
        });

        return $this->model;
    }

    private function getUsers()
    {
        return ProjectTaskUser::where([
            'project_id' => $this->request->project_id,
            'task_id' => $this->model->id,
            'is_liable' => 0
        ])->get();
    }

    private function getLiable()
    {
        return ProjectTaskUser::where([
            'project_id' => $this->request->project_id,
            'task_id' => $this->model->id,
            'is_liable' => 1
        ])->get();
    }
}
