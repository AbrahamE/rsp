<?php

namespace Modules\Projects\Jobs\Tasks;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\ShouldCreate;
use Illuminate\Support\Facades\DB;
use Modules\Projects\Models\TaskStatus;

class CreateTaskStatus extends Job implements ShouldCreate, HasOwner
{

    /**
     * Execute the job.
     *
     * @return TaskStatus
     */
    public function handle()
    {
        DB::transaction(function () {
            $this->model = TaskStatus::create($this->request->all());
        });

        return $this->model;
    }
}
