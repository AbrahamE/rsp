<?php

namespace Modules\Projects\Jobs\Comments;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\ShouldUpdate;
use Illuminate\Support\Facades\DB;
use App\Models\Common\Media;

class UpdateComment extends Job implements ShouldUpdate, HasOwner
{
    /**
     * Execute the job.
     *
     * @return Task
     */
    public function handle()
    {
        DB::transaction(function () {
            $this->model->update($this->request->all());

              // Upload attachment
            if ($this->request->attachment) {
                if ($this->model->attachment) {
                    $this->deleteMediaModel($this->model, 'attachment', $this->request);
                }

                foreach ($this->request->attachment as $value) {
                    if (is_array($value)) {
                        Media::withTrashed()->find($value['id'])->restore();
                    }
                }

                foreach ($this->request->file('attachment') ?? [] as $attachment) {
                    $media = $this->getMedia($attachment, "projects/{$this->model->project_id}/comments/tasks/{$this->model->id}");

                    $this->model->attachMedia($media, 'attachment');
                }
            } elseif (! $this->request->attachment && $this->model->attachment) {
                $this->deleteMediaModel($this->model, 'attachment', $this->request);
            }
        });

        return $this->model;
    }
}