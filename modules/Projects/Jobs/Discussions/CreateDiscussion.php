<?php

namespace Modules\Projects\Jobs\Discussions;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\ShouldCreate;
use Illuminate\Support\Facades\DB;
use Modules\Projects\Models\Discussion;

class CreateDiscussion extends Job implements ShouldCreate, HasOwner
{
    /**
     * Execute the job.
     *
     * @return Task
     */
    public function handle()
    {
        DB::transaction(function () {
            $this->model = Discussion::create($this->request->all());
        });

        if ($this->request->attachment) {
            foreach ($this->request->attachment as $attachment) {
                $media = $this->getMedia($attachment, "projects/{$this->model->project_id}/discussion/{$this->model->id}");

                $this->model->attachMedia($media, 'attachment');
            }
        }

        return $this->model;
    }
}