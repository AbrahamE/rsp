<?php

namespace Modules\Projects\BulkActions;

use App\Abstracts\BulkAction;
use Modules\Projects\Models\Activity;

class Activities extends BulkAction
{
    public $model = Activity::class;

    public $text = 'projects::general.task';

    public $path = [
        'group' => 'task',
        'type' => 'task',
    ];

    public $actions = [
        'delete' => [
            'icon' => 'delete',
            'name' => 'general.delete',
            'message' => 'bulk_actions.message.delete',
            'permission' => 'delete-projects-tasks',
        ],
    ];
}
