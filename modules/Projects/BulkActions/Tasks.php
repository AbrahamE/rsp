<?php

namespace Modules\Projects\BulkActions;

use App\Abstracts\BulkAction;
use Modules\Projects\Models\Task;

class Tasks extends BulkAction
{
    public $model = Task::class;

    public $text = 'projects::general.task';

    public $path = [
        'group' => 'projects',
        'type' => 'task',
    ];

    public $actions = [
        'delete' => [
            'icon' => 'delete',
            'name' => 'general.delete',
            'message' => 'bulk_actions.message.delete',
            'permission' => 'delete-projects-tasks',
        ],
    ];
}
