<?php

namespace Modules\Projects\Http\Requests;

use App\Abstracts\Http\FormRequest as Request;
use Modules\Projects\Traits\Validation;

class TaskUpdateStatus extends Request
{
    use Validation;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'status'         => 'required',
            'status_id'      => 'required',
        ];

        return $rules;
    }
}
