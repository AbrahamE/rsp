<?php

namespace Modules\Projects\Http\Requests;

use App\Abstracts\Http\FormRequest as Request;
use Modules\Projects\Traits\Validation;

class Timesheet extends Request
{
    use Validation;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'started_at'    => 'required|date',
            'ended_at'      => 'nullable|date',
            'task_id'       => 'required|integer',
            'user_id'       => 'required|integer',
            'note'          => 'nullable|string',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => trans('validation.required', ['attribute' => trans_choice('general.users', 1)]),
            'task_id.required' => trans('validation.required', ['attribute' => trans_choice('projects::general.tasks', 1)]),
        ];
    }
}
