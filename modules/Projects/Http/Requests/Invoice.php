<?php

namespace Modules\Projects\Http\Requests;

use App\Abstracts\Http\FormRequest as Request;
use Modules\Projects\Traits\Validation;

class Invoice extends Request
{
    use Validation;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tasks' => 'required',
            'tasks.*' => 'required|filled',
            'project_invoicing_type' => 'required|string',
        ];
    }
}
