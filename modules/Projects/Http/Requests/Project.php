<?php

namespace Modules\Projects\Http\Requests;

use App\Abstracts\Http\FormRequest as Request;

class Project extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $attachment = 'nullable';

        if ($this->files->get('attachment')) {
            $attachment = 'mimes:' . config('filesystems.mimes') . ',pdf,txt,doc,docs,xls,xlsx,cvs,zip|between:0,' . config('filesystems.max_size') * 1024;
        }

        return [
            'name'              => 'required|string',
            'description'       => 'required|string',
            'customer_id'       => 'required|integer',
            'started_at'        => 'required|date',
            'ended_at'          => 'required|date',
            'members'           => 'required|array',
            'task_statuses'     => 'required|array',
            'billing_type'      => 'required|string',
            'currency_code'     => 'required|string|currency',
            'total_rate'        => 'required_if:billing_type,fixed-rate|amount:0',
            'rate_per_hour'     => 'required_if:billing_type,projects-hours|amount:0',
            'attachment.*'      => $attachment,
        ];
    }
}
