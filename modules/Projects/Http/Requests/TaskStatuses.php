<?php

namespace Modules\Projects\Http\Requests;

use App\Abstracts\Http\FormRequest as Request;
use Modules\Projects\Traits\Validation;
use Illuminate\Validation\Rule;
use Modules\Projects\Models\TaskStatuses as ModelsTaskStatuses;

class TaskStatuses extends Request
{
    use Validation;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $status_end = ModelsTaskStatuses::whereNull('project_id')->where('status_end', '<>', 0)->get();

        $rules = [
            'name'          => 'required|string',
            'color'         => 'required|string',
            'company_id'    => 'required|exists:companies,id',
            'status_end'    => 'nullable',
        ];

        if (!$status_end->isEmpty()) {
            $rules['status_end'] =  $rules['status_end'] . '|not_in:true,1';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'status_end.not_in' => trans('messages.error.tasks_status', ['status' => $this->request->get('name')])
        ];
    }
}
