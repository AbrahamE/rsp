<?php

namespace Modules\Projects\Http\Requests;

use App\Abstracts\Http\FormRequest as Request;
use Modules\Projects\Traits\Validation;

class Comment extends Request
{
    use Validation;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $attachment = 'nullable';

        if ($this->files->get('attachment')) {
            $attachment = 'mimes:' . config('filesystems.mimes') . ',pdf,txt,doc,docs,xls,xlsx,cvs,zip|between:0,' . config('filesystems.max_size') * 1024;
        }

        return [
            'comment' => 'required|string',
            'attachment.*'  => $attachment,
        ];
    }
}
