<?php

namespace Modules\Projects\Http\Controllers;

use App\Abstracts\Http\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Modules\Projects\Http\Requests\Task as Request;
use Modules\Projects\Http\Requests\TaskStatuses as RequestStatuses;
use Modules\Projects\Http\Requests\TaskUpdateStatus;
use Modules\Projects\Jobs\ProjectTaskUsers\UpdateProjectTaskUser;
use Modules\Projects\Jobs\Tasks\CreateTask;
use Modules\Projects\Jobs\Tasks\CreateTaskStatuses;
use Modules\Projects\Jobs\Tasks\DeleteTask;
use Modules\Projects\Jobs\Tasks\UpdateTask;
use Modules\Projects\Jobs\Tasks\UpdateTaskStatus;
use Modules\Projects\Models\Project;
use Modules\Projects\Models\ProjectTaskUser;
use Modules\Projects\Models\Task as Model;
use Modules\Projects\Models\TaskStatuses;
use Modules\Projects\Traits\Modals;

class Tasks extends Controller
{
    use Modals;

    public function index(Project $project)
    {
        $tasks = $project->tasks()->collect();

        return view('projects::part.tasks', compact('project', 'tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Project $project)
    {
        $response_json = $this->getModal('create', compact('project'));

        $data = $response_json->getData();

        if ($data->success) {
            $data->data->large = true;
            $data->data->is_top = true;
            $response_json->setData($data);
        }

        return $response_json;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Project $project
     * @param Request $request
     * @return Response
     */
    public function store(Project $project, Request $request)
    {
        $response = $this->ajaxDispatch(new CreateTask($request));

        $response['redirect'] = route('projects.projects.show', $project->id) . '#tasks';

        if ($response['success']) {
            $message = trans('messages.success.added', ['type' => trans_choice('projects::general.tasks', 1)]);

            flash($message)->success();
        } else {
            $message = $response['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show(Project $project, Model $task)
    {
        $response_json = $this->getModal('show', compact('project', 'task'));

        $data = $response_json->getData();

        if ($data->success) {
            $data->data->show = true;
            $data->data->large = true;
            $data->data->is_top = true;
            $response_json->setData($data);
        }

        return $response_json;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Project $project
     * @return Response
     */
    public function edit(Project $project, Model $task)
    {
        $response_json = $this->getModal('edit', compact('project', 'task'));

        $data = $response_json->getData();

        if ($data->success) {
            $data->data->large = true;
            $data->data->is_top = true;
            $response_json->setData($data);
        }

        return $response_json;
    }

    public function comments(Project $project, Model $task)
    {
        $response_json = $this->getModal('comments', compact('project', 'task'));

        $data = $response_json->getData();

        if ($data->success) {
            $data->data->large = true;
            $data->data->is_top = true;
            $response_json->setData($data);
        }

        return $response_json;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Project $project
     * @param Model $task
     * @param Request $request
     * @return Response
     */
    public function update(Project $project, Model $task, Request $request)
    {
        $response = $this->ajaxDispatch(new UpdateTask($task, $request));

        $response['redirect'] = route('projects.projects.show', $project->id) . '#tasks';

        if ($response['success']) {
            $message = trans('messages.success.updated', ['type' => trans_choice('projects::general.tasks', 1)]);

            flash($message)->success();
        } else {
            $message = $response['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy(Project $project, Model $task)
    {
        $response = $this->ajaxDispatch(new DeleteTask($task));

        $response['redirect'] = route('projects.projects.show', $project->id) . '#tasks';

        if ($response['success']) {
            $message = trans('messages.success.deleted', ['type' => $task->name]);

            flash($message)->success();
        } else {
            $message = $response['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    public function getStatuses()
    {
        $project_id = null;
        if (request()->has('project_id')) {
            $project_id = request()->get('project_id');
        }

        return response()->json([
            'success' => true,
            'error' => false,
            'html' => view('projects::modals.tasks_statuses.create', compact('project_id'))->render(),
            'message' => 'null'
        ]);
    }

    public function createStatuses(RequestStatuses $request)
    {
        if ($request->has('status_end')) {
            if ($request->status_end == "true") $request->merge(['status_end' => 1]);
            else $request->merge(['status_end' => 0]);
        }

        $response = $this->ajaxDispatch(new CreateTaskStatuses($request));

        $response['redirect'] = route('projects.projects.create');

        if ($response['success']) {
            $response['message'] = trans('messages.success.added', ['type' => trans_choice('projects::general.tasks_status', 1)]);

            flash($response['message'])->success();
        } else {
            flash($response['message'])->error()->important();
        }

        return response()->json($response);
    }

    public function updateStatus(Project $project, Model $task, TaskUpdateStatus $taskUpdateStatus)
    {

        $response = $this->ajaxDispatch(new UpdateTaskStatus($task->statusCurrent, $taskUpdateStatus));

        $response['redirect'] = route('projects.projects.show', $project->id) . '#board';

        if ($response['success']) {
            $message = trans('messages.success.updated', ['type' => trans_choice('projects::general.tasks', 1)]);

            flash($message)->success();
        } else {
            $message = $response['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    public function notify(Project $project, Model $task, $request)
    {
        $projectTaskUser = ProjectTaskUser::where('project_id', $project->id)
            ->where('task_id', $task->id)
            ->where('user_id', $request)
            ->first();

        if (!(!!$projectTaskUser)) {
            flash("No estás asignado a la tarea, por lo que no puedo recibir notificaciones al respecto.")->error()->important();

            return response()->json([
                'success' => false,
                'error' => true,
                'data' => null,
                'redirect'=> route('projects.projects.show', $project->id) . '#tasks',
                'message' => '',
            ]);
        }

        $response = $this->ajaxDispatch(
            new UpdateProjectTaskUser($projectTaskUser, [
                'receive_notifications' => !$projectTaskUser->receive_notifications
            ])
        );

        $response['redirect'] = route('projects.projects.show', $project->id) . '#tasks';

        if ($response['success']) {
            $message =  $projectTaskUser->receive_notifications
                ? 'Ahorar resiviras notificaciones'
                : 'desactivaste las notificaciones';

            flash($message)->success();
        } else {
            $message = $response['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }
}
