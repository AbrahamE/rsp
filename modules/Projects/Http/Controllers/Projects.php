<?php

namespace Modules\Projects\Http\Controllers;

use App\Abstracts\Http\Controller;
use App\Traits\Uploads;
use App\Models\Common\Contact;
use App\Models\Setting\Currency;
use App\Traits\Contacts;
use Modules\Projects\Exports\TimesheetsExport;
use Modules\Projects\Exports\TransactionsExport;
use Modules\Projects\Http\Requests\Invoice as InvoiceRequest;
use Modules\Projects\Http\Requests\Project as Request;
use Modules\Projects\Jobs\Projects\CreateInvoice;
use Modules\Projects\Jobs\Projects\CreateProject;
use Modules\Projects\Jobs\Projects\DeleteProject;
use Modules\Projects\Jobs\Projects\UpdateProject;
use Modules\Projects\Jobs\Tasks\CreateTaskStatuses;
use Modules\Projects\Models\Project;
use Modules\Projects\Models\ProjectFollowers;
use Modules\Projects\Models\ProjectStatus;
use Modules\Projects\Models\ProjectUser;
use Modules\Projects\Models\TaskStatuses;

class Projects extends Controller
{
    use Uploads, Contacts;

    public function index()
    {
        $projects = Project::with('users.user.media')->collect();

        return view('projects::projects.index', compact('projects'));
    }

    public function create()
    {
        $contacts = Contact::type($this->getCustomerTypes())->pluck('name', 'id');

        $users = company()->users()->pluck('name', 'id');

        $billing_types = [
            'fixed-rate' => trans('projects::general.fixed_rate'),
            'projects-hours' => trans('projects::general.projects_hours'),
            'task-hours' => trans('projects::general.task_hours'),
            'not-billable' => trans('projects::general.not_billable'),
        ];

        $currency = Currency::code(default_currency())->first();

        $statuses = [
            'to_start' => trans('projects::general.to_start'),
            'suspended' => trans('projects::general.suspended'),
            'inprogress' => trans('projects::general.inprogress'),
            'completed' => trans('projects::general.completed'),
        ];

        $taskStatuses = TaskStatuses::whereNull('project_id')->pluck('name', 'id');

        if (!count($taskStatuses)) {
            $response = $this->ajaxDispatch(new CreateTaskStatuses([
                'name' => 'Creado',
                'color' => 'green-500',
                'company_id' => company_id(),
                'status_end' => '0'
            ]));

            $taskStatuses = TaskStatuses::whereNull('project_id')->pluck('name', 'id');
        }

        return view('projects::projects.create', compact('contacts', 'users', 'billing_types', 'currency', 'taskStatuses', 'statuses'));
    }

    public function store(Request $request)
    {
        $response = $this->ajaxDispatch(new CreateProject($request));

        if ($response['success']) {
            $response['redirect'] = route('projects.projects.index');

            $message = trans('messages.success.added', ['type' => trans_choice('projects::general.projects', 1)]);

            flash($message)->success();
        } else {
            $response['redirect'] = route('projects.projects.create');

            $message = $response['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    public function show(Project $project)
    {
        $tabs = [
            'overview',
            'tasks',
            'board',
            'timesheets',
            'milestones',
            'activities',
            'discussions',
            'gantt',
            'attachments'
        ];

        return view('projects::projects.show', compact('project', 'tabs'));
    }

    public function edit(Project $project)
    {
        $contacts = Contact::type($this->getCustomerTypes())->pluck('name', 'id');

        $statuses = [
            'to_start' => trans('projects::general.to_start'),
            'suspended' => trans('projects::general.suspended'),
            'inprogress' => trans('projects::general.inprogress'),
            'completed' => trans('projects::general.completed'),
        ];

        $users = company()->users()->pluck('name', 'id');

        $billing_types = [
            'fixed-rate' => trans('projects::general.fixed_rate'),
            'projects-hours' => trans('projects::general.projects_hours'),
            'task-hours' => trans('projects::general.task_hours'),
            'not-billable' => trans('projects::general.not_billable'),
        ];
       
        $currency = Currency::code($project->currency_code)->first();

        $members = ProjectUser::where('project_id', '=', $project->id)->pluck('user_id');

        $followers = ProjectFollowers::where('project_id', '=', $project->id)->pluck('user_id');

        $taskStatuses = TaskStatuses::where('project_id', '=', $project->id)->pluck('name', 'id');

        return view('projects::projects.edit', compact('project', 'contacts', 'statuses', 'users', 'members', 'followers', 'taskStatuses', 'billing_types', 'currency'));
    }

    public function update(Project $project, Request $request)
    {
        $response = $this->ajaxDispatch(new UpdateProject($project, $request));

        if ($response['success']) {
            $response['redirect'] = route('projects.projects.index');

            $message = trans('messages.success.updated', ['type' => trans_choice('projects::general.projects', 1)]);

            flash($message)->success();
        } else {
            $response['redirect'] = route('projects.projects.edit', $project->id);

            $message = $response['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    public function destroy(Project $project)
    {
        $response = $this->ajaxDispatch(new DeleteProject($project));

        if ($response['success']) {
            $response['redirect'] = route('projects.projects.index');

            $message = trans('messages.success.deleted', ['type' => trans_choice('projects::general.projects', 1)]);

            flash($message)->success();
        } else {
            $response['redirect'] = route('projects.projects.edit', $project->id);

            $message = $response['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    public function invoice(Project $project)
    {
        if ($project->billing_type === 'fixed-rate') {
            $project_invoicing_type['single_line'] = trans('projects::general.single_line');
        } else {
            $project_invoicing_type = [
                'task_per_item' => trans('projects::general.task_per_item'),
                'all_timesheets_individually' => trans('projects::general.all_timesheets_individually'),
            ];
        }

        return response()->json([
            'success' => true,
            'error' => false,
            'data'    => [
                'title'   => trans('general.title.new', ['type' => trans_choice('projects::general.invoices', 1)]),
            ],
            'html' => view('projects::modals.invoice', compact('project', 'project_invoicing_type'))->render(),
        ]);
    }

    public function storeInvoice(Project $project, InvoiceRequest $request)
    {
        $response = $this->ajaxDispatch(new CreateInvoice($project, $request));

        if ($response['success']) {
            $response['message'] = trans('messages.success.added', ['type' => trans_choice('general.invoices', 1)]);

            $response['redirect'] = route('invoices.show', $response['data']->id);

            flash($response['message'])->success();
        } else {
            $response['redirect'] = route('projects.projects.show', $project->id);

            flash($response['message'])->error()->important();
        }

        return response()->json($response);
    }

    public function attachments(Project $project, \Illuminate\Http\Request $request)
    {
        foreach ($request->file('attachment') ?? [] as $attachment) {
            $project->attachMedia($this->getMedia($attachment, "projects/{$project->id}"), 'attachment');
        }

        flash(trans('messages.success.added', ['type' => trans_choice('general.attachments', 1)]))->success();

        return response()->json([
            'success' => true,
            'error' => false,
            'redirect' => route('projects.projects.show', $project->id . '#attachments')
        ]);
    }

    public function printProject(Project $project, Request $request)
    {
        return $this->index();

        $date_format = $this->getCompanyDateFormat();
        $transactions = $this->prepareDataForTransactionsTab($project, 999999);
        $profitMarginOfProject = $this->calculateProfitMargin($project);

        return view('projects::projects.print', compact('transactions', 'project', 'profitMarginOfProject', 'date_format'));
    }

    public function exportTransactions(Project $project)
    {
        return $this->exportExcel(new TransactionsExport($project->id), trans_choice('projects::general.transaction', 2));
    }

    public function exportTimesheets(Project $project)
    {
        return $this->exportExcel(new TimesheetsExport($project->id), trans_choice('projects::general.timesheet', 2));
    }

    public function getStatusTaks()
    {

        $statuses = TaskStatuses::orderBy('name')
            ->take(setting('default.select_limit'))
            ->pluck('name', 'id');


        return response()->json([
            'success' => true,
            'error' => false,
            "data" =>  $statuses,
            'message' => '',
        ]);
    }
}
