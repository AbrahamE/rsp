<?php

namespace Modules\Projects\Http\Controllers;

use App\Abstracts\Http\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Projects\Jobs\Comments\CreateComment;
use Modules\Projects\Jobs\Comments\DeleteComment;
use Modules\Projects\Jobs\Comments\UpdateComment;
use Modules\Projects\Http\Requests\Comment as RequestComments;
use Modules\Projects\Models\Comment;
use Modules\Projects\Models\Discussion;
use Modules\Projects\Models\Project;
use Modules\Projects\Models\Task;
use Modules\Projects\Traits\Modals;
use App\Traits\DateTime;

class Comments extends Controller
{
    use DateTime, Modals;
    
    public function show(Project $project)
    {
        if (request()->has('discussion_id')) 
            return $this->commentsDiscussion(
                (string) request()->get('type_comment'), 
                Discussion::find(request()->get('discussion_id')),
                $project
            );
        if (request()->has('task_id')) 
            return $this->commentsTasks(
                (string) request()->get('type_comment'), 
                Task::find(request()->get('task_id')),
                $project
            );
    }

    private function commentsDiscussion($type, $discussion, $project){
        $response_json = $this->getModal("$type", compact('project', 'discussion'));

        $data = $response_json->getData();

        if ($data->success) {
            $data->data->title = $discussion->subject;
            $response_json->setData($data);
        }
        return $response_json;
    } 

    private function commentsTasks($type, $task, $project){
        $response_json = $this->getModal("$type", compact('project', 'task'));

        $data = $response_json->getData();

        if ($data->success) {
            $data->data->title = $task->name;
            $response_json->setData($data);
        }

        return $response_json;
    } 

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Project $project, RequestComments $request)
    {
        $param = $request->only(['company_id', 'project_id', 'discussion_id','task_id', 'attachment', 'comment']);
        
        $response = $this->ajaxDispatch(new CreateComment($param));

        $tab = $request->has('task_id') ? '#tasks' : '#discussions';

        $response['redirect'] = route('projects.projects.show', $project->id) . $tab;

        if ($response['success']) {
            $message = trans('projects::messages.success.comment', [
                'type' => trans_choice('projects::general.comments', 1),    
            ]);

            flash($message)->success();
        } else {
            $message = $response['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Project $project, Discussion $discussion, Comment $comment, Request $request)
    {
        $response = $this->ajaxDispatch(new UpdateComment($comment, $request));

        $response['redirect'] = route('projects.projects.show', $project->id) . '#discussions';

        if ($response['success']) {
            $message = trans('projects::messages.success.updated', [
                'type' => trans_choice('projects::general.comments', 1),
            ]);

            flash($message)->success();
        } else {
            $message = $response['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Project $project, Discussion $discussion, Comment $comment, Request $request)
    {
        $request->merge([
            'company_id' => company_id(),
        ]);

        $response = $this->ajaxDispatch(new DeleteComment($request));

        $response['redirect'] = route('projects.projects.show', $project->id) . '#discussions';

        if ($response['success']) {
            $message = trans('projects::messages.success.deleted', [
                'type' => trans_choice('projects::general.comments', 1),
            ]);

            flash($message)->success();
        } else {
            $message = $response['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }
}
