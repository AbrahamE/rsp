<?php

namespace Modules\Projects\Database\Seeds;

use App\Abstracts\Model;
use App\Models\Setting\EmailTemplate;
use Illuminate\Database\Seeder;

class Templates extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->create();

        Model::reguard();
    }

    private function create()
    {
        $company_id = $this->command->argument('company');

        $templates = [
            [
                'alias'    => 'projects_task_assignment_member',
                'class'    => 'Modules\Projects\Notifications\Tasks\MemberAssignment',
                'name'     => 'projects.task_assignment_user.name',
                'messages' => 'projects.task_assignment_user',
            ],
            [
                'alias'    => 'projects_task_assignment_liable',
                'class'    => 'Modules\Projects\Notifications\Tasks\LiableAssignment',
                'name'     => 'projects.task_assignment_Liable.name',
                'messages' => 'projects.task_assignment_Liable',
            ],
            [
                'alias'    => 'projects_task_update',
                'class'    => 'Modules\Projects\Notifications\Tasks\TaskUpdate',
                'name'     => 'projects.task_update.name',
                'messages' => 'projects.task_update',
            ],
            [
                'alias'    => 'projects_task_status_update',
                'class'    => 'Modules\Projects\Notifications\Tasks\TaskStatusUpdate',
                'name'     => 'projects.status_update.name',
                'messages' => 'projects.status_update',
            ],
            [
                'alias'    => 'projects_task_create_comment',
                'class'    => 'Modules\Projects\Notifications\Tasks\TasksCreateComment',
                'name'     => 'projects.task_create_comment.name',
                'messages' => 'projects.task_create_comment',
            ],
        ];

        foreach ($templates as $template) {
            EmailTemplate::firstOrCreate([
                'company_id' => $company_id,
                'alias' => $template['alias'],
                'class' => $template['class'],
                'name' => "projects::".$template['name'],
                'subject' => trans('projects::email_templates.' . $template['messages'] . '.subject'),
                'body' => trans('projects::email_templates.' . $template['messages'] . '.body'),
            ]);
        }
    }
}
