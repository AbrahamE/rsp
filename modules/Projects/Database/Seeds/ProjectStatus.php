<?php

namespace Modules\Projects\Database\Seeds;

use App\Abstracts\Model;
use App\Models\Common\Report;
use Illuminate\Database\Seeder;
use Modules\Projects\Models\ProjectStatus as ModelsProjectStatus;

class ProjectStatus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->create();

        Model::reguard();
    }

    private function create()
    {

        $rows = [
            [
                'id_status' => '1',
                'name' => 'Por Iniciar',  
            ],
            [
                'id_status' => '2',
                'name' => 'Suspendido',  
            ],
            [
                'id_status' => '3',
                'name' => 'En Progreso',  
            ],
            [
                'id_status' => '4',
                'name' => 'Culminado',  
            ],
            
        ];

        foreach ($rows as $row) {
            ModelsProjectStatus::create($row);
        }
    }
}
