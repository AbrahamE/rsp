<?php

return [

    'title' => 'Módulo de Proyecto|Módulo de Proyectos',
    'projects' => 'Módulo de Proyecto|Módulo de Proyectos',
    'dashboard' => 'Tablero',
    'tasks' => 'Tarea|Tareas',
    'timesheets' => 'Hoja de horas|Horas de hojas',
    'milestones' => 'Hito|Hitos',
    'documents' => 'Docuemento|Documentos',
    'discussions' => 'Discusión|Discusiones',
    'transactions' => 'Transacción|Transacciones',
    'activities' => 'Actividad|Actividades',
    'percentage' => 'Porcentaje de avance',
    'new' => 'Nuevo',
    'status' => 'Estatus',
    'liable' => 'Responsable',
    'tasks_status' => 'Estatus de tarea',
    'inprogress' => 'En progreso', 
    'inprogress' => 'En progreso',
    'to_start' => 'Por Iniciar',
    'suspended' => 'Supendido',
    'active' => 'Activo',
    'completed' => 'Culminado',
    'canceled' => 'Cancelado',
    'success' => 'El proyecto se crea exitosamente.',
    'mustfield' => 'Llenar campos básicos.',
    'overview' => 'Descripción general.',
    'edit' => 'Editar',
    'invoices' => 'Factura|Facturas',
    'revenues' => 'Ganancia|Ganancias',
    'bills' => 'Bill|Bills',
    'payments' => 'Pago|Pagos',
    'files' => 'Archivo|Archivos',
    'loss' => 'Pérdida',
    'profit' => 'Ganancia',
    'margin' => 'Margen',
    'latest' => 'El último',
    'types' => 'Tipo|Tipos',
    'deadline' => 'Fecha límite',
    'priorities' => 'Prioridad|Prioridades',
    'low' => 'Bajo',
    'medium' => 'Medio',
    'high' => 'Alta',
    'urgent' => 'Urgente',
    'not-started' => 'No Iniciado',
    'subject' => 'Asunto',
    'created_by' => 'Creado por',
    'total' => 'Total',
    'last' => 'Último',
    'comments' => 'Comentario|Comentarios',
    'likes' => 'Me gusta|Gustos',
    'dislike' => 'Dislike',
    'enter' => 'Enter',
    'recently' => 'Recientemente',
    'added' => 'Añadido',
    'summary' => 'Resumen',
    'members' => 'Miembro|Miembros',
    'followers' => 'Seguidores',
    'hide' => 'Ocultar',
    'net' => 'Red',
    'view' => 'Ver',
    'close' => 'Cerrar',
    'feed' => 'Feed',
    'billable' => 'Facturable',
    'not_billable' => 'No facturable',
    'is_visible_to_customer' => 'Visible para el cliente',
    'party' => 'Parte',
    'due_date' => 'Fecha de vencimiento',
    'time_hourly' => 'Hora (h)',
    'time_decimal' => 'Hora (decimal)',
    'info' => 'Información',
    'assignees' => 'Asignatario|Asignatarios',
    'timer' => 'Temporizador',
    'Billing' => 'Factura',
    'billing_type' => 'Tipo de facturación',
    'billing_rate' => 'Tarifa de facturación',
    'fixed_rate' => 'Tarifa Fija',
    'projects_hours' => 'Horas de proyecto',
    'task_hours' => 'Horas Tarea',
    'total_rate' => 'Tarifa Total',
    'rate_per_hour' => 'Tarifa por hora',
    'start' => 'Inicio',
    'stop' => 'Parada',
    'single_line' => 'Línea simple',
    'task_per_item' => 'Tarea por artículo',
    'all_timesheets_individually' => 'Todas las hojas de tiempo individualmente',
    'invoiced_tasks' => 'Todas las tareas facturadas se marcarán como completadas',
    'hours' => 'Horas',
    'days' => 'Días',
    'attachments' => 'Archivos adjuntos',
    'file_name' => 'Nombre del Archivo',
    'file_size' => 'Tamaño del Archivo',
    'file' => 'Archivo',
    'created_from' => 'Creado desde',
    
    'empty' => [
        'projects' => 'Los proyectos le permiten gestionar el trabajo en equipo y entregar los proyectos a tiempo. Puede crear un número ilimitado de proyectos, adjuntarlos a las transacciones de compra y venta, discutir con su equipo y mucho más',
        'tasks' => 'Las tareas le permiten gestionar el trabajo en equipo y entregar los proyectos a tiempo. Puede crear un número ilimitado de proyectos, adjuntarlos a las transacciones de compra y venta, discutir con su equipo y mucho más',
        'timesheets' => 'Las hojas de tiempo le permiten gestionar el trabajo en equipo y entregar los proyectos a tiempo. Puede crear un número ilimitado de proyectos, adjuntarlos a las transacciones de compra y venta, discutir con su equipo y mucho más',
        'hitos' => 'Los hitos le permiten gestionar el trabajo en equipo y entregar los proyectos a tiempo. Puede crear un número ilimitado de proyectos, adjuntarlos a las transacciones de ventas y compras, discutir con su equipo y mucho más',

    ],

    'no_records' => [
        'tasks' => 'Vea todas las tareas de su proyecto aquí. Las tareas están ordenadas por Nombre, Fecha de inicio/finalización, Estado y Prioridad. Añade una nueva tarea desde el botón verde de arriba',
        'timesheets' => 'Vea el tiempo que dedica a cada tarea aquí. La información de la hoja de horas está ordenada en Miembro, Tarea, Fechas de inicio/final y Tiempo (en horas). Sólo puede añadir una hoja de horas para una tarea existente',
        'milestones' => 'Vea todos sus hitos aquí. La información sobre los hitos se ordena en Nombre, Fecha de finalización y Descripción. Los hitos pueden ser creados por separado y asociados con las tareas',
        'discusiones' => 'Vea todos sus comentarios aquí. Las discusiones se clasifican en Asunto, Creado por, Comentarios y Me gusta. Puede crear discusiones independientes de cualquier tarea.',
        'transacciones' => 'Todas las transacciones de su proyecto - ingresos y gastos - se enumeran aquí. Los detalles de las transacciones se clasifican en Fecha y Número de Transacción, Tipo y Categoría, Cuenta, Contacto y Documento, e Importe',
    ],

    'widgets' => [
        'latest_income' => 'Últimos ingresos',
        'latest_income_by_project' => 'Últimos ingresos por proyecto',
        'active_discussion' => 'Discusiones Activas',
        'active_discussion_by_project' => 'Discusiones activas por proyecto',
        'recently_added_task' => 'Tareas añadidas recientemente',
        'recently_added_task_by_project' => 'Tareas Añadidas Recientemente Por Proyecto',
        'total_invoice' => 'Factura total',
        'total_revenue' => 'Ingresos Totales',
        'total_bill' => 'Factura Total',
        'total_payment' => 'Pago Total',
        'total_activity' => 'Actividad Total',
        'total_task' => 'Total Tarea',
        'total_days' => 'Total Días',
        'total_discussion' => 'Total Discusión',
        'total_user' => 'Total de usuarios',
        'line_chart' => 'Line Chart',
        'cash_flow_by_project' => 'Flujo de caja por proyecto',
        'tasks_by_statuses'  => 'Tareas por estado',
        'tasks_by_milestone'  => 'Tareas por Hitos',
        'portal' => [
            'project_brief' => 'Resumen del proyecto',
            'progress_completed_tasks' => 'Tareas completadas',
            'progress_days_left' => 'Días restantes',
            'most_logged_tasks' => 'Tareas más registradas',
        ],
    ],

    'reports' => [

        'name' => [
            'profit_loss' => 'Pérdidas y ganancias (Proyectos)',
            'income_summary' => 'Resumen de Ingresos (Proyectos)',
            'expense_summary' => 'Resumen de Gastos (Proyectos)',
            'income_expense' => 'Ingresos vs Gastos (Proyectos)',
        ],

        'description' => [
            'profit_loss' => 'Pérdidas y ganancias trimestrales por proyecto',
            'income_summary' => 'Resumen mensual de ingresos por proyecto',
            'expense_summary' => 'Resumen mensual de gastos por proyecto',
            'income_expense' => 'Ingresos vs gastos mensuales por proyecto',
        ],
    ],

    'tooltips' => [
        'single_line' => 'Item name: Project name <br> Item description: All tasks + total logged time per task',
        'task_per_item' => 'Item name: Project name + Task name <br> Item description: Total logged time',
        'all_timesheets_individually' => 'Item name: Project name + Task name <br> Item description: Timesheet logged time & timesheet description',
    ],

    'show_accordion' => [
        'description' => 'Ver proyectos relacionados',
        'project' => 'El proyecto adjunto es :project',
    ],

    'error' => [
        'project_not_found' => 'Proyecto no encontrado',
        'task_status'  => 'Status ya existente' 
    ],  


];
