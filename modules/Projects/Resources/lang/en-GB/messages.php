<?php

return [

    'success' => [
        'added' => ':¡tipo añadido!',
        'updated' => ':¡tipo actualizado!',
        'eliminado' => ':¡tipo eliminado!',
        'duplicado' => ':¡tipo duplicado!',
        'importado' => ':¡tipo importado!',
        'activado' => ':¡tipo activado!',
        'disabled' => ':type disabled!',
        'iniciado' => ':type ¡iniciado!',
        'parado' => ':¡tipo parado!',
    ],

    'error' => [
        'over_payment' => 'Error: ¡Pago no añadido! El importe introducido sobrepasa el total: :importe',
        'not_user_company' => 'Error: No tiene permiso para gestionar esta empresa',
        'customer' => 'Error: ¡Usuario no creado! :nombre ya utiliza esta dirección de correo electrónico',
        'no_file' => 'Error: No file selected!',
        'last_category' => 'Error: No se puede eliminar la última categoría :type!',
        'change_type' => 'Error: No se puede cambiar el tipo porque tiene :text relacionado!',
        'invalid_apikey' => 'Error: La Clave API introducida no es válida!',
        'import_column' => 'Error: :mensaje Nombre de hoja: :hoja. Número de línea: :line.',
        'import_sheet' => 'Error: El nombre de la hoja no es válido. Por favor, compruebe el archivo de ejemplo',
        'unknown' => 'Error desconocido. Por favor, inténtelo de nuevo más tarde',
        'timesheet_for_invoice' => 'La tarea no tiene ninguna hoja de horas',
        'prevent_user_delete' => 'No puede eliminar este usuario porque tiene proyectos y el proyecto también contiene tareas y comentarios asociados al usuario.',
    ],

    'warning' => [
        'deleted' => 'Warning: You are not allowed to delete <b>:name</b> because it has :text related.',
        'disabled' => 'Warning: You are not allowed to disable <b>:name</b> because it has :text related.',
        'disable_code' => 'Warning: You are not allowed to disable or change the currency of <b>:name</b> because it has :text related.',
        'payment_cancel' => 'Warning: You have cancelled your recent :method payment!',
    ],

];
