<?php

return [
    'created' => [

        'invoice'       => ':user has created a new invoice, :number.',
        'bill'          => ':user has created a new bill, :number.',
        'income'        => ':user has added an income, :number.',
        'expense'       => ':user has added an expense, :number.',
        'project'       => ':user has created a new project titled :project.',
        'comment'       => [
            'discussion' => ':user has commented on a discussion board, :discussion.',
            'task'      => ':user has commented on a task list, :task.',
        ],
        'discussion'    => ':user has created a discussion board titled :discussion.',
        'milestone'     => ':user has created a milestone titled :milestone.',
        'task'          => ':user has created an item on a task list, :task.',

    ],

    'updated' => [
        'invoice'       => ':user has updated the invoice, :number.',
        'bill'          => ':user has updated the bill, :number.',
        'income'        => ':user has updated the income, :number.',
        'expense'       => ':user has updated the expense, :number.',
        'project'       => ':user has updated the project titled :project.',
        'comment'       => [
            'discussion' => ':user has updated a comment on the discussion board, :discussion.',
            'task'      => ':user has updated a comment on the task list, :task.',
        ],
        'discussion'    => ':user has updated the discussion board titled :discussion.',
        'milestone'     => ':user has updated the milestone titled :milestone.',
        'task'          => ':user has updated the item on the task list, :task.',
    ],

    'deleted' => [
        'invoice'       => ':user has deleted the invoice, :number.',
        'bill'          => ':user has deleted the bill, :number.',
        'income'        => ':user has deleted the income, :number.',
        'expense'       => ':user has deleted the expense, :number.',
        'project'       => ':user has deleted the project titled :project.',
        'comment'       => [
            'discussion' => ':user has deleted a comment on the discussion board, :discussion.',
            'task'      => ':user has deleted a comment on the task list, :task.',
        ],
        'discussion'    => ':user has deleted the discussion board titled :discussion.',
        'milestone'     => ':user has deleted the milestone titled :milestone.',
        'task'          => ':user has deleted the item on the task list, :task.',
    ],
];
