<?php

return [

    'billing'                   => 'Facturación',
    'task_status'               => 'Estatus de la tarea',
    'form_description' => [
        'general' => 'Seleccione una categoría para que sus informes sean más detallados. La descripción se rellenará cuando el artículo se seleccione en una factura o recibo',
        'billing' => 'Determine si este proyecto se facturará. Por defecto, el proyecto no se facturará',
        'fiel' => 'Determine si este proyecto puede ser facturado. Por defecto, el proyecto puede ser facturado',
        'other' => 'Adjunte sus archivos a este proyecto. Puede subir cualquier tipo de archivo, incluyendo imágenes, documentos, hojas de cálculo y PDFs',
        'task_status' => 'Determine cuales seran los status que van a tener las tareas de este proyecto. Por defecto, todas las tareas se consideran completadas',
    ],

];
