<?php

return [

    'form_description' => [
        'general' => 'Seleccione una categoría para que sus informes sean más detallados. La descripción se rellenará cuando se seleccione el artículo en una factura o recibo.',
    ],

];
