<?php

return [
    'projects' => [
        'task_assignment_user'=>[
            'subject'       => 'New Task Assigned!',
            'body'          => 'Dear {member_name},<br /><br />New task assigned. Further information related to the task is located below.<br /><br />Task Name: {task_name}<br /><br />Task Description: {task_description}<br /><br />Task Status: {status}<br /><br />Task Started: {task_started_at}<br /><br />Task Deadline: {task_deadline_at}<br /><br />Task Priority: {task_priority}<br /><br />Best Regards,<br />{company_name}',
            'name'           => 'Asignación de miembros para la tarea del proyecto',
        ],
        'task_assignment_Liable'=>[
            'subject'       => 'New Task Assigned!',
            'body'          => 'Dear {member_name},<br /><br />New task assigned. Further information related to the task is located below.<br /><br />Task Name: {task_name}<br /><br />Task Description: {task_description}<br /><br />Task Status: {status}<br /><br />Task Started: {task_started_at}<br /><br />Task Deadline: {task_deadline_at}<br /><br />Task Priority: {task_priority}<br /><br />Best Regards,<br />{company_name}',
            'name'          => 'Asignación de miembros para la tarea del proyecto',
        ]
    ]

];
