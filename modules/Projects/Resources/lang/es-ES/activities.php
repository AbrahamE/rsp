<?php

return [
    'created' => [

        'invoice'       => ':user ha creado una nueva factura, :number.',
        'bill'          => ':user ha creado una nueva cuenta, :number.',
        'income'        => ':user ha añadido un ingreso, :number.',
        'expense'       => ':user ha añadido un gasto, :number.',
        'project'       => ':user ha creado un nuevo proyecto titulado :project.',
        'comment'       => [
            'discussion' => ':user ha comentado en un foro de discusión, :discussion.',
            'task'      => ':user ha comentado en una lista de tareas, :task.',
        ],
        'discussion'    => ':user ha creado un foro de discusión titulado :discussion.',
        'milestone'     => ':user ha creado un hito titulado :milestone.',
        'task'          => ':user ha creado un elemento en una lista de tareas, :task.',

    ],

    'updated' => [
        'invoice'       => ':user ha actualizado la factura, :number.',
        'bill'          => ':user ha actualizado la cuenta, :number.',
        'income'        => ':user ha actualizado el ingreso, :number.',
        'expense'       => ':user ha actualizado el gasto, :number.',
        'project'       => ':user ha actualizado el proyecto titulado :project.',
        'comment'       => [
            'discussion' => ':user ha actualizado un comentario en el foro de discusión, :discussion.',
            'task'      => ':user ha actualizado un comentario en la lista de tareas, :task.',
        ],
        'discussion'    => ':user ha actualizado el foro de discusión titulado :discussion.',
        'milestone'     => ':user ha actualizado el hito titulado :milestone.',
        'task'          => ':user ha actualizado el elemento en la lista de tareas, :task.',
    ],

    'deleted' => [
        'invoice'       => ':user ha eliminado la factura, :number.',
        'bill'          => ':user ha eliminado la cuenta, :number.',
        'income'        => ':user ha eliminado el ingreso, :number.',
        'expense'       => ':user ha eliminado el gasto, :number.',
        'project'       => ':user ha eliminado el proyecto titulado :project.',
        'comment'       => [
            'discussion' => ':user ha eliminado un comentario en el foro de discusión, :discussion.',
            'task'      => ':user ha eliminado un comentario en la lista de tareas, :task.',
        ],
        'discussion'    => ':user ha eliminado el foro de discusión titulado :discussion.',
        'milestone'     => ':user ha eliminado el hito titulado :milestone.',
        'task'          => ':user ha eliminado el elemento en la lista de tareas, :task.',
    ],
];
