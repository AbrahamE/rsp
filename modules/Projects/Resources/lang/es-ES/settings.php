<?php

return [

    'email' => [

        'templates' => [
            'tags' => '<strong>Etiquetas disponibles:</strong> :tag_list',
            'projects' => [
                'task_assignment_user'=>[
                    'subject' => 'Tarea Asignada',
                    'body' => 'Un cordial saludo {member_name}, Se te a asignado una tarea con el nombre {task_name}, y su descripcion es: {task_description}',
                    'name' => 'Asignación de miembros para la tarea del proyecto',
                ],
                'task_assignment_Liable'=>[
                    'subject' => 'Tarea Asignada',
                    'body' => 'Un cordial saludo {member_name}, Se te a asignado una tarea con el nombre {task_name}, y su descripcion es: {task_description}',
                    'name' => 'Asignación de miembros para la tarea del proyecto',
                ]
            ]
        ]
    ],

];
