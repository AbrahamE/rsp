<?php

return [

    'projects' => [
        'task_assignment_user'  => [
            'subject' => 'Tarea Asignada',
            'body'    => 'Estimado/a {use_name},<br /><br />Nueva tarea asignada. La información relacionada con la tarea se encuentra a continuación.<br /><br />Nombre de la tarea: {name}<br /><br />Descripción de la tarea: {description}<br /><br />Estado de la tarea: {status}<br /><br />Tarea iniciada: {started_at}<br /><br />Fecha límite de la tarea: {deadline_at}<br /><br />Prioridad de la tarea: {priority}<br /><br />Atentamente,<br /> {company_name}<br /> {route}',
            'name'    => 'Asignación de miembros para la tarea del proyecto',
        ],
        'task_assignment_Liable' => [
            'subject' => 'Tarea Asignada',
            'body'    => 'Estimado/a {use_name},<br /><br />Nueva tarea asignada. La información relacionada con la tarea se encuentra a continuación.<br /><br />Nombre de la tarea: {name}<br /><br />Descripción de la tarea: {description}<br /><br />Estado de la tarea: {status}<br /><br />Tarea iniciada: {started_at}<br /><br />Fecha límite de la tarea: {deadline_at}<br /><br />Prioridad de la tarea: {priority}<br /><br />Atentamente,<br /> {company_name}<br /> {route}',
            'name'    => 'Asignación de miembros para la tarea del proyecto'
        ],
        'task_create_comment'    => [
            'subject' => 'Nuevo Comentario en la Tarea',
            'body'    => 'Estimado/a {use_name},<br /><br />Se ha añadido un nuevo comentario a la tarea.<br /><br />Nombre de la tarea: {name}<br /><br />Comentario: {comment}<br /><br />Atentamente,<br /> {company_name}<br /> {route}',
            'name'    => 'Notificación de nuevo comentario en la tarea'
        ],
        'task_create'            => [
            'subject' => 'Nueva Tarea Creada',
            'body'    => 'Estimado/a {use_name},<br /><br />Se ha creado una nueva tarea. La información relacionada con la tarea se encuentra a continuación.<br /><br />Nombre de la tarea: {name}<br /><br />Descripción de la tarea: {description}<br /><br />Fecha límite de la tarea: {deadline_at}<br /><br />Prioridad de la tarea: {priority}<br /><br />Atentamente,<br /> {company_name}<br />{route}',
            'name'    => 'Notificación de nueva tarea creada'
        ],
        'status_update'     => [
            'subject' => 'Actualización del Estado de la Tarea',
            'body'    => 'Estimado/a {use_name},<br /><br />El estado de la tarea ha sido actualizado.<br /><br />Nombre de la tarea: {name}<br /><br />Nuevo estado: {status}<br /><br />Atentamente,<br /> {company_name}<br />{route}',
            'name'    => 'Notificación de actualización del estado de la tarea'
        ],
        'task_update'           => [
            'subject' => 'Tarea Actualizada',
            'body'    => 'Estimado/a {use_name},<br /><br />La tarea ha sido actualizada. La información relacionada con la tarea se encuentra a continuación.<br /><br />Nombre de la tarea: {name}<br /><br />Descripción actualizada: {description}<br /><br />Estado actual: {status}<br /><br />Atentamente,<br /> {company_name}<br />{route}',
            'name'    => 'Notificación de actualización de la tarea'
        ]
    ]
    
];
