<x-form id="form-create-tasks" route="" :model="$models->task">
    <x-tabs active="general" class="grid grid-cols-2" override="class" ignore-hash>
        <x-slot name="navs">
            <x-tabs.nav id="general">
                {{ trans('general.general') }}
            </x-tabs.nav>

            <x-tabs.nav id="other">
                {{ trans_choice('general.others', 1) }}
            </x-tabs.nav>
        </x-slot>

        <x-slot name="content">
            <x-tabs.tab id="general">
                <div class="grid sm:grid-cols-6 gap-x-8 gap-y-6 my-3.5">
                    <x-form.group.text name="name" :label="trans('general.name')" disabled="true" form-group-class="col-span-6" />

                    <x-form.group.date name="started_at" :label="trans('general.start_date')" :show-date-format="company_date_format()" disabled="true"
                        form-group-class="col-span-6" />

                    <x-form.group.date name="deadline_at" :label="trans('general.end_date')" :show-date-format="company_date_format()" not-required
                        disabled="true" form-group-class="col-span-6" />

                    <x-form.group.money name="hourly_rate" :label="trans('projects::general.rate_per_hour')" :currency="$currency" :v-show="$is_task_hours"
                        :value="$models->task->hourly_rate" disabled="true" form-group-class="col-span-6" />

                    <x-form.group.select name="users" :label="trans_choice('projects::general.members', 2)" :options="$users" :selected="isset($models->task) ? $models->task->users->pluck('user_id') : []"
                        multiple collapse not-required v-disabled="true" form-group-class="col-span-6" />

                    <x-form.group.select name="liable" :label="trans('projects::general.liable')"  :options="$users"  :selected="isset($models->task) ? $models->task->liable->pluck('user_id') : []"
                        multiple form-group-class="sm:col-span-6"  v-disabled="true" />

                    <x-form.group.select name="status_id" :label="trans_choice('projects::general.status', 1)" :options="$models->project->taskStatuses->pluck('name', 'id')"
                        form-group-class="col-span-6" :selected="$models->task->statusCurrent->status_id" v-disabled="true" />
                </div>
            </x-tabs.tab>

            <x-tabs.tab id="other">
                <div class="grid sm:grid-cols-6 gap-x-8 gap-y-6 my-3.5">
                    <x-form.group.textarea name="description" :label="trans('general.description')" not-required disabled="true"
                        form-group-class="col-span-6" />

                    <x-form.group.select name="milestone_id" :label="trans_choice('projects::general.milestones', 1)" :options="$milestones" not-required
                        v-disabled="true" form-group-class="col-span-6" />

                    <x-form.group.select name="priority" :label="trans_choice('projects::general.priorities', 1)" :options="$priorities"
                        form-group-class="col-span-6" not-required v-disabled="true" />

                    <x-form.group.number name="advance" :label="trans('projects::general.percentage')" not-required disabled="true" :value="$models->task->advance" form-group-class="col-span-6" />

                    @if ($models->task->attachment)
                        <div class="grid-rows-1 sm:col-span-6">
                            @foreach ($models->task->attachment as $file)
                                <x-media.file :file="$file" />
                            @endforeach
                        </div>
                    @endif

                    <x-form.input.hidden name="project_id" :value="$models->project->id" />
                </div>
            </x-tabs.tab>

        </x-slot>
    </x-tabs>
</x-form>
