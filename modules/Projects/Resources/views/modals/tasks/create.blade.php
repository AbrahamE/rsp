@php
    $is_portal = user()->isCustomer() ? 'portal.' : '';
@endphp

<x-form id="form-create-tasks" method="POST" :route="[$is_portal . 'projects.tasks.store', $models->project->id]" :model="$models->task ?? false">
    <x-tabs active="general" class="grid grid-cols-2" override="class" ignore-hash>
        <x-slot name="navs">
            <x-tabs.nav id="general">
                {{ trans('general.general') }}

                <span class="invalid-feedback block text-xs text-red whitespace-normal"
                    v-if="form.errors.has('name') || form.errors.has('hourly_rate') || form.errors.has('users') || form.errors.has('email_send_me') || form.errors.has('priority')">
                    {{ trans('general.validation_error') }}
                </span>
            </x-tabs.nav>

            <x-tabs.nav id="other">
                {{ trans_choice('general.others', 1) }}

                <span class="invalid-feedback block text-xs text-red whitespace-normal"
                    v-if="form.errors.has('description') || form.errors.has('attachment')">
                    {{ trans('general.validation_error') }}
                </span>
            </x-tabs.nav>
        </x-slot>

        <x-slot name="content">
            <x-tabs.tab id="general">
                <div class="grid sm:grid-cols-6 gap-x-8 gap-y-6 my-3.5">
                    <x-form.group.text name="name" :label="trans('general.name')" form-group-class="col-span-6" />

                    <x-form.group.date name="started_at" :label="trans('general.start_date')" value="{{ Date::now()->toDateString() }}"
                        :show-date-format="company_date_format()" form-group-class="col-span-6" />

                    <x-form.group.date name="deadline_at" :label="trans('general.end_date')" :show-date-format="company_date_format()"
                        form-group-class="col-span-6" not-required />

                    <x-form.group.money name="hourly_rate" :label="trans('projects::general.rate_per_hour')" :currency="$currency" :v-show="$is_task_hours"
                        value="0" form-group-class="col-span-6" />

                    <x-form.group.select name="users" :label="trans_choice('projects::general.members', 2)" :options="$users"
                        multiple collapse form-group-class="col-span-6" not-required />

                    <x-form.group.select name="liable" :label="trans('projects::general.liable')" :options="$users"
                    multiple collapse form-group-class="sm:col-span-6" not-required  />

                    <x-form.group.select name="status_id" :label="trans_choice('projects::general.status', 1)" :options="$models->project->taskStatuses->pluck('name', 'id') ?? []"
                        form-group-class="col-span-6" />

                    <x-form.group.checkbox name="email_send_me"
                        v-show="Array.isArray(form.users) && form.users.find(u => u == {{ user()->id }}) ? true : false"
                        :options="['1' => trans('projects::tasks.email_send_me', ['email' => user()->email])]" checkbox-class="col-span-6" />
                </div>
            </x-tabs.tab>

            <x-tabs.tab id="other">
                <div class="grid sm:grid-cols-6 gap-x-8 gap-y-6 my-3.5">
                    <x-form.group.textarea name="description" :label="trans('general.description')" not-required />

                    <x-form.group.select name="milestone_id" :label="trans_choice('projects::general.milestones', 1)" :options="$milestones"
                        form-group-class="col-span-6" required />

                    <x-form.group.select name="priority" :label="trans_choice('projects::general.priorities', 1)" :options="$priorities"
                        form-group-class="col-span-6" not-required />

                    <x-form.group.number name="advance" :label="trans('projects::general.percentage')" not-required form-group-class="col-span-6" />

                    <x-projects::form.attachment />

                    <x-form.input.hidden name="project_id" :value="$models->project->id" />
                        
                    <x-form.input.hidden name="status" value="1" />
                </div>
            </x-tabs.tab>
        </x-slot>
    </x-tabs>
</x-form>
