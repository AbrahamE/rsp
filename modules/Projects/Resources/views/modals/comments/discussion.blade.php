@php
    $is_portal = user()->isCustomer() ? 'portal.' : '';
@endphp

<x-form id="form-create-comments" method="POST" :route="[$is_portal . 'projects.comments.store', [(int) $models->project->id, $models->discussion->id]]">

    {{-- <x-tabs active="general" class="grid grid-cols-2" override="class" ignore-hash>
        <x-slot name="navs">
            <x-tabs.nav id="general">
                {{ trans('general.general') }}
            </x-tabs.nav>

            <x-tabs.nav id="other">
                {{ trans_choice('general.others', 1) }}

                <span class="invalid-feedback block text-xs text-red whitespace-normal" v-if="form.errors.has('description') || form.errors.has('attachment')">
                    {{ trans('general.validation_error') }}
                </span>
            </x-tabs.nav>
        </x-slot> --}}

        {{-- <x-slot name="content">
            <x-tabs.tab id="general"> --}}
                <div class="grid sm:grid-cols-6 gap-x-8 gap-y-6 my-3.5">
                    <div class="w-full h-40 overflow-auto mb-6 col-span-6">
                        <p>{{ $models->discussion->description }}</p>
                    </div>

                    
                    @if(count($models->discussion->comments))
                        <div class="w-full border-b pb-5 col-span-6">
                            <h4 class="h-18 text-base font-medium pt-5 pb-5 flex justify-start">
                                {{ $models->discussion->comments->count() }}
                                {{ trans_choice('projects::general.comments', 2) }}
                            </h4>
                            @foreach ($models->discussion->comments as $comment)
                                <div class="flex items-start justify-between py-2">
                                    <div class="mb-5 flex items-start w-8/12">
                                        @php $user = $comment->user; @endphp
                                        @if (setting('default.use_gravatar', '0') == '1')
                                            <img src="{{ $user->picture }}" class="w-6 h-6 rounded-full mr-2 hidden lg:block" title="{{ $user->name }}" alt="{{ $user->name }}">
                                        @elseif (is_object($user->picture))
                                            <img src="{{ Storage::url($user->picture->id) }}" class="w-6 h-6 rounded-full mr-2 hidden lg:block" alt="{{ $user->name }}" title="{{ $user->name }}">
                                        @else
                                            <img src="{{ asset('public/img/user.svg') }}" class="w-6 h-6 rounded-full mr-2 hidden lg:block" alt="{{ $user->name }}"/>
                                        @endif
                                        <div class="grow flex flex-col gap-3">
                                            <p>{{ $comment->comment }}</p>
    
    
                                            @if ($comment->attachment)
                                                @foreach ($comment->attachment as $file)
                                                    <x-media.file :file="$file" />
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>

                                    <time class="ltr:pl-6 rtl:pr-6 ltr:text-right rtl:text-left w-4/12 block mb-2 text-sm font-normal leading-none">
                                        {{ $comment->created_at->diffForHumans() }}
                                    </time>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
                
            {{-- </x-tabs.tab>

            <x-tabs.tab id="other"> --}}
                <div class="grid sm:grid-cols-6 gap-x-8 gap-y-6 my-3.5">  
                    <x-form.group.textarea
                        name="comment"
                        :label="trans_choice('projects::general.comments', 1)"
                    />
                 
                    <x-projects::form.attachment />
                
                    <x-form.input.hidden name="project_id" :value="$models->project->id" />
                    <x-form.input.hidden name="company_id" :value="company_id()" />
                    <x-form.input.hidden name="discussion_id" :value="$models->discussion->id" />
                </div>
            {{-- </x-tabs.tab>
        </x-slot>
    </x-tabs> --}}

 
</x-form>
