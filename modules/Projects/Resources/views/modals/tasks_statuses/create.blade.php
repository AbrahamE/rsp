<x-form id="form-create-status" route="projects.projects.modals.create_tacks_statuses">
    <div class="grid sm:grid-cols-6 gap-x-8 gap-y-6 my-3.5">
        <x-form.group.text name="name" label="{{ trans('general.name') }}" form-group-class="col-span-6" />

        <x-form.group.color name="color" label="{{ trans('general.color') }}" form-group-class="col-span-6" />

        <x-form.input.hidden name="company_id" value="{{ company_id() }}" />

        <x-form.group.checkbox
            name="status_end"
            :options="['1' => 'Ultimo status']"
            checkbox-class="col-span-6"
        />

        @if ($project_id)
            <x-form.input.hidden name="project_id" value="{{ $project_id }}" />
        @endif
    </div>
</x-form>
