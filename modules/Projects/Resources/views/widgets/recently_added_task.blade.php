@php
    $width = isset($class->default_settings)? $class->default_settings['width'] : $class->model->settings->width;
@endphp
<div id="widget-{{ $class->model->id }}" class="{{ $width }} my-8">
    @include($class->views['header'], ['header_class' => ''])

    <ul class="text-sm space-y-3 my-3">
        @foreach($tasks as $task)
            <li class="flex flex-items justify-between">
                <div class="w-8/12 cursor-pointer truncate" @click="onModalAddNew('{{ route('projects.tasks.show', ['project' => $task->project->id, 'task' => $task->id]) }}')">
                    {{ $task->name }}
                </div>
                <div class="w-4/12">{{ $task->at }}</div>
            </li>
        @endforeach
    </ul>
</div>
