@if ($tasks->isNotEmpty())
    <div class="flex flex-col gap-5">

        <gantt :project-tasks="{{ json_encode($tasks_project) }}"
            :project-dependencies="{{ json_encode($dependencies) }}" />

        @push('css')
        
            <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/23.2.6/css/dx.material.blue.light.css" />
            <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/23.2.6/css/dx-gantt.css" />
            <link rel="stylesheet" type="text/css" href={{ asset('modules/Projects/Resources/assets/css/dx-material-light.css?v=' . version('short')) }}>
            <link rel="stylesheet" type="text/css" href={{ asset('modules/Projects/Resources/assets/css/gantt.css?v=' . version('short')) }}>  
            
            <!-- Dictionary files for German language -->
            <script src="https://cdn3.devexpress.com/jslib/23.2.6/js/localization/dx.messages.es.js"></script>
            <!-- Common and language-specific CLDR data -->
            <script src="https://unpkg.com/devextreme-cldr-data/supplemental.js"></script>
            <script src="https://unpkg.com/devextreme-cldr-data/es.js"></script>
        @endpush

    </div>
@else
    <x-projects::show.no-records name="tasks" :project="$project" />
@endif
