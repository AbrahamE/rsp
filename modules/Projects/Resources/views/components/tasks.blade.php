@if ($tasks->isNotEmpty())
    <div class="flex flex-col gap-5">
        <x-index.search search-string="Modules\Projects\Models\Task" bulk-action="Modules\Projects\BulkActions\Tasks" />
        <x-table>
            <x-table.thead>
                <x-table.tr>
                    <x-table.th class="w-4/12 sm:w-4/12">
                        <x-slot name="first">
                            <x-sortablelink column="started_at" title="{{ trans('general.start_date') }}" />
                        </x-slot>
                        <x-slot name="second">
                            <x-sortablelink column="ended_at" title="{{ trans('general.end_date') }}" />
                        </x-slot>
                    </x-table.th>

                    <x-table.th class="w-3/12">
                        {{ trans_choice('general.statuses', 1) }}
                    </x-table.th>

                    <x-table.th class="w-3/12 sm:w-3/12">
                        <x-sortablelink column="name" title="{{ trans('general.name') }}" />
                    </x-table.th>

                    <x-table.th class="w-1/12">
                        <x-sortablelink column="comment"
                            title="{{ trans_choice('projects::general.comments', 2) }}" />
                    </x-table.th>

                    <x-table.th class="w-2/12 sm:w-2/12" kind="right">
                        {{ trans_choice('projects::general.priorities', 1) }}
                    </x-table.th>
                </x-table.tr>
            </x-table.thead>

            <x-table.tbody>
                @foreach ($tasks as $item)
                    <x-table.tr>
                        <x-table.td class="w-4/12 sm:w-4/12">
                            <x-slot name="first">
                                {{ company_date($item->started_at) }}
                            </x-slot>
                            <x-slot name="second">
                                {{ empty($item->deadline_at) ? '-' : company_date($item->deadline_at) }}
                            </x-slot>
                        </x-table.td>

                        <x-table.td class="w-3/12">
                            <span class="flex items-center">
                                @if ($item->statusCurrent)
                                    <span
                                        class="px-2.5 py-1 text-xs font-medium rounded-xl bg-{{ $item->statusCurrent->status->color }} text-white">
                                        {{ $item->statusCurrent->status->name }}
                                    </span>
                                @else
                                    <span class="px-2.5 py-1 text-xs font-medium rounded-xl bg-gray-500 text-white">
                                        Sin status
                                    </span>
                                @endif

                            </span>
                        </x-table.td>

                        <x-table.td class="w-3/12 sm:w-3/12">
                            {{ $item->name }}
                        </x-table.td>

                        <x-table.td class="w-1/12">
                            @if ($item->comments)
                                {{ $item->comments->count() }}
                            @endif
                        </x-table.td>

                        <x-table.td class="w-2/12" kind="right">
                            @if ($item->priority)
                                {{ trans("projects::general.$item->priority") }}
                            @else
                                <x-empty-data />
                            @endif
                        </x-table.td>

                        <x-table.td kind="action">
                            <x-table.actions :model="$item" />
                        </x-table.td>
                    </x-table.tr>
                @endforeach
            </x-table.tbody>
        </x-table>

        <x-pagination :items="$tasks" />
        
    </div>
@else
    <x-projects::show.no-records name="tasks" :project="$project" />
@endif
