<div class="my-5">
    <x-index.search search-string="Modules\Projects\Models\Activity" bulk-action="Modules\Projects\BulkActions\Activities" />
    <x-table>
        <x-table.thead>
            <x-table.tr>
                <x-table.th class="w-6/12">
                    {{ trans_choice('general.description', 1) }}
                </x-table.th>

                <x-table.th class="w-6/12" kind="right">
                    {{ trans_choice('projects::general.priorities', 1) }}
                </x-table.th>
            </x-table.tr>
        </x-table.thead>

        <x-table.tbody>
            @foreach ($activities as $activity)
                <x-table.tr>
                    <x-table.td class="w-6/12 flex gap-5">
                        <div class="w-full flex gap-5">
                            <span
                                class="flex justify-center items-center w-6 h-6 bg-gray-300 rounded-full ring-8 ring-transparent">
                                <span class="material-icons-outlined text-lg">
                                    {{ $activity->getIcon() }}
                                </span>
                            </span>
                            <div class="flex items-center justify-between">
                                <h3 class="mb-2 w-10/12">
                                    {{ $activity->description }}
                                </h3>
                              
                            </div>
                        </div>
                    </x-table.td>
                    <x-table.td class="w-6/12" kind="right">
                        <time>
                            {{ $activity->created_at->diffForHumans() }}
                        </time>
                    </x-table.td>
                </x-table.tr>
            @endforeach
        </x-table.tbody>
    </x-table>
</div>

<x-pagination :items="$activities" />
