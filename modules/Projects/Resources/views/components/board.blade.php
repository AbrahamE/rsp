@if ($tasks->isNotEmpty())
    <div class="flex flex-col gap-5 w-full">
        <kanban
            :blocks="{{ json_encode(
                $tasks->map(function ($task) {
                    $task->status = $task->statusCurrent->status->name;
                    $task->priority = trans("projects::general.$task->priority");
                    $task->created = $task->created_at->format('d/m/Y');
                    return $task;
                }),
            ) }}"
            :stages="{{ json_encode($project->taskStatuses()->pluck('name')) }}"
            :stages-color="{{ json_encode($project->taskStatuses()->pluck('color', 'name')) }}"
            :status="{{ json_encode($project->taskStatuses()->pluck('id', 'name')) }}">
            <template #actions="{ data: action }">

            </template>
        </kanban>
    </div>
    @push('css')
        <link rel="stylesheet" href={{ asset('modules/Projects/Resources/assets/css/kanban.css?v=' . version('short')) }}>
    @endpush
@else
    <x-projects::show.no-records name="board-tasks" :project="$project" />
@endif
