@if ($milestones->isNotEmpty())
    <x-table>
        <x-table.thead>
            <x-table.tr>
                <x-table.th class="w-3/12">
                    <x-sortablelink column="deadline_at" :title="trans('general.end_date')" />
                </x-table.th>

                <x-table.th class="w-2/12">
                    <x-sortablelink column="name" title="{{ trans('general.name') }}" />
                </x-table.th>

                <x-table.th class="w-2/12">
                    <x-sortablelink column="name" title="{{ trans('projects::general.percentage') }}" />
                </x-table.th>

                <x-table.th class="w-2/12">
                    <x-sortablelink column="name" title="{{ trans('projects::general.status') }}" />
                </x-table.th>

                <x-table.th class="w-3/12" kind="right">
                    <x-sortablelink column="description" :title="trans('general.description')" />
                </x-table.th>
            </x-table.tr>
        </x-table.thead>

        <x-table.tbody>
            @foreach ($milestones as $item)
                <x-table.tr>
                    <x-table.td class="w-3/12">
                        <x-date :date="$item->deadline_at" />
                    </x-table.td>

                    <x-table.td class="w-2/12">
                        {{ $item->name }}
                    </x-table.td>

                    <x-table.td class="w-2/12">
                        <span
                            class="px-2.5 py-1 text-xs font-medium rounded-xl bg-{{ $item->status_color() }}-200 text-{{ $item->status_color() }}-400">
                            {{ $item->status() }}%
                        </span>
                    </x-table.td>

                    <x-table.td class="w-2/12">

                        <span
                            class="px-2.5 py-1 text-xs font-medium rounded-xl bg-{{ $item->status_color() }}-300 text-{{ $item->status_color() }}-600">
                            {{ $item->status() == 100
                                ? 'Finalizado'
                                : ($item->status() >= 50
                                    ? 'En progreso'
                                    : ($item->status() == 0
                                        ? 'Pendiente'
                                        : ($item->status() < 50
                                            ? 'En progreso'
                                            : 'Sin status'
                                        )
                                    )
                                ) 
                            }}
                        </span>

                    </x-table.td>

                    <x-table.td class="w-3/12" kind="right">
                        {{ $item->description }}
                    </x-table.td>

                    <x-table.td kind="action">
                        <x-table.actions :model="$item" />
                    </x-table.td>
                </x-table.tr>
            @endforeach
        </x-table.tbody>
    </x-table>

    <x-pagination :items="$milestones" />
@else
    <x-projects::show.no-records name="milestones" :project="$project" />
@endif
