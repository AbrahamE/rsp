<div class="dashboard flex flex-wrap lg:-mx-12">
    @if(! user()->isCustomer())
    {{-- @widget('Modules\Projects\Widgets\TotalInvoice', $project)
    @widget('Modules\Projects\Widgets\TotalRevenue', $project)
    @widget('Modules\Projects\Widgets\TotalBill', $project)
    @widget('Modules\Projects\Widgets\TotalPayment', $project)
    @widget('Modules\Projects\Widgets\TotalActivity', $project)
    @widget('Modules\Projects\Widgets\TotalTask', $project)
    @widget('Modules\Projects\Widgets\TotalDiscussion', $project)
    @widget('Modules\Projects\Widgets\TotalUser', $project) --}}

    @widget('Modules\Projects\Widgets\TasksByStatuses', $project)
    @widget('Modules\Projects\Widgets\RecentlyAddedTask', $project)

    @widget('Modules\Projects\Widgets\TasksByMilestone', $project)
    @widget('Modules\Projects\Widgets\ActiveDiscussion', $project)
    @if($project->billing_type == 'billable')
        @widget('Modules\Projects\Widgets\ProjectLineChart', $project)
        @widget('Modules\Projects\Widgets\LatestIncome', $project)
    @endif
    
    @else
    @widget('Modules\Projects\Widgets\Portal\ProjectBrief', $project)
    @widget('Modules\Projects\Widgets\Portal\ProgressCompletedTasks', $project)
    @widget('Modules\Projects\Widgets\Portal\ProgressDaysLeft', $project)
    @widget('Modules\Projects\Widgets\Portal\RecentlyAddedTasks', $project)
    @widget('Modules\Projects\Widgets\Portal\MostLoggedTasks', $project)
    @widget('Modules\Projects\Widgets\ActiveDiscussion', $project)
    @endif
</div>
