<?php

namespace Modules\Projects\Listeners\User;

use App\Traits\Modules;
use App\Events\Auth\UserDeleted as Event;
use Illuminate\Support\Facades\DB;
use Modules\Projects\Models\ProjectTaskTimesheet;
use Modules\Projects\Models\ProjectTaskUser;
use Modules\Projects\Models\ProjectUser;

class UserDeleted
{
    use Modules;

    /**
     * Handle the event.
     *
     * @param \App\Events\Auth\UserDeleted $event
     * @return void
     */
    public function handle(Event $event)
    {
        if ($this->moduleIsDisabled('projects')) {
            return;
        }

        $user = $event->user;

        DB::transaction(function () use ($user) {
            ProjectUser::where('user_id', $user->id)->delete();
            ProjectTaskUser::where('user_id', $user->id)->delete();
            ProjectTaskTimesheet::where('user_id', $user->id)->delete();
        });
    }
}
