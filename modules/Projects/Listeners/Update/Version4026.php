<?php

namespace Modules\Projects\Listeners\Update;

use App\Abstracts\Listeners\Update as Listener;
use App\Events\Install\UpdateFinished;
use Illuminate\Support\Facades\File;

class Version4026 extends Listener
{
    const ALIAS = 'projects';

    const VERSION = '4.0.26';

    /**
     * Handle the event.
     *
     * @param  $event
     * @return void
     */
    public function handle(UpdateFinished $event)
    {
        if ($this->skipThisUpdate($event)) {
            return;
        }

        $this->deleteOldFiles();
    }

    public function deleteOldFiles()
    {
        $files = [
            'Observers/Document.php',
            'Observers/Transaction.php',
            'Observers/User.php',
            'Listeners/PreventUserDeleting.php',
        ];

        foreach ($files as $file) {
            File::delete(module_path(self::ALIAS, $file));
        }
    }
}
