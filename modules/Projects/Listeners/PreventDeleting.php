<?php

namespace Modules\Projects\Listeners;

use App\Events\Common\RelationshipCounting as Event;

class PreventDeleting
{
    public function handle(Event $event)
    {
        $condition = in_array($event->record->model->getTable(), [
            'contacts',
            'currencies'
        ]);

        if ($condition) {
            $event->record->relationships['projects'] = 'projects::general.projects';
        }
    }
}
