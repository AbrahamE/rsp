<?php

namespace Modules\Projects\Listeners\Transaction;

use App\Traits\Jobs;
use App\Traits\Modules;
use App\Events\Banking\TransactionUpdated as Event;
use Modules\Projects\Jobs\Financials\CreateFinancial;
use Modules\Projects\Jobs\Financials\DeleteFinancial;
use Modules\Projects\Jobs\Financials\UpdateFinancial;
use Modules\Projects\Models\Financial;

class UpdateTransaction
{
    use Jobs, Modules;

    /**
     * Handle the event.
     *
     * @param \App\Events\Banking\TransactionUpdated $event
     * @return void
     */
    public function handle(Event $event)
    {
        if ($this->moduleIsDisabled('projects')) {
            return;
        }

        $request = request();
        $project_id = $this->getProjectId($request);

        $transaction = $event->transaction;

        $financial = Financial::where([
            'financialable_id' => $transaction->id,
            'financialable_type' => get_class($transaction),
        ])->first();

        if (! $project_id) {
            if ($financial) {
                $this->dispatch(new DeleteFinancial($financial));
            }

            return;
        }

        if (empty($financial)) {
            $request->merge([
                'company_id' => company_id(),
                'financialable_id' => $transaction->id,
                'financialable_type' => get_class($transaction),
            ]);
    
            $this->dispatch(new CreateFinancial($request));

            return;
        }

        $this->dispatch(new UpdateFinancial($financial, ['project_id' => $project_id]));
    }

    protected function getProjectId($request)
    {
        $project_id = $request->project_id;

        if (is_null($request->project_id) && $request->segment(2) == 'projects') {
            $project_id = $request->segment(4);
        }

        return $project_id;
    }
}
