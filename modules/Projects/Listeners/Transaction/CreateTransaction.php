<?php

namespace Modules\Projects\Listeners\Transaction;

use App\Traits\Jobs;
use App\Traits\Modules;
use App\Events\Banking\TransactionCreated as Event;
use Modules\Projects\Jobs\Activities\CreateActivity;
use Modules\Projects\Jobs\Financials\CreateFinancial;

class CreateTransaction
{
    use Jobs, Modules;

    /**
     * Handle the event.
     *
     * @param \App\Events\Banking\TransactionCreated $event
     * @return void
     */
    public function handle(Event $event)
    {
        if ($this->moduleIsDisabled('projects')) {
            return;
        }
        
        $request = request();
        $project_id = $this->getProjectId($request);

        if (! $project_id) {
            return;
        }

        $transaction = $event->transaction;

        $this->dispatch(new CreateActivity([
            'company_id' => company_id(),
            'project_id' => $project_id,
            'activity_id' => $transaction->id,
            'activity_type' => get_class($transaction),
            'description' => trans('projects::activities.created.' . $transaction->type, [
                'user' => auth()->user()->name,
                'number' => $transaction->number
            ]),
            'created_by' => auth()->id()
        ]));

        $request->merge([
            'company_id' => company_id(),
            'financialable_id' => $transaction->id,
            'financialable_type' => get_class($transaction),
        ]);

        $this->dispatch(new CreateFinancial($request));
    }

    protected function getProjectId($request)
    {
        $project_id = $request->project_id;

        if (is_null($request->project_id) && $request->segment(2) == 'projects') {
            $project_id = $request->segment(4);
        }

        return $project_id;
    }
}
