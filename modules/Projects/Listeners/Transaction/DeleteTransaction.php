<?php

namespace Modules\Projects\Listeners\Transaction;

use App\Traits\Jobs;
use App\Traits\Modules;
use App\Events\Banking\TransactionDeleted as Event;
use Modules\Projects\Jobs\Financials\DeleteFinancial;
use Modules\Projects\Models\Financial;

class DeleteTransaction
{
    use Jobs, Modules;

    /**
     * Handle the event.
     *
     * @param \App\Events\Banking\TransactionDeleted $event
     * @return void
     */
    public function handle(Event $event)
    {
        if ($this->moduleIsDisabled('projects')) {
            return;
        }

        $transaction = $event->transaction;

        $financial = Financial::where([
            'financialable_id' => $transaction->id,
            'financialable_type' => get_class($transaction),
        ])->first();

        if ($financial) {
            $this->dispatch(new DeleteFinancial($financial));
        }
    }
}
