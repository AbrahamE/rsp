<?php

namespace Modules\Projects\Listeners\Document;

use App\Traits\Jobs;
use App\Traits\Modules;
use App\Models\Document\Document;
use App\Events\Document\DocumentDeleted as Event;
use Illuminate\Support\Facades\DB;
use Modules\Projects\Jobs\Financials\DeleteFinancial;
use Modules\Projects\Models\Financial;
use Modules\Projects\Models\Task;

class DeleteDocument
{
    use Jobs, Modules;

    /**
     * Handle the event.
     *
     * @param \App\Events\Document\DocumentDeleted $event
     * @return void
     */
    public function handle(Event $event)
    {
        if ($this->moduleIsDisabled('projects')) {
            return;
        }

        $document = $event->document;

        $financial = Financial::where([
            'financialable_id' => $document->id,
            'financialable_type' => get_class($document),
        ])->first();

        if ($financial) {
            if ($document->type == Document::INVOICE_TYPE) {
                DB::transaction(function () use ($financial, $document) {
                    Task::where([
                        'project_id' => $financial->project_id,
                        'invoice_id' => $document->id,
                    ])->update([
                        'is_invoiced' => false,
                        'invoice_id' => null,
                    ]);
                });
            }

            $this->dispatch(new DeleteFinancial($financial));
        }
    }
}
