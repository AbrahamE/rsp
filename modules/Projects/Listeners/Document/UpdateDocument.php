<?php

namespace Modules\Projects\Listeners\Document;

use App\Traits\Jobs;
use App\Traits\Modules;
use App\Events\Document\DocumentUpdated as Event;
use Modules\Projects\Jobs\Activities\CreateActivity;
use Modules\Projects\Jobs\Financials\CreateFinancial;
use Modules\Projects\Jobs\Financials\DeleteFinancial;
use Modules\Projects\Jobs\Financials\UpdateFinancial;
use Modules\Projects\Models\Financial;

class UpdateDocument
{
    use Jobs, Modules;

    /**
     * Handle the event.
     *
     * @param \App\Events\Document\DocumentUpdated $event
     * @return void
     */
    public function handle(Event $event)
    {
        if ($this->moduleIsDisabled('projects')) {
            return;
        }

        $document = $event->document;

        $financial = Financial::where([
            'financialable_id' => $document->id,
            'financialable_type' => get_class($document),
        ])->first();

        $request = request();
        $project_id = $this->getProjectId($document, $request);

        if (! $project_id) {

            if ($financial) {
                $this->dispatch(new DeleteFinancial($financial));
            }

            return;
        }

        if (empty($financial)) {
            $this->dispatch(new CreateActivity([
                'company_id' => company_id(),
                'project_id' => $project_id,
                'activity_id' => $document->id,
                'activity_type' => get_class($document),
                'description' => trans('projects::activities.created.' . $document->type, [
                    'user' => auth()->user()->name,
                    'number' => $document->document_number
                ]),
                'created_by' => auth()->id()
            ]));
    
            $request->merge([
                'company_id' => company_id(),
                'project_id' => $project_id,
                'financialable_id' => $document->id,
                'financialable_type' => get_class($document),
            ]);
    
            $this->dispatch(new CreateFinancial($request));

            return;
        }
            
        $this->dispatch(new UpdateFinancial($financial, ['project_id' => $project_id]));
    }

    protected function getProjectId($document, $request)
    {
        if (is_null($project_id = $request->project_id)) {
            if ($request->segment(2) == 'projects') {
                $project_id = $request->segment(4);
            } elseif (str($request->getPathInfo())->contains('duplicate')) {
                $document = $request->route($document->type);

                $project_id = Financial::where([
                    'financialable_id' => $document->id,
                    'financialable_type' => get_class($document),
                ])->first()->project_id ?? null;
            }
        }

        return $project_id;
    }
}
