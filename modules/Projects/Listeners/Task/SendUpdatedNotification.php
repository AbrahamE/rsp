<?php

namespace Modules\Projects\Listeners\Task;

use Modules\Projects\Events\Tasks\Updated as Event;

class SendUpdatedNotification
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Event $event)
    {
        $task = $event->task;
        $user = $event->user;
        $notification = $event->notification;

        // Notify the user
        if ($user && !empty($user->email)) {
            $user->notify(new $notification($task, $user, "projects_task_update"));
        }
    }
}
