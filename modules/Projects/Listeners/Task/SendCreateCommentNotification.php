<?php

namespace Modules\Projects\Listeners\Task;

use Modules\Projects\Events\Tasks\CreateComment as Event;

class SendCreateCommentNotification
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Event $event)
    {
        $task = $event->task;
        $user = $event->user;
        $notification = $event->notification;

        // Notify the user
        if ($user && !empty($user->email)) {
            $user->notify(new $notification($task, $user, "projects_task_create_comment"));
        }
    }
}
