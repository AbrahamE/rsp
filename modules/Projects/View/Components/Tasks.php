<?php

namespace Modules\Projects\View\Components;

use Modules\Projects\Abstracts\View\ProjectsComponent;

class Tasks extends ProjectsComponent
{
    const RELATION = 'tasks';

    /** @var mixed */
    public $tasks;

    /** @var mixed */
    public $project;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(mixed $project = null)
    {
        $this->tasks = $project->tasks()->orderBy('created_at', 'DESC')->collect();

        $this->project = $project;
    }
}
