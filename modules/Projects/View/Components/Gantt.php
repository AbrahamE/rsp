<?php

namespace Modules\Projects\View\Components;

use Carbon\Carbon;
use Modules\Projects\Abstracts\View\ProjectsComponent;

class Gantt extends ProjectsComponent
{
    const RELATION = 'tasks';

    /** @var mixed */
    public $tasks;

    /** @var mixed */
    public $project;

    /** @var mixed */
    public $tasks_project;

    /** @var mixed */
    public $milestones;

    /** @var mixed */
    public $dependencies;

    /** @var mixed */
    public $id_increment = 1;

     /** @var mixed */
    public $id_milestones;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(mixed $project = null)
    {
        $this->project = $project;

        $this->tasks = $project->tasks()->orderBy('created_at', 'ASC')->collect();
        $this->milestones = $project->milestones()->orderBy('created_at', 'ASC')->collect();
        $this->tasks_project = $this->getProjectDuration();
        $this->createTasksData();
        $this->createDependencies();
        $this->tasks_project = [...$this->tasks_project];
    }


    public function createTasksData()
    {
        $this->milestones->map(function ($milestone, int $key) {
            $task =  [
                "id" => $this->id_increment,
                "parentId" => 1,
                "title" =>  $milestone->name,
                "start" => $milestone->created_at->format('Y-m-d'),
                "end" => $milestone->deadline_at,
                "progress" => (int) $milestone->status(),
            ];

            $this->id_milestones =  $task["id"];
            $this->incrementId();

            $this->tasks_project->push(
                $task,
                ...$milestone->tasks->map(function ($task)  {
                    $result = [
                        "id" => $this->id_increment,
                        "title" => $task->name,
                        "start" => $task->started_at,
                        "end" => $task->deadline_at,
                        "progress" => $task->advance,
                        "parentId" =>  $this->id_milestones,
                    ];

                    $this->incrementId();

                    return $result;
                })->toArray()
            );
        });
    }

    public function createDependencies()
    {
        $this->dependencies = $this->tasks_project->map(function ($task) {
            return $task["id"];
        });

        $dependencies = $this->dependencies->toArray();

        $this->dependencies = array_map(function ($id, $index) use ($dependencies) {
            return [
                'id' => $id,
                'predecessorId' => $id,
                'successorId' => isset($dependencies[$index + 1]) ? $dependencies[$index + 1] : $dependencies[$index] + 1,
                'type' => 0,
            ];
        }, $dependencies, array_keys($dependencies));
    }

    public function getProjectDuration()
    {
        $project = collect([]);

        $project->push([
            "id" => $this->id_increment,
            "parentId" => -1,
            "title" =>  $this->project->name,
            "start" => $this->project->started_at,
            "end" => $this->project->ended_at,
            "progress" => 0,
        ]);

        $this->incrementId();

        return $project;
    }


    protected function incrementId()
    {
        return $this->id_increment = $this->id_increment + 1;
    }
}
