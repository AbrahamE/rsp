<?php

namespace Modules\Projects\View\Components;

use Modules\Projects\Abstracts\View\ProjectsComponent;

use function PHPSTORM_META\map;

class Board extends ProjectsComponent
{
    const RELATION = 'tasks';

    /** @var mixed */
    public $tasks;

    /** @var mixed */
    public $project;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(mixed $project = null)
    {
        $this->tasks = $project->tasks()->orderBy('created_at', 'DESC')->collect();

        $this->tasks =  $this->tasks->map(function ($task) {
            $task->deadline_at = date('d/m/Y', strtotime($task->deadline_at));
            return $task;
        });

        $this->project = $project;
    }
}
