<div class="text border-bottom-1 py-1">
    <span class="float-left font-semibold">
        {{ trans_choice('general.totals', 1) }}:
    </span>

    <span>
        {{ $words }}
    </span>
</div>