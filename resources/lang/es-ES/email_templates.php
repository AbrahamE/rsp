<?php

return [

    'invoice_new_customer' => [
        'subject' => 'Factura {invoice_number} creada',
        'body' => 'Estimado/a {customer_name},<br /><br />Hemos preparado la siguiente factura para usted: <strong>{invoice_number}</strong>.<br /><br />Puede ver los detalles de la factura y proceder con el pago desde el siguiente enlace: <a href="{invoice_guest_link}">{invoice_number}</a>.<br /><br />No dude en contactarnos si tiene alguna pregunta.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Nueva Factura para el Cliente',
    ],

    'invoice_remind_customer' => [
        'subject' => 'Aviso de factura {invoice_number} vencida',
        'body' => 'Estimado/a {customer_name},<br /><br />Este es un aviso de vencimiento para la factura <strong>{invoice_number}</strong>.<br /><br />El total de la factura es {invoice_total} y su fecha de vencimiento fue el <strong>{invoice_due_date}</strong>.<br /><br />Puede ver los detalles de la factura y proceder con el pago desde el siguiente enlace: <a href="{invoice_guest_link}">{invoice_number}</a>.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Aviso de Factura Vencida al Cliente',
    ],

    'invoice_remind_admin' => [
        'subject' => 'Aviso de factura {invoice_number} vencida',
        'body' => 'Hola,<br /><br />{customer_name} ha recibido un aviso de vencimiento para la factura <strong>{invoice_number}</strong>.<br /><br />El total de la factura es {invoice_total} y su fecha de vencimiento fue el <strong>{invoice_due_date}</strong>.<br /><br />Puede ver los detalles de la factura desde el siguiente enlace: <a href="{invoice_admin_link}">{invoice_number}</a>.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Aviso de Factura Vencida al Administrador',
    ],

    'invoice_recur_customer' => [
        'subject' => 'Factura recurrente {invoice_number} creada',
        'body' => 'Estimado/a {customer_name},<br /><br />Con base en su ciclo recurrente, hemos preparado la siguiente factura para usted: <strong>{invoice_number}</strong>.<br /><br />Puede ver los detalles de la factura y proceder con el pago desde el siguiente enlace: <a href="{invoice_guest_link}">{invoice_number}</a>.<br /><br />No dude en contactarnos si tiene alguna pregunta.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Factura Recurrente para el Cliente',
    ],

    'invoice_recur_admin' => [
        'subject' => 'Factura recurrente {invoice_number} creada',
        'body' => 'Hola,<br /><br />Con base en el ciclo recurrente de {customer_name}, se ha creado automáticamente la factura <strong>{invoice_number}</strong>.<br /><br />Puede ver los detalles de la factura desde el siguiente enlace: <a href="{invoice_admin_link}">{invoice_number}</a>.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Factura Recurrente Creada para el Administrador',
    ],

    'invoice_view_admin' => [
        'subject' => 'Factura {invoice_number} vista',
        'body' => 'Hola,<br /><br />{customer_name} ha visto la factura <strong>{invoice_number}</strong>.<br /><br />Puede ver los detalles de la factura desde el siguiente enlace: <a href="{invoice_admin_link}">{invoice_number}</a>.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Factura Vista por el Administrador',
    ],

    'invoice_payment_customer' => [
        'subject' => 'Su recibo de la factura {invoice_number}',
        'body' => 'Estimado/a {customer_name},<br /><br />Gracias por su pago. Encuentre los detalles del pago a continuación:<br /><br />-------------------------------------------------<br />Monto: <strong>{transaction_total}</strong><br />Fecha: <strong>{transaction_paid_date}</strong><br />Número de factura: <strong>{invoice_number}</strong><br />-------------------------------------------------<br /><br />Siempre puede ver los detalles de la factura desde el siguiente enlace: <a href="{invoice_guest_link}">{invoice_number}</a>.<br /><br />No dude en contactarnos si tiene alguna pregunta.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Recibo de Pago de la Factura para el Cliente',
    ],

    'invoice_payment_admin' => [
        'subject' => 'Pago recibido para la factura {invoice_number}',
        'body' => 'Hola,<br /><br />{customer_name} ha registrado un pago para la factura <strong>{invoice_number}</strong>.<br /><br />Puede ver los detalles de la factura desde el siguiente enlace: <a href="{invoice_admin_link}">{invoice_number}</a>.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Pago Recibido para la Factura - Administrador',
    ],

    'bill_remind_admin' => [
        'subject' => 'Aviso de recordatorio de factura {bill_number}',
        'body' => 'Hola,<br /><br />Este es un aviso de recordatorio para la factura <strong>{bill_number}</strong> a {vendor_name}.<br /><br />El total de la factura es {bill_total} y su fecha de vencimiento es el <strong>{bill_due_date}</strong>.<br /><br />Puede ver los detalles de la factura desde el siguiente enlace: <a href="{bill_admin_link}">{bill_number}</a>.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Aviso de Recordatorio de Factura - Administrador',
    ],

    'bill_recur_admin' => [
        'subject' => 'Factura recurrente {bill_number} creada',
        'body' => 'Hola,<br /><br />Con base en el ciclo recurrente de {vendor_name}, se ha creado automáticamente la factura <strong>{bill_number}</strong>.<br /><br />Puede ver los detalles de la factura desde el siguiente enlace: <a href="{bill_admin_link}">{bill_number}</a>.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Factura Recurrente Creada - Administrador',
    ],

    'payment_received_customer' => [
        'subject' => 'Su recibo de {company_name}',
        'body' => 'Estimado/a {contact_name},<br /><br />Gracias por su pago. <br /><br />Puede ver los detalles del pago desde el siguiente enlace: <a href="{payment_guest_link}">{payment_date}</a>.<br /><br />No dude en contactarnos si tiene alguna pregunta.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Recibo de Pago para el Cliente',
    ],

    'payment_made_vendor' => [
        'subject' => 'Pago realizado por {company_name}',
        'body' => 'Estimado/a {contact_name},<br /><br />Hemos realizado el siguiente pago. <br /><br />Puede ver los detalles del pago desde el siguiente enlace: <a href="{payment_guest_link}">{payment_date}</a>.<br /><br />No dude en contactarnos si tiene alguna pregunta.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Pago Realizado - Proveedor',
    ],

    'task_assignment_user' => [
        'subject' => 'Tarea Asignada',
        'body' => 'Un cordial saludo {member_name}, se te ha asignado una tarea con el nombre {task_name}, como miembro de esta, y su descripción es: {task_description}.',
        'name' => 'Asignación de Tarea al Usuario',
    ],

    'task_assignment_liable' => [
        'subject' => 'Tarea Asignada',
        'body' => 'Un cordial saludo {member_name}, se te ha asignado una tarea con el nombre {task_name}, como responsable de esta, y su descripción es: {task_description}.',
        'name' => 'Asignación de Tarea al Responsable',
    ],

    'task_completion_user' => [
        'subject' => 'Tarea Completada',
        'body' => 'Estimado/a {member_name},<br /><br />La tarea {task_name} ha sido completada. Gracias por tu esfuerzo y dedicación.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Notificación de Tarea Completada al Usuario',
    ],

    'task_completion_admin' => [
        'subject' => 'Tarea Completada',
        'body' => 'Hola,<br /><br />La tarea {task_name} ha sido completada por {member_name}.<br /><br />Atentamente,<br />{company_name}',
        'name' => 'Notificación de Tarea Completada al Administrador',
    ],
];
