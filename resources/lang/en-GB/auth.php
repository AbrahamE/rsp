<?php

return [

    'auth' => 'Autenticación',
    'profile' => 'Perfil',
    'logout' => 'Cierre de sesión',
    'login' => 'Inicio de sesión',
    'forgot' => 'Olvidó',
    'login_to' => 'Inicia sesión',
    'remember_me' => 'Recuérdame',
    'forgot_password' => 'He olvidado mi contraseña',
    'reset_password' => 'Restablecer contraseña',
    'change_password' => 'Cambiar contraseña',
    'enter_email' => 'Introduzca su dirección de correo electrónico',
    'current_email' => 'Correo electrónico actual',
    'reset' => 'Restablecer',
    'never' => 'Nunca',
    'landing_page' => 'Página de aterrizaje',
    'personal_information' => 'Información Personal',
    'register_user' => 'Registrar Usuario',
    'register' => 'Registrar',

    'form_description' => [
        'personal' => 'TSe enviará un enlace de invitación al nuevo usuario, así que asegúrese de que la dirección de correo electrónico es correcta. El nuevo usuario podrá introducir su contraseña.',
        'assign' => 'The user will have access to the selected companies. You can restrict the permissions from the <a href=":url" class="border-b border-black">roles</a> page.',
        'preferences' => 'Seleccione el idioma por defecto del usuario. También puede configurar la página de destino después de que el usuario inicie sesión.',
    ],

    'password' => [
        'pass' => 'Contraseña',
        'pass_confirm' => 'Confirmación de contraseña',
        'current' => 'Contraseña actual',
        'current_confirm' => 'Confirmación de contraseña actual',
        'new' => 'Nueva Contraseña',
        'new_confirm' => 'Confirmación de nueva contraseña',    
    ],

    'error' => [
        'self_delete'       => 'Error: Can not delete yourself!',
        'self_disable'      => 'Error: Can not disable yourself!',
        'unassigned'        => 'Error: Can not unassigned company! The :company company must be assigned at least one user.',
        'no_company'        => 'Error: No company assigned to your account. Please, contact the system administrator.',
    ],

    'login_redirect'        => 'Verification done! You are being redirected...',
    'failed'                => 'These credentials do not match our records.',
    'throttle'              => 'Too many login attempts. Please try again in :seconds seconds.',
    'disabled'              => 'This account is disabled. Please, contact the system administrator.',

    'notification' => [
        'message_1'         => 'You are receiving this email because we received a password reset request for your account.',
        'message_2'         => 'If you did not request a password reset, no further action is required.',
        'button'            => 'Reset Password',
    ],

    'invitation' => [
        'message_1'         => 'You are receiving this email because you are invited to join the Akaunting.',
        'message_2'         => 'If you do not want to join, no further action is required.',
        'button'            => 'Get started',
    ],

    'information' => [
        'invoice'           => 'Create invoices easily',
        'reports'           => 'Get detailed reports',
        'expense'           => 'Track any expense',
        'customize'         => 'Customize your Akaunting',
    ],

    'roles' => [
        'admin' => [
            'name'          => 'Admin',
            'description'   => 'They get full access to your Akaunting including customers, invoices, reports, settings, and apps.',
        ],
        'manager' => [
            'name'          => 'Manager',
            'description'   => 'They get full access to your Akaunting, but can\'t manage users and apps.',
        ],
        'customer' => [
            'name'          => 'Customer',
            'description'   => 'They can access the Client Portal and pay their invoices online through the payment methods you set up.',
        ],
        'accountant' => [
            'name'          => 'Accountant',
            'description'   => 'They can access invoices, transactions, reports, and create journal entries.',
        ],
        'employee' => [
            'name'          => 'Employee',
            'description'   => 'They can create expense claims and track time for assigned projects, but can only see their own information.',
        ],
    ],

];
