@if ($service)
    <x-table.tr data-collapse="child-{{ $service->category_id }}" data-animation class="relative flex items-center hover:bg-gray-100 px-1 group border-b transition-all collapse-sub" href="{{ route('categories.edit', $service->id) }}">
        <x-table.td kind="bulkaction">
            <x-index.bulkaction.single id="{{  $service->id }}" name="{{ $service->name }}" />
        </x-table.td>

        <x-table.td class="w-full ltr:pr-6 rtl:pl-6 py-4 ltr:text-left rtl:text-right whitespace-nowrap text-sm font-medium text-black truncate" style="padding-left: 40px;">
            <div class="flex items-center font-bold">
                <span class="material-icons transform mr-1 text-lg leading-none">subdirectory_arrow_right</span>
                {{ $service->name }}
            </div>

            {{-- @if (! $service->enabled)
                <x-index.disable text="{{ trans_choice('general.categories', 1) }}" />
            @endif --}}
        </x-table.td>

        {{-- <x-table.td class="w-5/12 ltr:pr-6 rtl:pl-6 py-4 ltr:text-left rtl:text-right whitespace-nowrap text-sm font-normal text-black cursor-pointer truncate">
            tickets
        </x-table.td> --}}

        {{-- <x-table.td class="ltr:pr-6 rtl:pl-6 py-4 ltr:text-left rtl:text-right whitespace-nowrap text-sm font-normal text-black cursor-pointer w-2/12 relative">
            <span class="material-icons text-3xl text-{{ $service->color }}" style="color:{{ $service->color }};">circle</span>
        </x-table.td> --}}

        <x-table.td kind="action">
            <x-table.actions :model="$service" />
        </x-table.td>
    </x-table.tr>
@endif
