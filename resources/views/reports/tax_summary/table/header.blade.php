<thead>
    <tr class="border-b border-purple">
        <th class="ltr:text-left rtl:text-right text-xl text-matisse-900 font-bold uppercase print-heading" colspan="{{ count($class->dates) + 2 }}">
            {{ $table_name }}
        </th>
    </tr>
</thead>
