@push('scripts_start')
    <script src="{{ asset($source) }}"></script>

    @if (!empty($variables))
        @foreach ($variables as $key => $value)
            <script>
                let {!! $key !!} = {!! json_encode($value) !!};
                console.log({!! $key !!});
            </script>
        @endforeach
    @endif
@endpush
