@stack('content_start')
<div class="w-2/5 h-full  flex flex-col justify-center gap-12 px-6 lg:px-24 py-24 lg:mt-0 bg-white">
    <div class="flex flex-col gap-4">
        {!! $slot !!}
    </div>
</div>
@stack('content_end')
