<div data-index-icon>
    <x-tooltip id="{{ $id }}" placement="{{ $position }}" message="{{ $text }}">
        <span class="material-icons{{ $iconType }} text-matisse-900 text-sm ml-2">
            {{ $icon }}
        </span>
    </x-tooltip>
</div>
